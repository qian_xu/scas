MonitorTimeStep = 30*dt;

for MonitorIndex = 1:150
    if(length(MotSim.Atoms) == 0)
        break;
    else
        MotSim.Evolution(MonitorTimeStep, dt);
%         PyramidSim.Monitor.PlotAtoms();
    end
end

% for ease of console work
sim = MotSim;
atoms = sim.Atoms;