classdef RecordMonitor < matlab.mixin.SetGet
    %RECORDMONITOR records atoms 
    %   Detailed explanation goes here
    
    properties
        Simulation;
        %InitialAtoms;
        OutAtoms;
        
        

    end
    
    methods
        function instance = RecordMonitor(sim)
            %RECORDMONITOR Construct an instance of this class
            %   Detailed explanation goes here
            instance.Simulation = sim;
            instance.OutAtoms = [];
            sim.RecordAtomsEvent.add(EventListener(@(sender, callback) instance.RecordOutAtoms(sender, callback)));
        end
        
        function RecordOutAtoms(instance, sender, callback)
            %RECORDOUTATOMS Copy the atoms coming out from simulation to
            %the OutAtom list 
            %   Detailed explanation goes here
            NewOutAtoms = arrayfun(@(x) x.CopyAtom(), instance.Simulation.Atoms.getAtom(instance.Simulation.Atoms.CurrentStates == AtomState.ComingOutFromTube));
            instance.OutAtoms = [instance.OutAtoms; NewOutAtoms];
        end
        
        function OutAtomsStatistics(instance)
            OutAtomsPositions = [instance.OutAtoms.Position];
            OutAtomsVelocities = [instance.OutAtoms.Velocity];
            x = OutAtomsPositions(1,:);
            y = OutAtomsPositions(2,:);
            z = OutAtomsPositions(3,:);
            vx = OutAtomsVelocities(1,:);
            vy = OutAtomsVelocities(2,:);
            vz = OutAtomsVelocities(3,:);
            v = sqrt(vx.^2 + vy.^2 + vz.^2);
            thetax = atan(vx./vz);
            
            figure;
            histogram(v,20);
            xlabel('v (m/s)', 'fontsize', 20);
            title('v distribution of Atoms that come out','fontsize',15);
            set(gca,'FontSize',20);
            
            figure;
            histogram(thetax,20);
            xlabel('\theta_x (rad)', 'fontsize', 20);
            title('\theta_x distribution of Atoms that come out','fontsize',15);
            set(gca,'FontSize',20);                  
        end
       
        
        
    end
end

