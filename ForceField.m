classdef (Abstract) ForceField < matlab.mixin.SetGet
    %FORCEFIELD Calculates forces applied to atoms.
    %  
    
    properties
    end
    
    methods (Abstract)
        
        %CALCULATEFORCES Calculates forces applied to atom
        f = CalculateForce(instance, Atoms);
        
    end
    
end

