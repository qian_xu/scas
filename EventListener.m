classdef EventListener < handle
    %EVENTLISTENER EventListener
    
    properties
        Callback
    end
    
    methods
        function instance = EventListener(callback)
            %LISTENER Creates an event listener with specified callback.
            %   callback(sender, args) is invoked on event:
            %    sender is an object reference.
            %    args describes the event circumstance
            EventListener.checkValid(callback);
            instance.Callback = callback;
        end
    end
    
    methods (Static)
        function checkValid(callback)
            if ~isa(callback, 'function_handle')
                error('callback must be a function handle.');
            end
            if nargin(callback)~= 2
                error('callback must have two input arguments: sender, args');
            end
        end
    end
end

