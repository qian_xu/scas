function [ out_kappa2 ] = kappa2( delta, I, x, T )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

f2 =@(vx) kappa0(delta, I, x, vx, T);

out_kappa2 = integral(f2,0,600);

end

