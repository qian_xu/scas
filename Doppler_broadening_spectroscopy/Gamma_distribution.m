function [p] = Gamma_distribution(v,T)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
kB =  1.38e-23;
m = 1.44e-25;

v0 = sqrt(2*kB*T/m);

a = 0.4e-3;
L = 4e-3;  
p = abs(v).*exp(-v.^2/v0^2).*igamma(-1/2, v.^2*L^2/(v0^2*a^2))/sqrt(8*pi*v0^2*(1+a^2/L^2)^(1/2));

end

