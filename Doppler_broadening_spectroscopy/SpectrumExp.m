%Load data from file
AbsorptionData = load('/Users/XQ/Desktop/Absorption_525.csv');
ScanTime = AbsorptionData(:,1);
EtalonSignal = AbsorptionData(:,2);
Transmission = AbsorptionData(:,3);

Gamma = 2*pi*32e6;

% figure;
% plot(ScanTime, EtalonSignal, ScanTime, Transmission);
% 
% figure;
% plot(ScanTime, EtalonSignal);

%%
EtalonResonantTimeList = ScanTime(EtalonSignal >= 0.3);
RefTime = EtalonResonantTimeList(1);
EtalonResonantTime = [];
j = 0;
i = 1;
while i < length(EtalonResonantTimeList)
        TempTime = 0;
        TempLength = 0;
        while(((abs(EtalonResonantTimeList(i) - RefTime) <= 0.2) && (i < length(EtalonResonantTimeList))))
            TempTime = TempTime + EtalonResonantTimeList(i);
            TempLength = TempLength + 1;
            i = i +1;
        end
        EtalonResonantTime(end + 1) = TempTime/TempLength;
        j = j +1;
        if(i <= length(EtalonResonantTimeList))
            RefTime = EtalonResonantTimeList(i);   
        end
end
     
EtalonResonantFrequency = (-750:300:(-750 + (length(EtalonResonantTime)-1)*300))*1e6/(Gamma/(2*pi));

Transmission = Transmission((ScanTime >= EtalonResonantTime(1))&(ScanTime <= EtalonResonantTime(end)));
ScanTime  = ScanTime((ScanTime >= EtalonResonantTime(1))&(ScanTime <= EtalonResonantTime(end)));

figure;
plot(EtalonResonantTime, EtalonResonantFrequency, 'o')

ScanFrequencyTemp = interp1(EtalonResonantTime,EtalonResonantFrequency,ScanTime);

% Roughly set the center according to the center of mass
ScanFrequency = ScanFrequencyTemp;

Transmission = Transmission/mean(Transmission(1:100));

CenterFrequency = sum((1-Transmission).*ScanFrequency)/sum((1-Transmission))

figure
plot(ScanFrequency, Transmission);
ylim([0,1]);

ScanFrequency = ScanFrequency - CenterFrequency;
figure;
plot(ScanFrequency, Transmission);
ylim([0,1]);
        
            
%% Statistics
TranMin = min(Transmission);
AbsorptionDepth = log(Transmission(1)/TranMin)
HWHMTran = (Transmission(1)+TranMin)/2;
HWHMIndex = find(abs(Transmission - HWHMTran)/Transmission(1) <= 0.006)
Transmission(HWHMIndex)/Transmission(1)
deltaf = ScanFrequency(47912) - ScanFrequency(31284)

lambda0 = 461e-9;
k = 2*pi/lambda0;
deltav = deltaf*Gamma/(k)
    