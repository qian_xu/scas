function [f] = VDF_xyz(vx,vy,vz,T)
%VDF_XYZ Summary of this function goes here
%   Detailed explanation goes here

f = VDF(sqrt(vx.^2 + vy.^2 + vz.^2),T).*j_theta(atan(sqrt(vx.^2 + vy.^2)./vz))./...
    (2*pi*sqrt(vx.^2 + vy.^2 + vz.^2).*sqrt(vx.^2 + vy.^2));
end

