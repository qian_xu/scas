%%
%Analyze the transvere velocity distribution
T = 482 + 273;
Scanvx = linspace(-150,150,1000);
Scanpop = zeros(1,length(Scanvx));
for i = 1:length(Scanvx)
    vx_temp = abs(Scanvx(i));
    f1 =@(vz) VDF_xyz(vx_temp, 0, vz,T);
    Scanpop(i) = integral(f1,0.00001,1000);
end
ScanGammaDis = Gamma_distribution(Scanvx,T);
Scanpop = Scanpop/(max(Scanpop));
ScanGammaDis = ScanGammaDis/max(ScanGammaDis);
figure;
plot(Scanvx,Scanpop, 'k')
hold on;
plot(ScanFrequency*Gamma/(2*pi/(461e-9)), (1-Transmission)/(max(1-Transmission)), 'r');
plot(scan_delta*Gamma/(2*pi/(461e-9)), (1-scan_I_t/I_i)/(max(1-scan_I_t/I_i)), 'b');
xlim([-150,150])
xlabel('v_x (m/s)', 'fontsize', 20);
ylabel('Normalized probability', 'fontsize', 20);
title('Transverse velocity distribution @ T_{measured} = 525^。, T_{fitted} = 482^。', 'fontsize',20);
legend({'f(v_x)','1 - T_{experiment}', '1 - T_{theory}'},'FontSize',20)
set(gca,'FontSize',20);
set(gca,'position',[0.1 0.1 0.8 0.8]);
set(gcf,'Position',[90 90 900 720]);


figure;
plot(Scanvx, Scanpop, Scanvx, ScanGammaDis)

figure;
plot(Scanvx, ScanGammaDis)