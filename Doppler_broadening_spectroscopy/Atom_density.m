function [ density ] = Atom_density(x, vx )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

%calculate the flux
T = 500 + 273;
P = 10^(7.753 - 7628/T);
d_hole = 0.4e-3;
A = 76*pi*d_hole^2/4;
kB =  1.38e-23;
m = 1.44e-25;
eta = P*A/sqrt(2*pi*m*kB*T)/pi;


%set parameters
lambda0 = 461e-9;
k = 2*pi/lambda0;
Gamma = 2*pi*32e6;
d = 83e-3;
I0 = 430;

theta_temp = atan(abs(x)/d);

density = eta*j_theta(theta_temp)./(2*pi*abs(vx).*(x.^2 + d^2))...
   .*VDF(abs(vx)./abs(x).*sqrt(x.^2+d^2)).*sqrt(x.^2+d^2)./abs(x);




end

