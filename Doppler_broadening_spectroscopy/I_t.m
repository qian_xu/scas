function [ out_I_t ] = I_t( delta, I_i, L, T)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

N = 500;
dx = L/N;
x_nega = linspace(-L/2, 0.000001, N/2);
x_posi = linspace(0.000001, L/2, N/2);
I_temp = I_i;

for i = 1:length(x_nega)
    
    I_temp = I_temp - kappa1(delta, I_temp, x_nega(i), T)*I_temp*dx;
    
end

for i = 1:length(x_posi)
    
    I_temp = I_temp - kappa2(delta, I_temp, x_posi(i), T)*I_temp*dx;
end

out_I_t = I_temp;

end

