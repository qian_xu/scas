function [ ThermalAtoms ] = GenerateSurfaceAtoms( AtomNum, XLowerBound, XUpperBound, YLowerBound, YUpperBound, L, Vtruncation  )
%GENERATESURFACEATOMS generates a group of atoms which are subject to the
%distribution of thermal atoms going through a surface

m = 1.44e-25;
Gamma = 2*pi*6.06e6;
I0 = 16.7;

f1 = arrayfun(@(x) Atom.Create(m,Gamma,I0,[XLowerBound + (XUpperBound - XLowerBound)*rand(); ...
    YLowerBound + (YUpperBound - YLowerBound)*rand();  L],[0,0,0]), 1:AtomNum, 'UniformOutput', 0);
ThermalAtoms = cat(1,f1{:});
muB = 9.27400e-24;
gp = - 2/3;
gm = 2/3;
gz = 0;
set(ThermalAtoms, {'Mup', 'Mum', 'Muz'}, {gp*muB,  gm*muB,  gz*muB});


%Set the velocity of atoms 
ThermalVelocities = ones(3,AtomNum);
vmax = Vtruncation;
ThetaMax = pi/2;
JMax = 1;

fmax = max(VDF(linspace(0,Vtruncation,100)));   %Only works when VTruncation <= Vp

for AtomIndex = 1:AtomNum
    VRandom = rand()*vmax;
    p = VDF(VRandom)/fmax;
    s = rand();
    while(s > p)
        VRandom = rand()*vmax;
        p = VDF(VRandom)/fmax;
        s = rand();
    end
    ThetaRandom = rand()*ThetaMax;
    p = JTheta(ThetaRandom)/1;
    s = rand();
    while(s > p)
        ThetaRandom = rand()*pi;
        p = JTheta(ThetaRandom)/JMax;
        s = rand();
    end
    PhiRandom = rand()*2*pi;
    
    ThermalVelocities(3,AtomIndex) = VRandom*cos(ThetaRandom);
    ThermalVelocities(1,AtomIndex) = VRandom*sin(ThetaRandom)*cos(PhiRandom);
    ThermalVelocities(2,AtomIndex) = VRandom*sin(ThetaRandom)*sin(PhiRandom);
    
end

set(ThermalAtoms, {'Velocity'}, num2cell(ThermalVelocities,1)');
set(ThermalAtoms, 'State', AtomState.InsideChamberNeverInPyramid);

end



