%set the parameters of Rubidium atom
m = 1.44e-25;
Gamma = 2*pi*32e6;
I0 = 430;
muB = 9.27400e-24;
gp = -1;
gm = 1;
gz = 0;

%set the parameters of laser beams
P = 200e-3;
lambda = 461e-9;
delta = - 3*Gamma;

Dx0 = 22e-3;
Dy0 = 14.5e-3;

P_push = 20;
Dx_push = 1.65e-3;
Dy_push = 1.65e-3;

% %set the parameters of the chamber
ChamberDiameter = 60e-3;
TubeDiameter = 4e-3;
ds = 83e-3;

%Create six beams
BeamXp = GaussianBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2, 0, 0);
set(BeamXp, {'Direction', 'Polarization'}, {[1; 0; 0],1});
BeamXm = GaussianBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2, pi, 0);
set(BeamXm, {'Direction', 'Polarization'}, {[- 1; 0; 0],1});
BeamYp = GaussianBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2,- pi/2, 0);
set(BeamYp, {'Direction', 'Polarization'}, {[0; 1; 0],1});
BeamYm = GaussianBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2,  pi/2, 0);
set(BeamYm, {'Direction', 'Polarization'}, {[0; - 1; 0],1});
BeamZp = GaussianBeam.Create(P_push, lambda, delta, Dx_push, Dy_push, 0, 0, 0 );
set(BeamZp, {'Direction', 'Polarization'}, {[0; 0; 1],- 1});
Beams = {BeamXp,BeamXm, BeamYp, BeamYm, BeamZp };

%Set the linear magnetic field
Xc = 0;
Yc = 0;
Zc = 0;
Beta = 20*0.01;   %Linear gradient
LinearMagField = LinearMagneticField();
set(LinearMagField, 'Gradient', Beta);
set(LinearMagField, 'FieldCenter', [Xc; Yc; Zc]);


%Set the gravity 
G = Gravity();

%Set the scattering force
dt = ChamberDiameter/10/3000;
ScatForce = ScatteringForce();
set(ScatForce, 'Beams', Beams);
set(ScatForce, 'MagField', LinearMagField);
set(ScatForce, 'TimeStep', dt);
set(ScatForce, {'IfAddRecoil', 'IfAddScatteringFluctuation'}, {false,false});
set(ScatForce,'IfResonant',true);

%Set total forces: Gravity and scattering force
forces = {ScatForce};

Vtruncation = 120;

%%

%Create a group of thermal atoms
AtomNum = 20000;
AtomicBeam = GenerateAtomicBeam(AtomNum, Vtruncation);

%Copy atoms to a initial atoms list
%InitialAtoms;

%%
%Create Pyramid_sim object

MotSim = Simulation();
MotSim.AddAtoms(AtomicBeam);
MotSim.SpecifyForces(forces);

set(MotSim, 'BoundaryCondition', 1);


%% Monitors
rtm = RealTimeMonitor(MotSim);
set(rtm, {'ChamberDiameter', 'ds'},{ChamberDiameter, ds});

RecordM = RecordMonitor(MotSim);

%% Set StateModifiers
StateModifier = RealTimeStateModifier(MotSim);
set(StateModifier, {'ChamberDiameter', 'TubeDiameter'},{ChamberDiameter, TubeDiameter});
set(StateModifier, 'IfKeepOutAtoms', true);

%%
%Initialize the atoms in Pyramid_sim 
MotSim.Initialize();

%% Evolution process
%Run while plotting the trajectories
runsim

%% Statistics of different type atoms & plot graphs
RecordM.OutAtomsStatistics();
