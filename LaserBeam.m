classdef LaserBeam <  matlab.mixin.SetGet
    %LASERBEAM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties 
        %POWER Total power of the laser beam
        Power;
        %WAVELENGTH WaveLength of the laser beam
        WaveLength;
        %DETUNING Detuning of the laser from a specific optical transition
        %of atoms
        Detuning;  
        %POLARIZATION Polarization of the laser beam (1:left-handed, -1:right-handed)
        Polarization;
        %DIRECTION Direction vector of the beam
        Direction;
        
             
        
    end
    
    methods (Abstract)
        CalculateIntensity(x,y,z);
        
        
    end
    
end

