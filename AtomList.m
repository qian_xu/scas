classdef AtomList < handle
    %ATOMLIST A list of atoms
    %   Handles packing and unpacking of atom arrays into lists of atoms.
    
    properties (SetAccess=private, GetAccess=private)
        Atoms;
    end
    
    properties
        Positions
        Velocities
        CurrentStates
        PreviousStates
        Indices
    end
    
    methods
        
        function obj = AtomList(atoms)
            %ATOMLIST Construct an AtomList from an array of atoms
            %   Creates an AtomList property from an array of atoms.
            
            obj.Atoms = Atom.empty();
            obj.Positions = zeros(3,0);
            obj.Velocities = zeros(3,0);
            obj.CurrentStates = AtomState.empty();
            obj.PreviousStates = AtomState.empty();
            obj.Indices = [];
            
            if nargin > 1
                if ~isa(atoms, 'Atom')
                    error('atoms must be of type Atom');
                end
                
%                 obj.Atoms      = atoms;
%                 obj.Positions  = [atoms.Position];
%                 obj.Velocities = [atoms.Velocity];
%                 obj.States     = [atoms.State];
%                 obj.Indices    = 1:length(obj.Atoms);
                obj.append(atoms);
            end
        end
        
        function append(atomList, atoms)
            
            if ~isa(atoms, 'Atom')
                error('atoms must be of type Atom');
            end
            
            atomList.Atoms      = [ atomList.Atoms atoms ];
            atomList.Positions = [atomList.Positions [atoms.Position]];
            atomList.Velocities = [atomList.Velocities [atoms.Velocity]];
            atomList.CurrentStates = [atomList.CurrentStates [atoms.CurrentState]];
            atomList.PreviousStates = [atomList.PreviousStates [atoms.PreviousState]];
            atomList.Indices = 1:size(atomList.Atoms);
        end
        
        function atom = getAtom(obj, index)
            %GETATOM Get atom at index.
            %   Also copies required properties into the atom, to ensure
            %   that atom is a good representation of that in the atom
            %   list.
            
            atom = obj.Atoms(index);
            if length(atom) == 1
                atom.Position = obj.Positions(:,index);
                atom.Velocity = obj.Velocities(:,index);
                atom.CurrentState = obj.CurrentStates(:,index);
                atom.PreviousState = obj.PreviousStates(:,index);
            else
                p = num2cell(obj.Positions(:,index),1);
                [atom.Position] = deal(p{:});
                v = num2cell(obj.Velocities(:,index),1);
                [atom.Velocity] = deal(v{:});
                cs = num2cell(obj.CurrentStates(:,index),1);
                [atom.CurrentState]    = deal(cs{:});
                ps = num2cell(obj.PreviousStates(:,index),1);
                [atom.PreviousState]    = deal(ps{:});
            end
            
        end
        
        function len = length(obj)
            len = length(obj.Atoms);
        end
        
        function delete(list, index)
            %DELETE Removes atoms at specified index
            
            list.getAtom(index); % forwards properties to deleted atoms
            list.Positions(:,index) = [];
            list.Velocities(:,index) = [];
            list.CurrentStates(:,index) = [];
            list.PreviousStates(:,index) = [];
            list.Atoms(index) = [];
            list.Indices(index) = [];
            
        end
        
    end
end

