classdef  Monitor < matlab.mixin.SetGet
    %MONITOR Records information on the simulation every timestep.
    
    properties
        %Geometrical parameters
        PyLength;
        PyDiameter;
        PyReflectivity;
        HoleDiameter;
        ChamberLength;
        ChamberDiameter;
        PyWallThickness;
        
        %SIMULATION Simulation this monitor observes
        Simulation;
        
        %PYRAMIDPLOTPOINTS Points of the pyramid to plot
        PyramidPlotPoints;
        
        %VTRUNCATION Truncation velocity when plotting the velocity
        %histogram
        Vtruncation;
        
        %Record the positions and velocities of atoms
        AtomsPositionsRecord;
        AtomsVelocitiesRecord;
    end
    
    methods
        
        function obj = Monitor(simulation)
            %MONITOR Creates a monitor to observe the specified simulation.
            obj.Simulation = simulation;
        end
        
        function [] = PlotAtoms(instance)
            %PLOTATOMS Plot the current positions of atoms
            Atoms = instance.Simulation.Atoms;
            if length(Atoms) == 0
                fprintf('No atoms left in the chamber \n');
            else
                AtomsPositions = Atoms.Positions;
                x = AtomsPositions(1,:);
                y = AtomsPositions(2,:);
                z = AtomsPositions(3,:);
                if(length(x) > 0)
                    plot3(instance.PyramidPlotPoints(1,:),instance.PyramidPlotPoints(2,:), instance.PyramidPlotPoints(3,:), '.k');
                    hold on;
                    plot3(x,y,z,'o');
                    xlabel('x (m)', 'fontsize', 20);
                    ylabel('y (m)', 'fontsize', 20);
                    zlabel('z (m)','fontsize', 20);
                    set(gca,'FontSize',20);
                    xlim([-instance.ChamberDiameter/(2), instance.ChamberDiameter/(2)]);
                    ylim([-instance.ChamberDiameter/(2), instance.ChamberDiameter/(2)]);
                    zlim([-instance.ChamberLength, 0.01]);
                    view([30,15]);
                    %view([0,90]);
                    hold off;
                    
                    drawnow;
                    
                end
            end
        end
        
        function UpdateAtomsStatesBeforeRecord(instance,Atoms)
            %UPDATEATOMSSTATESBEFORERECORD Update the states of atoms
            %before recording
            AtomsPositions = Atoms.Positions;
            AtomsVelocities = Atoms.Velocities;
            AtomStates = Atoms.States;
            x = AtomsPositions(1,:);
            y = AtomsPositions(2,:);
            z = AtomsPositions(3,:);
            vx = AtomsVelocities(1,:);
            vy = AtomsVelocities(2,:);
            vz = AtomsVelocities(3,:);
            
            ThetaCut = 0.2;
            IfInsidePy = ((z >= - instance.PyLength) & (z <= 0) & (x > z - instance.HoleDiameter/2) & (x < -z + instance.HoleDiameter/2) & (y > z - instance.HoleDiameter/2) & (y < -z + instance.HoleDiameter/2));
            IfComingOut  = ((z >= 0) & ((abs(x) <= instance.HoleDiameter/2)& (abs(y) <= instance.HoleDiameter/2)) & (vz > 0) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= ThetaCut))...
                & (~(AtomStates == AtomState.HasComeOutOfHole));    %the atoms going out the pyramid through the hole
            IfThoughBottomSurface = ((z >= - instance.PyLength) & (z <= - instance.PyLength + 0.0002) & ...
                (abs(x) <= instance.PyDiameter/2) & (abs(y) <= instance.PyDiameter/2) ...
                & (vz > 0)) & (~(AtomStates == AtomState.InsidePyramid));
            IfHittingChamber = ( (abs(x) > instance.ChamberDiameter/2) | (abs(y) > instance.ChamberDiameter/2) ...
                |(z < - instance.ChamberLength) | ((z > 0))) & (~((AtomStates == AtomState.HasComeOutOfHole)|(IfComingOut)));
            
            Atoms.States(IfInsidePy) = AtomState.InsidePyramid;
            Atoms.States(IfComingOut) = AtomState.ComingOutOfPyramidFromHole;
            Atoms.States(IfThoughBottomSurface) = AtomState.EnteringPyramidFromBottomSurface;
            Atoms.States(IfHittingChamber) = AtomState.HittingChamber;
        end
        
        function UpdateAtomsStatesAfterRecord(~,atomList,IfKeepOutAtoms)
            %UPDATEATOMSSTATESAFTERRECORD Update the states of atoms after
            %recording
            
            atomList.States(atomList.States == AtomState.ComingOutOfPyramidFromHole) = AtomState.HasComeOutOfHole;
            atomList.States(atomList.States == AtomState.EnteringPyramidFromBottomSurface) = AtomState.InsidePyramid;
            
            if(~IfKeepOutAtoms)
                atomList.States(atomList.States == AtomState.HasComeOutOfHole) = AtomState.IfHittingChamber;
            end
        end
        
    end
    
end

