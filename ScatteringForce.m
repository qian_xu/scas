classdef ScatteringForce < ForceField
    %SCATTERINGFORCE Scattering force of the laser beams on atoms
    %   A ScatteringForce has properties: Beams
    %   A ScatteringForce has methods: CalculateForce(Atons)
    
    properties
        %BEAMS Laser beams in the field
        Beams;   
        %MAGFIELD Magnetic field
        MagField;
        
        %IFADDRECOIL If add recoil force
        IfAddRecoil;
        
        %IFADDSCATTERINGFLUCTUATION
        IfAddScatteringFluctuation;
        
        %TIMESTEP Time step of the simulation, applied to calculte the
        %fluctuation of the scattring force and the recoil force
        TimeStep;
        
        %IFRESONANT If atoms are in resonance with all laser beams
        %simutaously
        IfResonant;     
    end
    
    methods (Static)
        function obj = Create()
            obj = ScatteringForce();
        end
    end
    
    methods
        
        function f = CalculateForce(instance, atomList)
            %CALAULATEFORCES Calculates the scattering force applied on an
            %tom
            AtomsPositions = atomList.Positions;
            AtomsVelocities = atomList.Velocities;
            x = AtomsPositions(1,:);
            y = AtomsPositions(2,:);
            z = AtomsPositions(3,:);
            
            Gamma = atomList.getAtom(1).TranLineWidth;
            I0 = atomList.getAtom(1).SatIntensity;
            
            mup = atomList.getAtom(1).Mup;
            mum = atomList.getAtom(1).Mum;
            muz = atomList.getAtom(1).Muz;
            
            hbar = 6.626e-34/(2*pi);
            
            %Calculate the magnetic field
            B = instance.MagField.CalculateMagField(x,y,z);
            Br = sqrt(sum(B.^2,1));
         
            %Calculate the total forces from all laser beams
            
            ScatteringRateList = zeros(length(instance.Beams),length(atomList));
            %fList = zeros(1,length(instance.Beams));
            
            for BeamIndex = 1:length(instance.Beams)
                S = instance.Beams{BeamIndex}.CalculateIntensity(x,y,z)/I0;   %Calculate the saturation
                delta = instance.Beams{BeamIndex}.Detuning;     %Get the detuning 
                k = 2*pi/instance.Beams{BeamIndex}.WaveLength;    %Get the wavevector
                direction = instance.Beams{BeamIndex}.Direction;   %Get the direction
                p = instance.Beams{BeamIndex}.Polarization;
                Costheta = transpose(instance.Beams{BeamIndex}.Direction)*B./(norm(instance.Beams{BeamIndex}.Direction)*Br); %Calculate the angle between the beam direction and the quantization axis
                
                %Calculate the scattering rate
                ScatteringRate = (1/4*(p*Costheta + 1).^2*Gamma/2./(1+S+4*(delta - k*transpose(direction)*AtomsVelocities - mup/hbar*Br).^2/Gamma^2) ...
                + 1/4*(p*Costheta - 1).^2*Gamma/2./(1+S+4*(delta - k*transpose(direction)*AtomsVelocities - mum/hbar*Br).^2/Gamma^2) ...
                +1/2*(1-(Costheta).^2)*Gamma/2./(1+S+4*(delta - k*transpose(direction)*AtomsVelocities - muz/hbar*Br).^2/Gamma^2)).*S;
 
                if(instance.IfAddScatteringFluctuation == true)
                    %Add fluctuations in the ScatteringRate
                    ScatteringRateFlucDiec= randi([-1,1],1,length(ScatteringRate));
                    ScatteringRate = ScatteringRate + abs(ScatteringRateFlucDiec.*sqrt(ScatteringRate*instance.TimeStep)/instance.TimeStep);                   
                end
                           
                ScatteringRateList(BeamIndex, :) = ScatteringRate;
            end
            
            TotalScatteringRate = sum(ScatteringRateList, 1) + 0.01;
            
            f = 0;
            for BeamIndex = 1:length(instance.Beams)
                k = 2*pi/instance.Beams{BeamIndex}.WaveLength;    %Get the wavevector
                direction = instance.Beams{BeamIndex}.Direction;   %Get the direction
                if(instance.IfResonant == 1)
                    f = f + hbar*k*direction*(ScatteringRateList(BeamIndex,:).*ScatteringRateList(BeamIndex,:)./TotalScatteringRate);  %Add up forces according to the weight of scattering rate
                else
                    f = f + hbar*k*direction*ScatteringRateList(BeamIndex,:);
                end
            end
            
            if(instance.IfAddRecoil == true)
                %Add recoil force
                ThetaRecoil = rand(1,length(atomList))*pi;
                PhiRecoil = rand(1,length(atomList))*2*pi;
                fRecoil = [hbar*k*sqrt(TotalScatteringRate*instance.TimeStep)/instance.TimeStep.*sin(ThetaRecoil).*sin(PhiRecoil);
                    hbar*k*sqrt(TotalScatteringRate*instance.TimeStep)/instance.TimeStep.*sin(ThetaRecoil).*cos(PhiRecoil);
                    hbar*k*sqrt(TotalScatteringRate*instance.TimeStep)/instance.TimeStep.*cos(ThetaRecoil)];
                f = f +fRecoil;
            end
            
        end   
            
    end
    
end

