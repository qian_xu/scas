classdef GaussianBeam <  LaserBeam
    %GAUSSIANBEAM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %BEAMWAIST Beam waists of the Gaussian beam along two orthogonal
        %axis
        BeamWaist1;
        BeamWaist2;
        %THETA Rotation angle along z axis
        Theta;
        %PHI Rotation angle along x(y) axis
        Phi;
        %DSHIFT Laser center shift dut to the existence of the hole
        Dshift;
        
       
    end
    
    methods (Static)
         function obj = Create(power, wavelength, detuning, beamwaist1, beamwaist2, theta, phi, dshift)
             %CREATE Craate an BeamWaist object with relevant parameters
             obj = GaussianBeam();
             obj.Power = power;
             obj.BeamWaist1 = beamwaist1;
             obj.BeamWaist2 = beamwaist2;
             obj.Theta = theta;
             obj.Phi = phi;
             obj.Dshift = dshift;
             obj.WaveLength = wavelength;
             obj.Detuning = detuning;
         end
    end
    methods
         function I = CalculateIntensity(instance, x,y,z)
            %CALCULATEINTENSITY Calculate the spacial intensity of the Gussian beam
            zR = 20;
            x0 = cos(instance.Phi)*x - sin(instance.Phi)*y;
            y0 = cos(instance.Theta)*sin(instance.Phi)*x + cos(instance.Phi)*cos(instance.Theta)*y  - sin(instance.Theta)*z + instance.Dshift;
            z0 = sin(instance.Theta)*sin(instance.Phi)*x + sin(instance.Theta)*cos(instance.Phi)*y + cos(instance.Theta)*z;

            Dx = instance.BeamWaist1*sqrt(1+(z0.^2)/zR^2);
            Dy = instance.BeamWaist2*sqrt(1+(z0.^2)/zR^2);

            I = instance.Power./(pi/2*Dx.*Dy).*exp(-2*x0.^2./Dx.^2 -2*y0.^2./Dy.^2);
        end
            
    end
    
end

