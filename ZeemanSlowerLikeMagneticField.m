classdef ZeemanSlowerLikeMagneticField < MagneticField
    %ZEEMANSLOWERLIKEMAGNETICFIELD Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        L;
        LobbyLength;
        am;
        v0;
        Vout;
        
        %GRADIENT Linear gradient of the field
        Gradient;
        
        %FIELDCENTER Field center (zero magnetic field position)
        FieldCenter;
        
        %WAVEVECTOR WaveVector of the laser
        WaveVector;
        
        %MU Effective magnetic moment
        Mu;
        
        %DETUNING Laser detuning
        Detuning;
        
    end
    
    methods
        function B = CalculateMagField(instance, x, y, z)
            %CALCULATEMAGFIELD Return the vector form of the magnetic field
            %given spacial coordinate
            hbar = 6.626e-34/(2*pi);
            BLinear = instance.Gradient*[-(x - instance.FieldCenter(1)); -(y - instance.FieldCenter(2)); 2*(z - instance.FieldCenter(3))];
            %BZeemanSlower = [- instance.Gradient*(x - instance.FieldCenter(1)); - instance.Gradient*(y - instance.FieldCenter(2)); ...
                 %- 1/(instance.Mu/hbar)*(instance.WaveVector*instance.v0 + instance.Detuning) - instance.WaveVector/(instance.Mu/hbar)*(sqrt(instance.v0^2 - 2*instance.am*(z + instance.L)) - ...
                %instance.v0)];
            BZeemanSlower = [- instance.Gradient*(x - instance.FieldCenter(1)); - instance.Gradient*(y - instance.FieldCenter(2)); ...
                 - 1/(instance.Mu/hbar)*(instance.Detuning + instance.WaveVector*sqrt(instance.Vout.^2 - 2*instance.am*(z + instance.LobbyLength)) )];
        
            B = [heaviside(z + instance.LobbyLength).*BLinear(1,:) + heaviside( - z - instance.LobbyLength).*BZeemanSlower(1,:);...
                heaviside(z + instance.LobbyLength).*BLinear(2,:) +  heaviside( - z - instance.LobbyLength).*BZeemanSlower(2,:);...
                heaviside(z + instance.LobbyLength).*BLinear(3,:) +  heaviside( - z - instance.LobbyLength).*BZeemanSlower(3,:)];
        end
    end    
end

