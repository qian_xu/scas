function [ jtheta] = j_theta( theta )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

r = 0.2e-3;
L = 4e-3;
beta = 2*r/L;
q_theta = 1/beta*tan(theta);
R_theta = acos(q_theta) - q_theta.*sqrt(1-q_theta.^2);
alpha = 1/2 - 1/(3*beta^2)*(1-2*beta^3 + (2*beta^2 - 1)*sqrt(1+beta^2))/(sqrt(1+beta^2) - beta^2*asinh(1/beta));
j1_theta = alpha*cos(theta) + 2/pi*cos(theta).*((1-alpha)*R_theta + 2./(3*q_theta).*(1-2*alpha).*(1-(1-q_theta.^2).^(3/2)));
j2_theta = alpha*cos(theta) + 4./(3*pi*q_theta)*(1-2*alpha).*cos(theta);
jtheta = (j1_theta.*heaviside(1-q_theta)  + j2_theta.*heaviside(q_theta-1))*2*pi.*sin(theta);
end

