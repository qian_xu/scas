function [ f ] = VDF( v )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%define the parameters
kB =  1.38e-23;
T = 500 + 273;
m = 1.44e-25;
vp = sqrt(2*kB*T/m);

f = 2*v.^3/vp^4.*exp(-v.^2/vp^2);

end





