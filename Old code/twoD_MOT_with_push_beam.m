%%
%simulation with conical beams
%define the parameters
m = 1.44e-25;
%m = 132.905*10^(-3)/(6.02*10^(23));
lambda = 461e-9;
k = 2*pi/lambda;
Gamma = 2*pi*32e6;
S = 1; %(saturation parameters)
delta = - Gamma;

muB = 9.27400e-24;
g_p = - 1;           %error
g_m = 1;
g_z = 0;

mup = g_p*muB;
mum = g_m*muB;
muz = g_z*muB;

hbar = 6.626e-34/(2*pi);

N1_atom = 100;
N2_atom = 100;
kB =  1.38e-23;
N_atom = N1_atom*N2_atom;

%set parameters about the Gaussian beam
I0 = 430;
%P = 200e-3;
%Dx0 = 16.5e-3;
%Dy0 = 16.5e-3;

%define the atom beam radius
r_beam = 0.2e-3;

%define the chamber radius (capture volume)
r_chamber = 30e-3;

%define the radius of the output tube
r_tube = 18.5e-3;

%distance between the atom source and the chamber
ds = 83e-3;

%initialize the atoms according to the VDF
%v_truncation = sqrt(2*hbar*k*Gamma/2/m*2*r0);   %set the truncation velocity
v_truncation = 120;     %set the truncation velocity
vmax = v_truncation;

%visualize the VDF & find the normalization value
v = linspace(0, vmax, 600);
fv = VDF(v);
fmax = max(fv);
%plot(v,fv,'.');

%find the normalization value of j(theta)
thetamax = asin(r_chamber/0.09);
theta_test = linspace(0, pi/2, 100);
f_theta_test = j_theta(theta_test);
jmax = max(f_theta_test);

y00 = linspace(-r_beam,r_beam,N1_atom);
z00 = linspace(-r_beam,r_beam,N2_atom);
[y00,z00] = meshgrid(y00,z00);
y00 = reshape(y00,1,N1_atom*N2_atom);
z00 = reshape(z00,1,N1_atom*N2_atom);
trunc_index = find(sqrt(y00.^2+z00.^2) > r_beam);
y00(trunc_index) = [];
z00(trunc_index) = [];
N_atom = N1_atom*N2_atom - length(trunc_index);


velo = 5;

x00 = -ones(1,N_atom)*ds;

%rotate pi/4
phi0 = pi/4;
x0 = cos(phi0)*x00 - sin(phi0)*y00;
y0 = sin(phi0)*x00 + cos(phi0)*y00;
z0 = z00;

vx00 = zeros(1,N_atom);
vy00 = zeros(1,N_atom);
vz00 = zeros(1,N_atom);

theta00 = zeros(1,N_atom);
v00 = zeros(1,N_atom);


%most importantly, initialize the beam velocity
for atom_index = 1:N_atom
    v_random = rand()*vmax;
    p = VDF(v_random)/fmax;
    s = rand();
    while(s > p)
        v_random = rand()*vmax;
        p = VDF(v_random)/fmax;
        s = rand();
    end
    theta_random = rand()*thetamax;
    p = j_theta(theta_random)/jmax;
    s = rand();
    while(s > p)
        theta_random = rand()*thetamax;
        p = j_theta(theta_random)/jmax;
        s = rand();
    end
    
    phi_random = rand()*2*pi;
    v00(atom_index) = v_random;
    theta00(atom_index) = theta_random;
    
    vx00(1,atom_index) = v_random*cos(theta_random);
    vy00(1,atom_index) = v_random*sin(theta_random)*cos(phi_random);
    vz00(1,atom_index) = v_random*sin(theta_random)*sin(phi_random);
    
end
figure;
histogram(vx00,100);

vx0 = cos(phi0)*vx00 - sin(phi0)*vy00;
vy0 = sin(phi0)*vx00 + cos(phi0)*vy00;
vz0 = vz00;


capture_proportion = length(vx0(vx0 <= 40))/N_atom

%calculate the capture rate

%set the parameters \delta and B'
delta_variable = -2;    %(per Gamma)
beta_variable = 20;    %(per G/cm)
delta = delta_variable*Gamma;
beta = beta_variable*0.01;


%set time step and steps
dt = r_chamber/5/abs(velo)/900;
t_total = 0.08;
step = round(t_total/dt);


f1 =@(v) VDF(v);
Fvc = integral(f1,0,vmax);

f2 =@(theta) j_theta(theta);
Fthetac = integral(f2,0,thetamax)/integral(f2,0,pi/2);


%calculate the total flux
r = 0.2e-3;
L = 4e-3;
beta_0 = 2*r/L;
alpha_0 = 1/2 - 1/(3*beta_0^2)*(1-2*beta_0^3 + (2*beta_0^2 - 1)*sqrt(1+beta_0^2))/(sqrt(1+beta_0^2) - beta_0^2*asinh(1/beta_0));
W = 1 + 2/3*(1-2*alpha_0)*[beta_0 - sqrt(1+beta_0^2)] + 2/3*(1+alpha_0)*(1-sqrt(1+beta_0^2))/beta_0^2;
T = 500 + 273;
P = 10^(7.753 - 7628/T);
d_hole = 4e-3;
A = 76*pi*d_hole^2/4;
Phi_total = W*P*A/sqrt(2*pi*m*kB*T);


%calculate the cross section
vrms = 2*sqrt(kB*T/m);
nmax = 1/(4*pi)*P*A/(kB*T)/(ds^2);
sigma_collision = 1.1e-18;
tau_collision_center = 1/(nmax*vrms*sigma_collision);

%%
%calculate the mean density
theta_calculated = thetamax;

g1 =@(theta) j_theta(theta);
temp1 = integral(g1,0,theta_calculated);

g2 = @(theta) 2*pi*sin(theta);
temp2 = integral(g2,0,theta_calculated);

ratio = temp1/temp2;
tau_collision = tau_collision_center*1/ratio




%%
%single run
dt = r_chamber/abs(velo)/900;
t_total = 0.1;
step = round(t_total/dt);

delta_variable = -3.9;    %(per Gamma)
beta_variable = 20;    %(per G/cm)
delta = delta_variable*Gamma;
beta = beta_variable*0.01;

P = 200e-3;
Dx0 = 22e-3;
Dy0 = 14.5e-3;

%push beam
P_p = 2e-3;
Dx0_p = 1.65e-3;
Dy0_p = 1.65e-3;
delta_p = 0;

x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

if_out = zeros(1,N_atom);
if_ever_in_chamber = zeros(1,N_atom);
in_time = zeros(1,N_atom);
out_time = zeros(1,N_atom);

time_step = 5;

out_num = 0;
run_time = 0;

x_record = zeros(round(step/time_step),N_atom);
y_record = zeros(round(step/time_step),N_atom);
z_record = zeros(round(step/time_step),N_atom);

current_time = 0;


for j = 1:round(step/time_step)
for i = 1:time_step
    
        %record the in_time and out_time
        in_time((sqrt(x.^2 + y.^2 + z.^2) <= r_chamber)&(~if_ever_in_chamber)) = current_time;
        out_time((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= 0.2) & (~if_out)) = current_time;
        %judge if the atoms enter the output tube
        if_out((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= 0.2)) = 1;
        
        %judge it the atoms have entered the chamber
        if_ever_in_chamber(sqrt(x.^2 + y.^2 + z.^2) <= r_chamber) = 1;
        
        
        
        
        %discard the atoms sticking to the chamber wall
        xi = x*cos(-phi0) - y*sin(-phi0);
        stick_index = find((sqrt(x.^2 + y.^2 + z.^2) >= r_chamber) & (if_ever_in_chamber)  & (~if_out));
        x(stick_index) = [];
        y(stick_index) = [];
        z(stick_index) = [];
        vx(stick_index) = [];
        vy(stick_index) = [];
        vz(stick_index) = [];
        if_out(stick_index) = [];
        if_ever_in_chamber(stick_index) = [];
        in_time(stick_index) = [];
        out_time(stick_index) = [];
        
        x_record(:,stick_index) = [];
        y_record(:,stick_index) = [];
        z_record(:,stick_index) = [];
        
        costhetax = -x./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetay = -y./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetaz = 2*z./sqrt(x.^2 + y.^2 + 4*z.^2);
 
        Br = beta*sqrt(x.^2+y.^2 + 4*z.^2);

        Sx = Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0)/I0;
        Sy = Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0)/I0;
        Sz = Gaussion_intensity(x,y,z,0,0,P_p, Dx0_p,Dy0_p)/I0;
        S_t = 2*(Sx + Sy) + Sz;

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+S_t+4*(delta_p - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
           
        R_t = Rxp + Rxm + Ryp + Rym + Rzp;
        
        
        theta_recoil = rand(1,length(x))*pi;
        phi_recoil = rand(1,length(x))*2*pi;
        
        direcx = randi([-1,1],1,length(x));
        direcy = randi([-1,1],1,length(x));
        direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm);  %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp);  %+ hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
        
        Fx(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fy(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fz(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
end

if((run_time >= 0.02)&& (length(if_out(if_out == 1)) == out_num))
    break;
else
    out_num = length(if_out(if_out == 1));
    run_time = j*time_step*dt;
    x_record(j,:) = x;
    y_record(j,:) = y;
    z_record(j,:) = z;
    
end


end
figure;
plot3(x_record, y_record, z_record,'.');
xlabel('m');
ylabel('m');
xlim([-0.06,0.03]);
ylim([-0.06,0.03]);
zlim([-0.03,0.06]);

title('Trajectories of output beam @ \delta =  - 1.5\Gamma, B = 20 G/cm, P = 200 mW, D_{xy} = 22 mm, D_z = 14.5 mm, P_p = 2 mW, D_p = 1.65 mm')

%figure;
%plot(theta00(cap_index)*180/(pi),v00(cap_index),'o');
%xlim([0,20]);
%ylim([0,120])
%xlabel('\theta_0');
%ylabel('v_0');
%title('P = 25 mW, D = 6mm, \delta = - 1.9\Gamma, B = 20 G/cm')

vxi = vx*cos(-phi0) - vy*sin(-phi0);
figure;
histogram(atan(vxi./vz),100);
xlabel('\theta_{\xi}');
title('\theta distribution @ \delta =  - 3\Gamma, B = 19 G/cm, P = 200 mW, D_{xy} = 22 mm, D_z = 14.5 mm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',10)
figure;
histogram(sqrt(vx.^2 + vy.^2 + vz.^2),100);
xlabel('v');
title('v distribution @ \delta =  - 3\Gamma, B = 19 G/cm, P = 200 mW, D_{xy} = 22 mm, D_z = 14.5 mm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',10)
flux = out_num/N_atom*Fvc*Fthetac*Phi_total
run_time
std(atan(vxi./vz))

figure;
histogram(out_time - in_time, 100);
xlabel('\tau_{push} (s)','fontsize',20);
title('\tau_{push} @ \delta =  - 3\Gamma, B = 19 G/cm, P = 200 mW, D_{xy} = 22 mm, D_z = 14.5 mm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',10)
tau_push = mean(out_time - in_time)
%%
%find optimal P and D
%set the parameters delta and B'

delta = - 1.5*Gamma;
beta = 20*0.01;
velo = 5;

t_total = 0.1;

dt = 1/800*r_chamber/velo;
step = round(t_total/dt);

time_step = 800;

out_num = 0;
run_time = 0;

P_p = 0.2e-3;
Dx0_p = 0.7e-3;
Dy0_p = 0.7e-3;

%set the parameters list 
scan_P_list = linspace(5, 800, 15);
scan_D_list = linspace(5, 30, 8);
%fixed_D = 16.5;
%fixed_P = 400;


scan_flux = zeros(length(scan_D_list), length(scan_P_list));
scan_run_time = zeros(length(scan_D_list), length(scan_P_list));
scan_velocity = zeros(length(scan_D_list), length(scan_P_list));
% = zeros(1,length(scan_D_list));

for P_index = 1:length(scan_P_list)    %the loop of changing parameters to be optimzed
for D_index = 1:length(scan_D_list)
   
P = scan_P_list(P_index)*1e-3;
D = scan_D_list(D_index)*1e-3;
Dx0 = D;
Dy0 = D;
    
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

if_out = zeros(1,N_atom);
if_ever_in_chamber = zeros(1,N_atom);


for j = 1:round(step/time_step)
for i = 1:time_step
    
    
        %judge if the atoms enter the output tube
        if_out((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10)) = 1;
        
        %judge it the atoms have entered the chamber
        if_ever_in_chamber(sqrt(x.^2 + y.^2 + z.^2) <= r_chamber) = 1;
        %discard the atoms sticking to the chamber wall
        xi = x*cos(-phi0) - y*sin(-phi0);
        stick_index = find((sqrt(x.^2 + y.^2 + z.^2) >= r_chamber) & (if_ever_in_chamber)  & (~if_out));
        x(stick_index) = [];
        y(stick_index) = [];
        z(stick_index) = [];
        vx(stick_index) = [];
        vy(stick_index) = [];
        vz(stick_index) = [];
        if_out(stick_index) = [];
        if_ever_in_chamber(stick_index) = [];
        
  
        
        costhetax = -x./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetay = -y./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetaz = 2*z./sqrt(x.^2 + y.^2 + 4*z.^2);
 
        Br = beta*sqrt(x.^2+y.^2 + 4*z.^2);

        Sx = Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0)/I0;
        Sy = Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0)/I0;
        Sz = Gaussion_intensity(x,y,z,0,0,P_p, Dx0_p,Dy0_p)/I0;
        S_t = 2*(Sx + Sy) + Sz;

        Fx = (1/4*(costhetax + 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)...
        - 1/4*(- costhetax + 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        - 1/4*(- costhetax - 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        - 1/2*(1-(costhetax).^2)*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;

        Fy = (1/4*(costhetay + 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)...
        - 1/4*(- costhetay + 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        - 1/4*(- costhetay - 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        - 1/2*(1-(costhetay).^2)*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
 
        %Fz = (1/4*(costhetaz - 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        %+ 1/4*(costhetaz + 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        %+1/2*(1-(costhetaz).^2)*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vz - muz/hbar*Br).^2/Gamma^2)...
       %- 1/4*(- costhetaz - 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vz - mup/hbar*Br).^2/Gamma^2)...
        %- 1/4*(- costhetaz + 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vz - mum/hbar*Br).^2/Gamma^2)...
        %-1/2*(1-(costhetaz).^2)*hbar*k*Gamma/2./(1+S_t+4*(delta + k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
    
        Fz = (1/4*(costhetaz - 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*hbar*k*Gamma/2./(1+S_t+4*(delta - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
   
        Fx(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fy(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fz(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
end

if((run_time >= 0.01)&& (length(if_out(if_out == 1)) == out_num))
    break;
else
    out_num = length(if_out(if_out == 1));
    run_time = j*time_step*dt;
end

end

flux = out_num/N_atom*Fvc;

scan_flux( D_index, P_index) = flux;
scan_run_time(D_index, P_index) = run_time;
v = sqrt(vx.^2 + vy.^2 + vz.^2);
scan_velocity(D_index, P_index) = mean(v(if_out == 1));



%cap_rate_list(end + 1) = cal_rate;

end
end


[scan_P_list, scan_D_list] = meshgrid(scan_P_list, scan_D_list);
figure;
plot3(scan_P_list, scan_D_list, scan_flux,'o');

%%
%%
%1-d scan
%find optimal beam ratio
%set the parameters delta and B'

delta = - 3.1*Gamma;
beta = 20*0.01;
velo = 5;

t_total = 0.1;

dt = 1/800*r_chamber/velo;
step = round(t_total/dt);

time_step = 1000;

out_num = 0;
run_time = 0;

P_p = 2e-3;
Dx0_p = 1.65e-3;
Dy0_p = 1.65e-3;
delta_p = 0;

%set the parameters list 
scan_beam_ratio_list = linspace(0.5, 4, 20);
%fixed_D = 16.5;
fixed_P = 200;
S_fixed = (16.5e-3)*(16.5e-3);

scan_flux = zeros(1,length(scan_D_list));
scan_mean_velocity = zeros(1,length(scan_D_list));
scan_angle_div = zeros(1,length(scan_D_list));
scan_push_time = zeros(1,length(scan_D_list));

% = zeros(1,length(scan_D_list));

%the loop of changing parameters to be optimzed
for beam_ratio_index = 1:length(scan_beam_ratio_list)
   
P = fixed_P*1e-3;
beam_ratio = scan_beam_ratio_list(beam_ratio_index);
Dx0 = sqrt(beam_ratio*S_fixed);
Dy0 = sqrt(1/beam_ratio*S_fixed);
    
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

if_out = zeros(1,N_atom);
if_ever_in_chamber = zeros(1,N_atom);
in_time = zeros(1,N_atom);
out_time = zeros(1,N_atom);

current_time = 0;

for j = 1:round(step/time_step)
for i = 1:time_step
    
        %record the in_time and out_time
        in_time((sqrt(x.^2 + y.^2 + z.^2) <= r_chamber)&(~if_ever_in_chamber)) = current_time;
        out_time((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10) & (~if_out)) = current_time;
    
        %judge if the atoms enter the output tube
        if_out((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10)) = 1;
        
        %judge it the atoms have entered the chamber
        if_ever_in_chamber(sqrt(x.^2 + y.^2 + z.^2) <= r_chamber) = 1;
        %discard the atoms sticking to the chamber wall
        xi = x*cos(-phi0) - y*sin(-phi0);
        stick_index = find((sqrt(x.^2 + y.^2 + z.^2) >= r_chamber) & (if_ever_in_chamber)  & (~if_out));
        x(stick_index) = [];
        y(stick_index) = [];
        z(stick_index) = [];
        vx(stick_index) = [];
        vy(stick_index) = [];
        vz(stick_index) = [];
        if_out(stick_index) = [];
        if_ever_in_chamber(stick_index) = [];
        in_time(stick_index) = [];
        out_time(stick_index) = [];
       
        
        costhetax = -x./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetay = -y./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetaz = 2*z./sqrt(x.^2 + y.^2 + 4*z.^2);
 
        Br = beta*sqrt(x.^2+y.^2 + 4*z.^2);

        Sx = Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0)/I0;
        Sy = Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0)/I0;
        Sz = Gaussion_intensity(x,y,z,0,0,P_p, Dx0_p,Dy0_p)/I0;
        S_t = 2*(Sx + Sy) + Sz;

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+S_t+4*(delta_p - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
           
        R_t = Rxp + Rxm + Ryp + Rym + Rzp;
        
        
        %theta_recoil = rand(1,length(x))*pi;
        %phi_recoil = rand(1,length(x))*2*pi;
        
        %direcx = randi([-1,1],1,length(x));
        %direcy = randi([-1,1],1,length(x));
        %direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp); %+ hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
        
        Fx(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fy(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fz(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
end

if((run_time >= 0.03)&& (length(if_out(if_out == 1)) == out_num))
    break;
else
    out_num = length(if_out(if_out == 1));
    run_time = j*time_step*dt;
end

end

tau_push = mean(out_time - in_time);


flux = out_num/N_atom*Fvc*Fthetac*Phi_total;

scan_flux(beam_ratio_index) = flux;
scan_push_time(beam_ratio_index) = tau_push;

v = sqrt(vx.^2 + vy.^2 + vz.^2);
scan_mean_velocity(beam_ratio_index) = mean(v);
vxi = vx*cos(-phi0) - vy*sin(-phi0);
scan_angle_div(beam_ratio_index) = std(atan(vxi./vz));
scan_velocity_div = std(v);

%cap_rate_list(end + 1) = cal_rate;

end

figure;
plot(scan_beam_ratio_list, scan_flux,'o');
xlabel('D (mm)','fontsize',18);
ylabel('R_{loading} (s^{-1})','fontsize',18 );
title('R_{loading} @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',14);

figure;
plot(scan_beam_ratio_list, scan_angle_div,'o');
xlabel('D (mm)','fontsize',18);
ylabel('\sigma_{\theta}','fontsize',18 );
title('\sigma_{\theta} @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);

figure;
plot(scan_beam_ratio_list, scan_mean_velocity,'o');
xlabel('D (mm)','fontsize',18);
ylabel('mean(v) (m/s)','fontsize',18 );
title('mean(v) (m/s) @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);


%%
%1-d scan
%find D
%set the parameters delta and B'

delta = - 3*Gamma;
beta = 20*0.01;
velo = 5;

t_total = 0.1;

dt = 1/800*r_chamber/velo;
step = round(t_total/dt);

time_step = 1000;

out_num = 0;
run_time = 0;

P_p = 2e-3;
Dx0_p = 1.65e-3;
Dy0_p = 1.65e-3;
delta_p = 0;

%set the parameters list 
scan_D_list = linspace(5, 30, 10);
%fixed_D = 16.5;
fixed_P = 200;


scan_flux = zeros(1,length(scan_D_list));
scan_mean_velocity = zeros(1,length(scan_D_list));
scan_angle_div = zeros(1,length(scan_D_list));
scan_mean_velocity_div = zeros(1,length(scan_D_list));
scan_push_time = zeros(1,length(scan_D_list));

% = zeros(1,length(scan_D_list));

%the loop of changing parameters to be optimzed
for D_index = 1:length(scan_D_list)
   
P = fixed_P*1e-3;
D = scan_D_list(D_index)*1e-3;
Dx0 = D;
Dy0 = D;
    
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

if_out = zeros(1,N_atom);
if_ever_in_chamber = zeros(1,N_atom);
in_time = zeros(1,N_atom);
out_time = zeros(1,N_atom);

current_time = 0;

for j = 1:round(step/time_step)
for i = 1:time_step
    
        %record the in_time and out_time
        in_time((sqrt(x.^2 + y.^2 + z.^2) <= r_chamber)&(~if_ever_in_chamber)) = current_time;
        out_time((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10) & (~if_out)) = current_time;
    
        %judge if the atoms enter the output tube
        if_out((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10)) = 1;
        
        %judge it the atoms have entered the chamber
        if_ever_in_chamber(sqrt(x.^2 + y.^2 + z.^2) <= r_chamber) = 1;
        %discard the atoms sticking to the chamber wall
        xi = x*cos(-phi0) - y*sin(-phi0);
        stick_index = find((sqrt(x.^2 + y.^2 + z.^2) >= r_chamber) & (if_ever_in_chamber)  & (~if_out));
        x(stick_index) = [];
        y(stick_index) = [];
        z(stick_index) = [];
        vx(stick_index) = [];
        vy(stick_index) = [];
        vz(stick_index) = [];
        if_out(stick_index) = [];
        if_ever_in_chamber(stick_index) = [];
        in_time(stick_index) = [];
        out_time(stick_index) = [];
       
        
        costhetax = -x./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetay = -y./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetaz = 2*z./sqrt(x.^2 + y.^2 + 4*z.^2);
 
        Br = beta*sqrt(x.^2+y.^2 + 4*z.^2);

        Sx = Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0)/I0;
        Sy = Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0)/I0;
        Sz = Gaussion_intensity(x,y,z,0,0,P_p, Dx0_p,Dy0_p)/I0;
        S_t = 2*(Sx + Sy) + Sz;

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+S_t+4*(delta_p - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
           
        R_t = Rxp + Rxm + Ryp + Rym + Rzp;
        
        
        %theta_recoil = rand(1,length(x))*pi;
        %phi_recoil = rand(1,length(x))*2*pi;
        
        %direcx = randi([-1,1],1,length(x));
        %direcy = randi([-1,1],1,length(x));
        %direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp); %+ hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
        
        Fx(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fy(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fz(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
end

if((run_time >= 0.03)&& (length(if_out(if_out == 1)) == out_num))
    break;
else
    out_num = length(if_out(if_out == 1));
    run_time = j*time_step*dt;
end

end

tau_push = mean(out_time - in_time);


flux = out_num/N_atom*Fvc*Fthetac*Phi_total;

scan_flux(D_index) = flux;
scan_push_time(D_index) = tau_push;

scan_run_time(D_index) = run_time;
v = sqrt(vx.^2 + vy.^2 + vz.^2);
scan_mean_velocity(D_index) = mean(v);
vxi = vx*cos(-phi0) - vy*sin(-phi0);
scan_angle_div(D_index) = std(atan(vxi./vz));
scan_velocity_div = std(v);

%cap_rate_list(end + 1) = cal_rate;

end

figure;
plot(scan_D_list, scan_flux,'o');
xlabel('D (mm)','fontsize',18);
ylabel('R_{loading} (s^{-1})','fontsize',18 );
title('R_{loading} @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',14);

figure;
plot(scan_D_list, scan_angle_div,'o');
xlabel('D (mm)','fontsize',18);
ylabel('\sigma_{\theta}','fontsize',18 );
title('\sigma_{\theta} @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);

figure;
plot(scan_D_list, scan_mean_velocity,'o');
xlabel('D (mm)','fontsize',18);
ylabel('mean(v) (m/s)','fontsize',18 );
title('mean(v) (m/s) @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);


%%
%2-d scan
%find optimal B and delta


velo = 5;

t_total = 0.1;

dt = 1/800*r_chamber/velo;
step = round(t_total/dt);

time_step = 1000;

out_num = 0;
run_time = 0;

P_p = 2e-3;
Dx0_p = 1.65e-3;
Dy0_p = 1.65e-3;
delta_p = 0;

%set the parameters list 
fixed_Dxy = 22;
fixed_Dz = 14.5;
%fixed_D = 16.5;
fixed_P = 200;

scan_delta = linspace(2,4,10);
scan_B = linspace(12,30,12);

scan_flux = zeros(length(scan_B),length(scan_delta));
scan_mean_velocity = zeros(length(scan_B),length(scan_delta));
scan_angle_div = zeros(length(scan_B),length(scan_delta));
scan_mean_angle = zeros(length(scan_B),length(scan_delta));

% = zeros(1,length(scan_D_list));

%the loop of changing parameters to be optimzed
for delta_index = 1:length(scan_delta)
for B_index = 1:length(scan_B)
   
delta = scan_delta(delta_index)*(-Gamma);
beta = scan_B(B_index)*0.01;
    
P = fixed_P*1e-3;
Dx0 = fixed_Dxy*1e-3;
Dy0 = fixed_Dz*1e-3;
    
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

if_out = zeros(1,N_atom);
if_ever_in_chamber = zeros(1,N_atom);


for j = 1:round(step/time_step)
for i = 1:time_step
    
    
        %judge if the atoms enter the output tube
        if_out((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= 0.2) ) = 1;
        
        %judge it the atoms have entered the chamber
        if_ever_in_chamber(sqrt(x.^2 + y.^2 + z.^2) <= r_chamber) = 1;
        %discard the atoms sticking to the chamber wall
        xi = x*cos(-phi0) - y*sin(-phi0);
        stick_index = find((sqrt(x.^2 + y.^2 + z.^2) >= r_chamber) & (if_ever_in_chamber)  & (~if_out));
        x(stick_index) = [];
        y(stick_index) = [];
        z(stick_index) = [];
        vx(stick_index) = [];
        vy(stick_index) = [];
        vz(stick_index) = [];
        if_out(stick_index) = [];
        if_ever_in_chamber(stick_index) = [];
        
       
        
        costhetax = -x./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetay = -y./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetaz = 2*z./sqrt(x.^2 + y.^2 + 4*z.^2);
 
        Br = beta*sqrt(x.^2+y.^2 + 4*z.^2);

        Sx = Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0)/I0;
        Sy = Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0)/I0;
        Sz = Gaussion_intensity(x,y,z,0,0,P_p, Dx0_p,Dy0_p)/I0;
        S_t = 2*(Sx + Sy) + Sz;

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+S_t+4*(delta_p - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
           
        R_t = Rxp + Rxm + Ryp + Rym + Rzp;
        
        
        %theta_recoil = rand(1,length(x))*pi;
        %phi_recoil = rand(1,length(x))*2*pi;
        
        %direcx = randi([-1,1],1,length(x));
        %direcy = randi([-1,1],1,length(x));
        %direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp); %+ hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
        
        Fx(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fy(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fz(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
end

if((run_time >= 0.03)&& (length(if_out(if_out == 1)) == out_num))
    break;
else
    out_num = length(if_out(if_out == 1));
    run_time = j*time_step*dt;
end

end


flux = out_num/N_atom*Fvc*Fthetac*Phi_total;

scan_flux(B_index, delta_index) = flux;
v = sqrt(vx.^2 + vy.^2 + vz.^2);
scan_velocity(B_index, delta_index) = mean(v);
vxi = vx*cos(-phi0) - vy*sin(-phi0);
scan_angle_div(B_index, delta_index) = std(atan(vxi./vz));
scan_velocity_div = std(v);
scan_mean_angle(B_index, delta_index) = mean(atan(vxi./vz));

%cap_rate_list(end + 1) = cal_rate;

end
end
[scan_delta, scan_B] = meshgrid(scan_delta, scan_B);

figure;
plot3(scan_delta, scan_B, scan_flux,'o');
xlabel('D (mm)','fontsize',18);
ylabel('flux','fontsize',18 );
title('flux @ P = 200 mW, \delta = - 1.5 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',14);

figure;
plot3(scan_delta, scan_B,'o');
xlabel('D (mm)','fontsize',18);
ylabel('\sigma_{\theta}','fontsize',18 );
title('\sigma_{\theta} @ P = 200 mW, \delta = - 1.5 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);

figure;
plot3(scan_delta, scan_B,'o');
xlabel('D (mm)','fontsize',18);
ylabel('v (m/s)','fontsize',18 );
title('v (m/s) @ P = 200 mW, \delta = - 1.5 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);


%%
%2-d scan
%find optimal B and delta
%calculate flux

tau_collision = 0.008;

velo = 5;

t_total = 0.1;

dt = 1/800*r_chamber/velo;
step = round(t_total/dt);

time_step = 1000;

out_num = 0;
run_time = 0;

P_p = 2e-3;
Dx0_p = 1.65e-3;
Dy0_p = 1.65e-3;
delta_p = 0;

%set the parameters list 
fixed_D = 16.5;
%fixed_D = 16.5;
fixed_P = 200;

scan_delta = linspace(0.8,4,12);
scan_B = linspace(5,50,15);

scan_flux = zeros(length(scan_B),length(scan_delta));
scan_mean_velocity = zeros(length(scan_B),length(scan_delta));
scan_angle_div = zeros(length(scan_B),length(scan_delta));
scan_mean_velocity_div = zeros(length(scan_B),length(scan_delta));
scan_push_time = zeros(length(scan_B),length(scan_delta));

% = zeros(1,length(scan_D_list));

%the loop of changing parameters to be optimzed
for delta_index = 1:length(scan_delta)
for B_index = 1:length(scan_B)
   
delta = scan_delta(delta_index)*(-Gamma);
beta = scan_B(B_index)*0.01;
    
P = fixed_P*1e-3;
D = fixed_D*1e-3;
Dx0 = D;
Dy0 = D;
    
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

if_out = zeros(1,N_atom);
if_ever_in_chamber = zeros(1,N_atom);
in_time = zeros(1,N_atom);
out_time = zeros(1,N_atom);

current_time = 0;



for j = 1:round(step/time_step)
for i = 1:time_step
    
        %record the in_time and out_time
        in_time((sqrt(x.^2 + y.^2 + z.^2) <= r_chamber)&(~if_ever_in_chamber)) = current_time;
        out_time((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= 0.2) & (~if_out)) = current_time;
        
        %judge if the atoms enter the output tube
        if_out((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= 0.2) ) = 1;
        
        %judge it the atoms have entered the chamber
        if_ever_in_chamber(sqrt(x.^2 + y.^2 + z.^2) <= r_chamber) = 1;
        %discard the atoms sticking to the chamber wall
        xi = x*cos(-phi0) - y*sin(-phi0);
        stick_index = find((sqrt(x.^2 + y.^2 + z.^2) >= r_chamber) & (if_ever_in_chamber)  & (~if_out));
        x(stick_index) = [];
        y(stick_index) = [];
        z(stick_index) = [];
        vx(stick_index) = [];
        vy(stick_index) = [];
        vz(stick_index) = [];
        if_out(stick_index) = [];
        if_ever_in_chamber(stick_index) = [];
        in_time(stick_index) = [];
        out_time(stick_index) = [];
   
       
        
        costhetax = -x./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetay = -y./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetaz = 2*z./sqrt(x.^2 + y.^2 + 4*z.^2);
 
        Br = beta*sqrt(x.^2+y.^2 + 4*z.^2);

        Sx = Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0)/I0;
        Sy = Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0)/I0;
        Sz = Gaussion_intensity(x,y,z,0,0,P_p, Dx0_p,Dy0_p)/I0;
        S_t = 2*(Sx + Sy) + Sz;

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+S_t+4*(delta_p - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
           
        R_t = Rxp + Rxm + Ryp + Rym + Rzp;
        
        
        %theta_recoil = rand(1,length(x))*pi;
        %phi_recoil = rand(1,length(x))*2*pi;
        
        %direcx = randi([-1,1],1,length(x));
        %direcy = randi([-1,1],1,length(x));
        %direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp); %+ hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
        
        Fx(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fy(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fz(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
end

if((run_time >= 0.02)&& (length(if_out(if_out == 1)) == out_num))
    break;
else
    out_num = length(if_out(if_out == 1));
    run_time = j*time_step*dt;
end

end


R_loading = out_num/N_atom*Fvc*Fthetac*Phi_total;
tau_push = mean(out_time - in_time);
scan_flux(B_index, delta_index) = R_loading;
v = sqrt(vx.^2 + vy.^2 + vz.^2);
scan_velocity(B_index, delta_index) = mean(v);
vxi = vx*cos(-phi0) - vy*sin(-phi0);
scan_angle_div(B_index, delta_index) = std(atan(vxi./vz));
scan_velocity_div = std(v);

%cap_rate_list(end + 1) = cal_rate;

end
end
[scan_delta, scan_B] = meshgrid(scan_delta, scan_B);

figure;
plot3(scan_delta, scan_B, scan_flux,'o');
xlabel('D (mm)','fontsize',18);
ylabel('flux','fontsize',18 );
title('flux @ P = 200 mW, \delta = - 1.5 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',14);

figure;
plot3(scan_delta, scan_B,'o');
xlabel('D (mm)','fontsize',18);
ylabel('\sigma_{\theta}','fontsize',18 );
title('\sigma_{\theta} @ P = 200 mW, \delta = - 1.5 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);

figure;
plot3(scan_delta, scan_B,'o');
xlabel('D (mm)','fontsize',18);
ylabel('v (m/s)','fontsize',18 );
title('v (m/s) @ P = 200 mW, \delta = - 1.5 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);


%%
%2-d scan
%find optimal beam size along xy and yz
%set the parameters delta and B'

delta = - 3*Gamma;
beta = 18*0.01;
velo = 5;

t_total = 0.1;

dt = 1/800*r_chamber/velo;
step = round(t_total/dt);

time_step = 1000;

out_num = 0;
run_time = 0;

P_p = 2e-3;
Dx0_p = 1.65e-3;
Dy0_p = 1.65e-3;
delta_p = 0;

%set the parameters list 
scan_Dxy = linspace(15,30,10);
scan_Dz = linspace(10,20,10);
%fixed_D = 16.5;
fixed_P = 200;

scan_flux = zeros(length(scan_Dz),length(scan_Dxy));
scan_mean_velocity = zeros(length(scan_Dz),length(scan_Dxy));
scan_angle_div = zeros(length(scan_Dz),length(scan_Dxy));
scan_push_time = zeros(length(scan_Dz),length(scan_Dxy));

% = zeros(1,length(scan_D_list));

%the loop of changing parameters to be optimzed

for Dxy_index = 1:length(scan_Dxy)
for Dz_index = 1:length(scan_Dz)
   
P = fixed_P*1e-3;
Dx0 = scan_Dxy(Dxy_index)*1e-3;
Dy0 = scan_Dz(Dz_index)*1e-3;
    
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

if_out = zeros(1,N_atom);
if_ever_in_chamber = zeros(1,N_atom);
in_time = zeros(1,N_atom);
out_time = zeros(1,N_atom);

current_time = 0;

for j = 1:round(step/time_step)
for i = 1:time_step
    
        %record the in_time and out_time
        in_time((sqrt(x.^2 + y.^2 + z.^2) <= r_chamber)&(~if_ever_in_chamber)) = current_time;
        out_time((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10) & (~if_out)) = current_time;
    
        %judge if the atoms enter the output tube
        if_out((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10)) = 1;
        
        %judge it the atoms have entered the chamber
        if_ever_in_chamber(sqrt(x.^2 + y.^2 + z.^2) <= r_chamber) = 1;
        %discard the atoms sticking to the chamber wall
        xi = x*cos(-phi0) - y*sin(-phi0);
        stick_index = find((sqrt(x.^2 + y.^2 + z.^2) >= r_chamber) & (if_ever_in_chamber)  & (~if_out));
        x(stick_index) = [];
        y(stick_index) = [];
        z(stick_index) = [];
        vx(stick_index) = [];
        vy(stick_index) = [];
        vz(stick_index) = [];
        if_out(stick_index) = [];
        if_ever_in_chamber(stick_index) = [];
        in_time(stick_index) = [];
        out_time(stick_index) = [];
       
        
        costhetax = -x./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetay = -y./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetaz = 2*z./sqrt(x.^2 + y.^2 + 4*z.^2);
 
        Br = beta*sqrt(x.^2+y.^2 + 4*z.^2);

        Sx = Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0)/I0;
        Sy = Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0)/I0;
        Sz = Gaussion_intensity(x,y,z,0,0,P_p, Dx0_p,Dy0_p)/I0;
        S_t = 2*(Sx + Sy) + Sz;

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+S_t+4*(delta_p - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
           
        R_t = Rxp + Rxm + Ryp + Rym + Rzp;
        
        
        %theta_recoil = rand(1,length(x))*pi;
        %phi_recoil = rand(1,length(x))*2*pi;
        
        %direcx = randi([-1,1],1,length(x));
        %direcy = randi([-1,1],1,length(x));
        %direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp); %+ hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
        
        Fx(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fy(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fz(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
end

if((run_time >= 0.03)&& (length(if_out(if_out == 1)) == out_num))
    break;
else
    out_num = length(if_out(if_out == 1));
    run_time = j*time_step*dt;
end

end

tau_push = mean(out_time - in_time);


flux = out_num/N_atom*Fvc*Fthetac*Phi_total;

scan_flux(Dz_index, Dxy_index) = flux;
scan_push_time(Dz_index, Dxy_index) = tau_push;

v = sqrt(vx.^2 + vy.^2 + vz.^2);
scan_mean_velocity(Dz_index, Dxy_index) = mean(v);
vxi = vx*cos(-phi0) - vy*sin(-phi0);
scan_angle_div(Dz_index, Dxy_index) = std(atan(vxi./vz));
scan_velocity_div = std(v);

%cap_rate_list(end + 1) = cal_rate;

end
end

[scan_Dxy,scan_Dz] = meshgrid(scan_Dxy,scan_Dz);

figure;
plot3(scan_Dxy,scan_Dz, scan_flux,'o');
xlabel('D (mm)','fontsize',18);
ylabel('R_{loading} (s^{-1})','fontsize',18 );
title('R_{loading} @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',14);

figure;
plot3(scan_Dxy,scan_Dz, scan_angle_div,'o');
xlabel('D (mm)','fontsize',18);
ylabel('\sigma_{\theta}','fontsize',18 );
title('\sigma_{\theta} @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);

figure;
plot3(scan_Dxy,scan_Dz, scan_mean_velocity,'o');
xlabel('D (mm)','fontsize',18);
ylabel('mean(v) (m/s)','fontsize',18 );
title('mean(v) (m/s) @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);


%%
%2-d scan
%find optimal power and beam size of push beam
%set the parameters delta and B'

tau_collision = 0.032;

delta = - 3*Gamma;
beta = 18*0.01;
velo = 5;

t_total = 0.1;

dt = 1/800*r_chamber/velo;
step = round(t_total/dt);

time_step = 1000;

out_num = 0;
run_time = 0;

delta_p = 0;

%set the parameters list 
scan_P_p = linspace(0.1, 10, 10);
scan_D_p = linspace(0.1, 4, 12);

%fixed_D = 16.5;
fixed_P = 200;
fixed_Dxy = 22;
fixed_Dz = 14.5;

scan_flux = zeros(length(scan_D_p),length(scan_P_p));
scan_mean_velocity = zeros(length(scan_D_p),length(scan_P_p));
scan_angle_div = zeros(length(scan_D_p),length(scan_P_p));
scan_push_time = zeros(length(scan_D_p),length(scan_P_p));
scan_mean_angle = zeros(length(scan_D_p),length(scan_P_p));

% = zeros(1,length(scan_D_list));

%the loop of changing parameters to be optimzed

for D_p_index = 1:length(scan_D_p)
for P_p_index = 1:length(scan_P_p)
   
P = fixed_P*1e-3;
Dx0 = fixed_Dxy*1e-3;
Dy0 = fixed_Dz*1e-3;

Dx0_p = scan_D_p(D_p_index)*1e-3;
Dy0_p = scan_D_p(D_p_index)*1e-3;
P_p = scan_P_p(P_p_index)*1e-3;
    
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

if_out = zeros(1,N_atom);
if_ever_in_chamber = zeros(1,N_atom);
in_time = zeros(1,N_atom);
out_time = zeros(1,N_atom);

current_time = 0;

for j = 1:round(step/time_step)
for i = 1:time_step
    
        %record the in_time and out_time
        in_time((sqrt(x.^2 + y.^2 + z.^2) <= r_chamber)&(~if_ever_in_chamber)) = current_time;
        out_time((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10) & (~if_out)) = current_time;
    
        %judge if the atoms enter the output tube
        if_out((z >= r_chamber) & (sqrt(x.^2 + y.^2) <= r_tube) & (sqrt(vx.^2 + vy.^2) <= 10)) = 1;
        
        %judge it the atoms have entered the chamber
        if_ever_in_chamber(sqrt(x.^2 + y.^2 + z.^2) <= r_chamber) = 1;
        %discard the atoms sticking to the chamber wall
        xi = x*cos(-phi0) - y*sin(-phi0);
        stick_index = find((sqrt(x.^2 + y.^2 + z.^2) >= r_chamber) & (if_ever_in_chamber)  & (~if_out));
        x(stick_index) = [];
        y(stick_index) = [];
        z(stick_index) = [];
        vx(stick_index) = [];
        vy(stick_index) = [];
        vz(stick_index) = [];
        if_out(stick_index) = [];
        if_ever_in_chamber(stick_index) = [];
        in_time(stick_index) = [];
        out_time(stick_index) = [];
       
        
        costhetax = -x./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetay = -y./sqrt(x.^2 + y.^2 + 4*z.^2);
        costhetaz = 2*z./sqrt(x.^2 + y.^2 + 4*z.^2);
 
        Br = beta*sqrt(x.^2+y.^2 + 4*z.^2);

        Sx = Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0)/I0;
        Sy = Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0)/I0;
        Sz = Gaussion_intensity(x,y,z,0,0,P_p, Dx0_p,Dy0_p)/I0;
        S_t = 2*(Sx + Sy) + Sz;

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+S_t+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+S_t+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+S_t+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+S_t+4*(delta_p - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+S_t+4*(delta_p - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz;
           
        R_t = Rxp + Rxm + Ryp + Rym + Rzp;
        
        
        theta_recoil = rand(1,length(x))*pi;
        phi_recoil = rand(1,length(x))*2*pi;
        
        direcx = randi([-1,1],1,length(x));
        direcy = randi([-1,1],1,length(x));
        direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm) + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym) + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp) + hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
        
        Fx(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fy(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        Fz(sqrt(x.^2 + y.^2 + z.^2) > r_chamber)=0;
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
end

if((run_time >= 0.03)&& (length(if_out(if_out == 1)) == out_num))
    break;
else
    out_num = length(if_out(if_out == 1));
    run_time = j*time_step*dt;
end

end

tau_push = mean(out_time - in_time);


flux = out_num/N_atom*Fvc*Fthetac*Phi_total*(tau_collision)/(tau_collision + tau_push);

scan_flux(D_p_index, P_p_index) = flux;
scan_push_time(D_p_index, P_p_index) = tau_push;

v = sqrt(vx.^2 + vy.^2 + vz.^2);
scan_mean_velocity(D_p_index, P_p_index) = mean(v);
vxi = vx*cos(-phi0) - vy*sin(-phi0);
scan_angle_div(D_p_index, P_p_index) = std(atan(vxi./vz));
scan_velocity_div = std(v);
scan_mean_angle(D_p_index, P_p_index) = mean(atan(vxi./vz));

%cap_rate_list(end + 1) = cal_rate;

end
end

[scan_P_p,scan_D_p] = meshgrid(scan_P_p,scan_D_p);

figure;
plot3(scan_P_p,scan_D_p, scan_flux,'o');
xlabel('D (mm)','fontsize',18);
ylabel('R_{loading} (s^{-1})','fontsize',18 );
title('R_{loading} @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push} = 1.65 mm','fontsize',14);

figure;
plot3(can_P_p,scan_D_p, scan_angle_div,'o');
xlabel('D (mm)','fontsize',18);
ylabel('\sigma_{\theta}','fontsize',18 );
title('\sigma_{\theta} @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);

figure;
plot3(can_P_p,scan_D_p, scan_mean_velocity,'o');
xlabel('D (mm)','fontsize',18);
ylabel('mean(v) (m/s)','fontsize',18 );
title('mean(v) (m/s) @ P = 200 mW, \delta = - 3 \Gamma, B = 20 G/cm, P_{push} = 2 mW, D_{push}','fontsize',14);
