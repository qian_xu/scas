classdef AtomState < uint8
   %ATOMSTATE Possible states an atom can be in
    %   Enumerates the states that an atom can be in

    enumeration
        HittingChamber (0),
        InsideChamber (1),
        ComingOutFromTube (2),
        HasComeOutOfTube (3), 
        NeverInChamber (4);
    end
end