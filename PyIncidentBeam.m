classdef PyIncidentBeam < GaussianBeam
    %PYINCIDENTBEAM Represents the incident beam, which is a children class
    %of GaussianBeam
    %   An PyIncidentBeam has properties:  PyLength, PyDiameter,
    %   PyReflectivity, HoleDiameter
    %   An PyIncidentBeam has methods: CalculateIntensity(x,y,z)
    
    properties
        %PYLENGTH Length of the pyramid
        PyLength;
        %PYDIAMETER Diameter of the pyramid
        PyDiameter;
        %PYREFLECTIVITY Reflectivity of the pyramid wall
        PyReflectivity;
        %HOLEDIAMETER Diameter of the hole
        HoleDiameter;
        %PYANGLE Angle of the pyramid wall (deviation from pi/4)
        PyAngle;
    end
    
     methods (Static)
         function obj = Create(power, wavelength, detuning, beamwaist1, beamwaist2, theta, phi, dshift)
             %CREATE Craate an BeamWaist object with relevant parameters
             obj = PyIncidentBeam();
             obj.Power = power;
             obj.BeamWaist1 = beamwaist1;
             obj.BeamWaist2 = beamwaist2;
             obj.Theta = theta;
             obj.Phi = phi;
             obj.Dshift = dshift;
             obj.WaveLength = wavelength;
             obj.Detuning = detuning;
         end
    end
    
    methods
        function I = CalculateIntensity(instance, x,y,z)
            %CALCULATEINTENSITY Calculate the spacial intensity of the
            %incident beam 
            zR = 20;
            x0 = cos(instance.Theta)*cos(instance.Phi)*x - cos(instance.Theta)*sin(instance.Phi)*y + sin(instance.Theta)*z - instance.Dshift*cos(2*instance.PyAngle);
            y0 = sin(instance.Phi)*x + cos(instance.Phi)*y;
            z0 = -sin(instance.Theta)*cos(instance.Phi)*x + sin(instance.Theta)*sin(instance.Phi)*y + cos(instance.Theta)*z;

            Dx = instance.BeamWaist1*sqrt(1+(z0.^2)/zR^2);
            Dy = instance.BeamWaist2*sqrt(1+(z0.^2)/zR^2);

            I = instance.Power./(pi/2*Dx.*Dy).*exp(-2*x0.^2./Dx.^2 -2*y0.^2./Dy.^2);
            
            %Set geometrical restrain on the incident beam
            IfInsidePy = ((z >= - instance.PyLength) & (z <= 0) & (x > z*tan(pi/4 - instance.PyAngle) - instance.HoleDiameter/2) & (x < -z*tan(pi/4 - instance.PyAngle) + instance.HoleDiameter/2) ...
                & (y > z*tan(pi/4 - instance.PyAngle) - instance.HoleDiameter/2) & (y < -z*tan(pi/4 - instance.PyAngle) + instance.HoleDiameter/2));
            
            I((z >= - instance.PyLength) & (abs(x) <= instance.PyDiameter/2) & (abs(y) <= instance.PyDiameter/2) & (~IfInsidePy)) = 0;
            %I(~IfInsidePy) = 0;

        end
    end
    
end

