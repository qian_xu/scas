classdef Event < handle
    %EVENT A simple but fast event framework
    
    properties (SetAccess=private,GetAccess=private)
        Listeners
    end
    
    methods
        
        function instance = Event()
            instance.Listeners = EventListener.empty();
        end
        
        % Syntax:
        % SomeEvent = Event();
        % listener = Listener(callback);
        %   where callback(sender, args) is invoked on event
        %    sender is an object reference.
        %    args describes the event circumstance
        % 
        % SomeEvent.add(listener);
        %
        % SomeEvent.remove(listener);
        %   unsubscribe event from listener.
        % 
        % SomeEvent.invoke(sender,args);
        
        function checkValid(~,b)
            if ~isa(b, 'EventListener')
                error('b must be an event listener.');
            end
        end
        
        function add(a,b)
            checkValid(a,b);
            a.Listeners(end+1) = b;            
        end
        
        function remove(a,b)          
            if length(b) > 1
                error('b must be length=1');
            end
            i = find(a.Listeners == b);
            if isempty(i)
                error('Could not find listener to unsubscribe.');
            end
            a.Listeners(i) = [];
        end
        
        function invoke(event, sender, args)
            %INVOKE Invoke the event with given sender and arguments
            for i=1:length(event.Listeners)
                event.Listeners(i).Callback(sender, args);
            end            
        end
    end
    
    methods (Static)
        
        function testEvent
            
            testCallback = @(sender, args) disp([ sender ':' args ]);
            e = Event();
            l1 = EventListener(testCallback);
            l2 = EventListener(testCallback);
            e.add(l1);
            e.add(l2);
            e.invoke('Elliot', 'testing');
            disp('Remove event.');
            e.remove(l2);
            e.invoke('Second', 'test');
            
        end
        
    end
end

