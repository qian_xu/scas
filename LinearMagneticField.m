classdef LinearMagneticField < MagneticField
    %LINEARMAGNETICFIELD Children class of MagneticField, has liner field
    %profile
    %   A LinearMagneticField has properties: Gradient
    %   A LinearMagneticField has methods: CalculateMagField(x,y,z)
    
    properties
        %GRADIENT Linear gradient of the field
        Gradient;
        
        %FIELDCENTER Field center (zero magnetic field position)
        FieldCenter;
        
    end
    
    methods
        function B = CalculateMagField(instance, x, y, z)
            %CALCULATEMAGFIELD Return the vector form of the magnetic field
            %given spacial coordinate
            B = instance.Gradient*[-(x - instance.FieldCenter(1)); -(y - instance.FieldCenter(2)); 2*(z - instance.FieldCenter(3))];
        end
    end
    
end

