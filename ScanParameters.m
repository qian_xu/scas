clear;
%set the parameters of Rubidium atom
m = 1.44e-25;
Gamma = 2*pi*6.06e6;
I0 = 16.7;

%set the parameters of laser beams
P = 380e-3;
lambda = 780e-9;
delta = - 4*Gamma;

Dx0 = 35e-3;
Dy0 = 35e-3;

global kB
kB =  1.38e-23;

%set the parameters of pyramid
PyLength = 28e-3;
PyDiameter = 56e-3;
PyReflectivity = 1;
ChamberLength = 120e-3;
global ChamberDiameter
ChamberDiameter = 100e-3;
HoleDiameter = 2e-3;
PyWallThickness = 0.8e-3;

%Create six beams
BeamXp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2, pi/2, HoleDiameter/2 );
set(BeamXp, {'Direction', 'Polarization'}, {[1; 0; 0],1});
set(BeamXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity});
BeamXm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2, 3*pi/2, HoleDiameter/2 );
set(BeamXm, {'Direction', 'Polarization'}, {[- 1; 0; 0],1});
set(BeamXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity});
BeamYp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2, 0, HoleDiameter/2 );
set(BeamYp, {'Direction', 'Polarization'}, {[0; 1; 0],1});
set(BeamYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity});
BeamYm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2, pi, HoleDiameter/2 );
set(BeamYm, {'Direction', 'Polarization'}, {[0; -1; 0],1});
set(BeamYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity});
BeamZp = PyIncidentBeam.Create(P, lambda, delta, Dx0, Dy0, 0, 0, 0 );
set(BeamZp, {'Direction', 'Polarization'}, {[0; 0; 1],- 1});
set(BeamZp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity});
BeamZm = PyRetroBeam.Create(P, lambda, delta, Dx0, Dy0, 0, pi, 0 );
set(BeamZm, {'Direction', 'Polarization'}, {[0; 0; - 1], - 1});
set(BeamZm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity});
Beams = {BeamXp,BeamXm,BeamYp,BeamYm,BeamZp,BeamZm};


%Set the linear magnetic field
Xc = 0;
Yc = 0;
Zc = - PyLength/2;
Beta = 8*0.01;   %Linear gradient
MagField = LinearMagneticField();
set(MagField, 'Gradient', Beta);
set(MagField, 'FieldCenter', [Xc; Yc; Zc]);

%Set the gravity 
G = Gravity();

%Set the scattering force
global dt
dt = ChamberDiameter/10/3000;
ScatForce = ScatteringForce();
set(ScatForce, 'Beams', Beams);
set(ScatForce, 'MagField', MagField);
set(ScatForce, 'TimeStep', dt);
set(ScatForce, {'IfAddRecoil', 'IfAddScatteringFluctuation'}, {true,true});

%Set total forces: Gravity and scattering force
forces = {ScatForce};

global Vtruncation;
Vtruncation = 150;

%Create the monitor for pyramid mot
PyramidMonitor = Monitor();
set(PyramidMonitor, {'ChamberLength', 'ChamberDiameter', 'PyLength', 'PyDiameter', 'PyReflectivity', 'HoleDiameter', 'PyWallThickness'},{ChamberLength, ChamberDiameter, PyLength, PyDiameter, PyReflectivity, HoleDiameter, PyWallThickness});
plot_z = linspace(-PyLength,0,1000);
plot_x1 = -plot_z + HoleDiameter/2;
plot_y1 = -plot_z + HoleDiameter/2;
plot_x2 = plot_z - HoleDiameter/2;
plot_y2 = plot_z - HoleDiameter/2;
PyramidPlotPoints = [[plot_x1;plot_y1;plot_z], [plot_x1;plot_y2;plot_z], [plot_x2;plot_y1;plot_z],[plot_x2;plot_y2;plot_z]];
set(PyramidMonitor, 'PyramidPlotPoints',PyramidPlotPoints);
set(PyramidMonitor, 'Vtruncation', Vtruncation);

%Set the chamber pressure and temperature
global ChamberPressure 
ChamberPressure = 1e-6;
global T 
T = 20 + 273;
%%

%Create a group of thermal atoms
global AtomNum
AtomNum = 3000;
ThermalAtoms = GenerateThermalAtoms(AtomNum, -ChamberDiameter/2, ChamberDiameter/2, -ChamberDiameter/2, ChamberDiameter/2, -ChamberLength, 0, Vtruncation);

%%
%Create Pyramid_sim object

PyramidSim = Simulation();
PyramidSim.SetPyramid(ChamberLength, ChamberDiameter, PyLength, PyDiameter, PyReflectivity, HoleDiameter, PyWallThickness);
PyramidSim.SetLaserBeams(Beams);
PyramidSim.SetMagneticField(MagField);
PyramidSim.SpecifyForces(forces);
PyramidSim.CreateMonitor(PyramidMonitor);
set(PyramidSim, 'BoundaryCondition', 2);
set(PyramidSim, 'IfKeepOutAtoms', false);


%%
InitialAtoms = arrayfun(@(atom) atom.CopyAtom(), ThermalAtoms, 'UniformOutput', 0);
InitialAtoms = cat(1,InitialAtoms{:});
CalculateFlux(PyramidSim, InitialAtoms, P, delta, Dx0, Dy0, Beta, PyLength, ChamberLength, ChamberDiameter)

%%
ScanDetuning = linspace(1,4,7)*(-Gamma);
ScanFlux = zeros(1,length(ScanDetuning));
for DetuningIndex = 1:length(ScanDetuning)
    ScanFlux(DetuningIndex) = CalculateFlux(PyramidSim, InitialAtoms, P, ScanDetuning(DetuningIndex), Dx0, Dy0, Beta, PyLength, ChamberLength, ChamberDiameter);
end
figure;
plot(ScanDetuning,ScanFlux);