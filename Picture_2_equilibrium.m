%define the parameters
%geometric parameters of the pyramid
L_py = 28e-3;
D_py = 56e-3;
L_chamber = 120e-3;
D_chamber = 100e-3;
d_hole = 2e-3;
d_t = 0.8e-3;


%parameters of rubidium
m = 1.44e-25;
lambda = 780e-9;
k = 2*pi/lambda;
Gamma = 2*pi*6.06e6;

muB = 9.27400e-24;
g_p = - 2/3;
g_m = 2/3;
g_z = 0;

mup = g_p*muB;
mum = g_m*muB;
muz = g_z*muB;

hbar = 6.626e-34/(2*pi);

kB =  1.38e-23;

N_atom = 2000;    %the number of atoms used in the simulation

%set parameters about the Gaussian beam
I0 = 16.7;
%P = 200e-3;
%Dx0 = 16.5e-3;
%Dy0 = 16.5e-3;

%define the reflectivity of the wall
Reflectivity = 1;



%initialize the atoms according to the VDF

v_truncation = 150;     %set the truncation velocity
vmax = v_truncation;

%visualize the VDF & find the normalization value
v = linspace(0, vmax, 600);
fv = VDF(v);
fmax = max(fv);
%plot(v,fv,'.');
f1 =@(v) VDF(v);
Fvc = integral(f1,0,vmax);

%initialize the position with uniform distribution inside the chamber
z0 = (rand(1,N_atom)-1)* (L_chamber);
x0 = (rand(1,N_atom)-0.5)*D_chamber/sqrt(2);
y0 = (rand(1,N_atom)-0.5)*D_chamber/sqrt(2);


vx0 = zeros(1,N_atom);
vy0 = zeros(1,N_atom);
vz0 = zeros(1,N_atom);

%initialize the velocity of atoms 
for atom_index = 1:N_atom
    v_random = rand()*vmax;
    p = VDF(v_random)/fmax;
    s = rand();
    while(s > p)
        v_random = rand()*vmax;
        p = VDF(v_random)/fmax;
        s = rand();
    end
    theta_random = rand()*pi;
    p = j_theta(theta_random)/1;
    s = rand();
    while(s > p)
        theta_random = rand()*pi;
        p = j_theta(theta_random)/1;
        s = rand();
    end
    phi_random = rand()*2*pi;
    
    vz0(1,atom_index) = v_random*cos(theta_random);
    vx0(1,atom_index) = v_random*sin(theta_random)*cos(phi_random);
    vy0(1,atom_index) = v_random*sin(theta_random)*sin(phi_random);
    
end


figure;
plot3(x0,y0,z0,'o')
xlim([-D_chamber/(2), D_chamber/2]);
ylim([-D_chamber/(2), D_chamber/2]);
zlim([-L_chamber, 0]);
hold on;


%plot the pyramid
plot_z = linspace(-L_py,0,1000);
plot_x1 = -plot_z + d_hole/2;
plot_y1 = -plot_z + d_hole/2;

plot_x2 = plot_z - d_hole/2;
plot_y2 = plot_z - d_hole/2;

plot3(plot_x1,plot_y1,plot_z, 'k');
plot3(plot_x2,plot_y1,plot_z, 'k');
plot3(plot_x1,plot_y2,plot_z, 'k');
plot3(plot_x2,plot_y2,plot_z, 'k');
hold on;



%calculate the collision rate
sigma_collision = 3.9e-17;
T = 20 + 273;
P_chamber = 1e-6;
n_collision = P_chamber/(kB*T);
v_rms = sqrt(2*kB*T/m);
tau_collision = 1/(n_collision*v_rms*sigma_collision);
Gamma_collision = 25;


P_chamber = 1e-6;
T = 20 + 273;
n_real = P_chamber/(kB*T);
n_sim = N_atom/(pi*D_chamber^2/4*(L_chamber - L_py));
%%
%single run
velo = 10;
dt = D_chamber/abs(velo)/3000;
t_total = 0.02;
step = round(t_total/dt);


%set the laser detuning and magnetic field gradient
delta_variable = - 4;    %(per Gamma)
beta_variable = 8;    %(per G/cm)
delta = delta_variable*Gamma;
beta = beta_variable*0.01;

%set laser power and beam waist
P = 380e-3;
Dx0 = 35e-3;
Dy0 = 35e-3;

%initilize the atoms
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

in_time = zeros(1,N_atom);

time_step = 200;    



%magnetic center
x_c = 0;
y_c = 0;
z_c = -L_py/2;

fig = figure;

%set 
flux_record = zeros(1,round(step/time_step));
hit_ratio_record = zeros(1,round(step/time_step));
loss_ratio_record = zeros(1,round(step/time_step));
time_record = linspace(0,t_total,round(step/time_step));
inside_num_record = zeros(1,round(step/time_step));
loss_record = zeros(1,round(step/time_step));
velocity_record = zeros(1,round(step/time_step));
angle_record = zeros(1,round(step/time_step));


if_ever_in_py = zeros(1,N_atom);
in_time = zeros(1,N_atom);

if_hit_wall = zeros(1,N_atom);


%record part of the output atom
N_record = 10000;
Record_out_v = zeros(1,N_record);
Record_initial_v = zeros(1,N_record);
Record_angle_x = zeros(1,N_record);

%record the atoms coming from the aperture
aperture_record_num = 2000;
aperture_atom_x = [];
aperture_atom_y = [];
aperture_atom_vx =[];
aperture_atom_vy = [];
aperture_atom_vz = [];


initial_v = sqrt(vx0.^2 + vy0.^2 + vz0.^2);

current_time = 0;
record_index = 0;
aperture_index = 0;

for j = 1:round(step/time_step)
out_num_from_wall = 0;
out_num = 0;
loss = 0;
output_velocity = 0;
output_angle = 0;
loss_num_through_surface = 0;
for i = 1:time_step
    
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        py_outer_wall_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2 - d_t) & (x < -z + d_hole/2 + d_t) & (y > z - d_hole/2 - d_t) & (y < -z + d_hole/2 + d_t));
        output_index = (z >= 0) & ((abs(x) <= d_hole/2)& (abs(y) <= d_hole/2)) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= 0.2);    %the atoms going out the pyramid through the hole
 
        stick_py_index = py_outer_wall_index & (~py_inside_index) & (if_ever_in_py);   %atoms hitting the pyramid walls
        stick_chamber_index = ((sqrt(x.^2 + y.^2) >= D_chamber/2) | (z <= -L_chamber) | ((z >= 0)));   %atoms sticking to the chamber walls
        if_hit_wall(py_outer_wall_index & (~py_inside_index)) = 1;
        
        if_hit_aperture = (z <= -L_py) & (z >= -L_py - 0.3e-3) & (abs(x) <= D_py/2 + d_hole/2) & (abs(y) <= D_py/2 + d_hole/2) &(~if_ever_in_py);
        
        in_time(py_inside_index & (~if_ever_in_py)) = current_time;     %the time when atoms move into the pyramid
        
        trap_time = current_time - in_time(output_index);      %the time atoms spend inside the pyramid
        
        out_num = out_num + length(output_index(output_index == 1));    
        judge_hit_wall = if_hit_wall(output_index);
        out_num_from_wall = out_num_from_wall + length(judge_hit_wall(judge_hit_wall == 1));
        loss_through_wall_index = (z >= - L_py)&(~py_outer_wall_index);
        loss_num_through_surface = loss_num_through_surface + length(loss_through_wall_index(loss_through_wall_index == 1));
        
        %update pre-set statistics
        loss = loss + sum(exp(-Gamma_collision*trap_time));
        v = sqrt(vx.^2 + vy.^2 + vz.^2);
        angle = atan(sqrt(vx.^2 + vy.^2)./vz);
        angle_x = atan(vx./vz);
        output_velocity = output_velocity+ sum(v(output_index));
        output_angle = output_angle + sum(angle(output_index));
        
        v_out = v(output_index);
        v_initial = initial_v(output_index);
        angle_x_out = angle_x(output_index);
        
        if(record_index <= N_record*0.8) && (length(v_out) >= 1);
        for out_index  = 1:length(length(v_out))
            Record_out_v(record_index + out_index) = v_out(out_index);
            Record_initial_v(record_index + out_index) = v_initial(out_index);
            Record_angle_x(record_index + out_index) = angle_x_out(out_index);
        end
        record_index = record_index + length(v_out);
        end
        
        if(current_time >= 0.01)
        aperture_atom_x = [aperture_atom_x, x(if_hit_aperture)];
        aperture_atom_y = [aperture_atom_y, y(if_hit_aperture)];
        aperture_atom_vx = [aperture_atom_vx, vx(if_hit_aperture)];
        aperture_atom_vy = [aperture_atom_vy, vy(if_hit_aperture)];
        aperture_atom_vz = [aperture_atom_vz, vz(if_hit_aperture)];
        end
        
            
        if_ever_in_py(py_inside_index) = 1;
        
        %renew the atoms sticking to the chamber wall
        stick_chamber_index = stick_chamber_index | stick_py_index;
        stick_chamber_real_index = find(stick_chamber_index == 1);
        N_stick_chamber = length(stick_chamber_real_index);
        
        if_ever_in_py(stick_chamber_index) = 0;
        in_time(stick_chamber_index) = 0;

    z_add_chamber = (rand(1,N_stick_chamber)-1)* L_chamber;      
    x_add_chamber = (rand(1,N_stick_chamber)-0.5)*D_chamber/sqrt(2);
    y_add_chamber = (rand(1,N_stick_chamber)-0.5)*D_chamber/sqrt(2);
    vx_add_chamber = zeros(1,N_stick_chamber);
    vy_add_chamber = zeros(1,N_stick_chamber);
    vz_add_chamber = zeros(1,N_stick_chamber);

for atom_index = 1:N_stick_chamber
    v_random = rand()*vmax;
    p = VDF(v_random)/fmax;
    s = rand();
    while(s > p)
        v_random = rand()*vmax;
        p = VDF(v_random)/fmax;
        s = rand();
    end
    theta_random = rand()*pi;
    p = j_theta(theta_random)/1;
    s = rand();
    while(s > p)
        theta_random = rand()*pi;
        p = j_theta(theta_random)/1;
        s = rand();
    end
    phi_random = rand()*2*pi;
    
    vz_add_chamber(1,atom_index) = v_random*cos(theta_random);
    vx_add_chamber(1,atom_index) = v_random*sin(theta_random)*cos(phi_random);
    vy_add_chamber(1,atom_index) = v_random*sin(theta_random)*sin(phi_random);
    
end
        x(stick_chamber_index) = x_add_chamber;
        y(stick_chamber_index) = y_add_chamber;
        z(stick_chamber_index) = z_add_chamber;
        %vx(stick_chamber_index) = vx_add_chamber;
        %vy(stick_chamber_index) = vy_add_chamber;
        %vz(stick_chamber_index) = vz_add_chamber;
        
        initial_v(stick_chamber_index) = sqrt(vx_add_chamber.^2 + vy_add_chamber.^2 + vz_add_chamber.^2);
        if_hit_wall(stick_chamber_index) = 0;    
        
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        
        %calculate the force
        costhetax = -(x - x_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetay = -(y - y_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetaz = 2*(z - z_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
 
        Br = beta*sqrt((x - x_c).^2+ (y - y_c).^2 + 4*(z - z_c).^2);

        Sz1 = Gaussion_intensity(x,y,z,0,0,P,Dx0,Dy0,0)/I0;
        Sz2 = Reflectivity^2*Gaussion_intensity(x,y,z,pi,0,P,Dx0,Dy0,0)/I0;

        Sx1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0, d_hole/2)/I0;
        Sx2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,3*pi/2,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi,P,Dx0,Dy0, d_hole/2)/I0;
        
        Sz2((abs(x) <= d_hole/2) & (abs(y) <= d_hole/2)) = 0;
        Sz2((abs(x) >= D_py/2) | (abs(y) >= D_py/2)) = 0;
        Sz1((z >= - L_py) & (~py_inside_index) ) = 0;
        Sz2((z >= - L_py) & (~py_inside_index) ) = 0;
        %Sz1( (~py_inside_index) ) = 0;
        %Sz2( (~py_inside_index) ) = 0;
        Sx1(~py_inside_index) = 0;
        Sx2(~py_inside_index) = 0;
        Sy1(~py_inside_index) = 0;
        Sy2(~py_inside_index) = 0;
        
        St = 1*(Sx1 + Sx2 + Sy1 + Sy2 + Sz1 + Sz2);

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx1+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx1;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx2+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx2;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy1+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy1;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy2+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy2;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz1+4*(delta - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz1;
    
        Rzm = (1/4*(- costhetaz - 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(- costhetaz + 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz2+4*(delta + k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz2;
           
        R_t = (Rxp + Rxm + Ryp + Rym + Rzp + Rzm);
        
        
        theta_recoil = rand(1,length(x))*pi;
        phi_recoil = rand(1,length(x))*2*pi;
        
        direcx = randi([-1,1],1,length(x));
        direcy = randi([-1,1],1,length(x));
        direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm)  + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym) + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp - Rzm)  + hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
       
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
        
       
end

    %record pre-set statistics
    flux_record(j) = out_num/(time_step*dt);
    loss_record(j) = loss/out_num;
    velocity_record(j) = output_velocity/out_num;
    angle_record(j) = output_angle/out_num;
    hit_ratio_record(j) = out_num_from_wall/out_num;
    loss_ratio_record(j) = loss_num_through_surface/out_num;
    
    
    plot3(plot_x1,plot_y1,plot_z, 'k', plot_x2,plot_y1,plot_z, 'k', plot_x1,plot_y2,plot_z, 'k', plot_x2,plot_y2,plot_z, 'k');
    hold on;
    plot3(x,y,z,'o');
    xlabel('x');
    ylabel('y');
    zlabel('z');
    xlim([-2*D_py/(2)*1.2, 1.5*D_py/(2)*1.2]);
    ylim([-2*D_py/(2)*1.2, 1.5*D_py/(2)*1.2]);
    zlim([-3*L_py, 0.01]);
    view([30,15]);
    hold off;
    
    drawnow
    frame = getframe(fig);
    im{j} = frame2im(frame);
    %pause(0.0001);
   

end

figure;
plot(time_record, flux_record,'.');
xlabel('t');
ylabel('flux')

figure;
plot(time_record, loss_record,'.');
xlabel('t');
ylabel('loss')

figure;
plot(time_record, velocity_record,'.');
xlabel('t');
ylabel('v');

figure;
plot(time_record, angle_record,'.');
xlabel('t');
ylabel('angle');

figure;
plot(time_record, hit_ratio_record,'.');
xlabel('t');
ylabel('hit ratio');

figure;
plot(time_record, loss_ratio_record,'.');
xlabel('t');
ylabel('loss ratio');

Recorded_out_v = Record_out_v(Record_out_v ~= 0);
Recorded_initial_v = Record_initial_v(Record_initial_v ~= 0);
Recorded_angle_x = Record_angle_x(Record_angle_x ~= 0);

figure;
histogram(Recorded_out_v,200);
xlabel('v_{out} (m/s)','fontsize',20);
%xlim([8,40]);
title('v_{out} @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 6 mm, L_{chamber} = 120 mm' );
set(gca,'FontSize',16);

figure;
histogram(Recorded_angle_x,200);
xlabel('\theta_x','fontsize',20);
title('\theta_{x} @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 6 mm, L_{chamber} = 120 mm' );
%xlim([-0.2, 0.2])
set(gca,'FontSize',16);

figure;
histogram(Recorded_initial_v,100);
xlabel('v_{initial}','fontsize',20);
title('v_{initial} (m/s) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 9 mm' );
set(gca,'FontSize',16);

hot_v = Recorded_out_v(Recorded_out_v >= 20);
hot_theta = Recorded_angle_x(Recorded_out_v >= 20);
figure;
histogram(hot_v, 100);
title('v_{out} of hot atoms @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 9 mm, L_{chamber} = 120 mm' );
set(gca,'FontSize',16);
figure;
histogram(hot_theta, 100);
title('\theta_{x} of hot atoms @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 9 mm, L_{chamber} = 120 mm' );
set(gca,'FontSize',16);

figure;
histogram(sqrt(aperture_atom_vx.^2 + aperture_atom_vy.^2 + aperture_atom_vz.^2),100);
xlabel('v_{through}','fontsize',20);
title('v_{through} (m/s) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm, L_{chamber} = 150 mm, with slower' );
set(gca,'FontSize',16);

figure;
plot(aperture_atom_x, aperture_atom_y, 'o', 'MarkerSize',2);
xlabel('x_{through}','fontsize',20);
ylabel('y_{through}','fontsize',20);
title('Postion @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm, L_{chamber} = 150 mm, with slower' );
set(gca,'FontSize',16);

mean_flux = mean(flux_record(end - 5: end));

mean_loss = mean(loss_record(end - 5:end));

mean_velocity = mean(velocity_record(end-5:end));

mean_angle = mean(angle_record(end-5:end));

mean_hit_ratio = mean(hit_ratio_record(end-5:end));

mean_loss_ratio = mean(loss_ratio_record(end-5:end));


P_chamber = 1e-6;
T = 20 + 273;
n_real = P_chamber/(kB*T);
n_sim = N_atom/(pi*D_chamber^2/4*(L_chamber - L_py));
real_load_flux = mean_flux*n_real/n_sim*Fvc
N_trapped = 100*n_real/n_sim*Fvc;
real_flux = real_load_flux*mean_loss
%%
%%
%single run with flash boundary conditon
velo = 10;
dt = D_chamber/abs(velo)/3000;
t_total = 0.02;
step = 20;


%set the laser detuning and magnetic field gradient
delta_variable = - 4;    %(per Gamma)
beta_variable = 8;    %(per G/cm)
delta = delta_variable*Gamma;
beta = beta_variable*0.01;

%set laser power and beam waist
P = 380e-3;
Dx0 = 35e-3;
Dy0 = 35e-3;

%initilize the atoms
x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

in_time = zeros(1,N_atom);

time_step = 200;    



%magnetic center
x_c = 0;
y_c = 0;
z_c = -L_py/2;

fig = figure;

%set 
flux_record = zeros(1,step);
hit_ratio_record = zeros(1,step);
loss_ratio_record = zeros(1,step);
time_record = linspace(0,t_total,step);
inside_num_record = zeros(1,step);
loss_record = zeros(1,step);
velocity_record = zeros(1,step);
angle_record = zeros(1,step);


if_ever_in_py = zeros(1,N_atom);
in_time = zeros(1,N_atom);

if_hit_wall = zeros(1,N_atom);


%record part of the output atom
N_record = 10000;
Record_out_v = zeros(1,N_record);
Record_initial_v = zeros(1,N_record);
Record_angle_x = zeros(1,N_record);

%record the atoms coming from the aperture
aperture_record_num = 2000;
aperture_atom_x = [];
aperture_atom_y = [];
aperture_atom_vx =[];
aperture_atom_vy = [];
aperture_atom_vz = [];


initial_v = sqrt(vx0.^2 + vy0.^2 + vz0.^2);

current_time = 0;
record_index = 0;
aperture_index = 0;

TotalOutNum = 0;

for j = 1:round(step)
out_num_from_wall = 0;
out_num = 0;
loss = 0;
output_velocity = 0;
output_angle = 0;
loss_num_through_surface = 0;
for i = 1:time_step
    
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        py_outer_wall_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2 - d_t) & (x < -z + d_hole/2 + d_t) & (y > z - d_hole/2 - d_t) & (y < -z + d_hole/2 + d_t));
        output_index = (z >= 0) & ((abs(x) <= d_hole/2)& (abs(y) <= d_hole/2)) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= 0.2);    %the atoms going out the pyramid through the hole
 
        stick_py_index = py_outer_wall_index & (~py_inside_index) & (if_ever_in_py);   %atoms hitting the pyramid walls
        stick_chamber_index = ((sqrt(x.^2 + y.^2) >= D_chamber/2) | (z <= -L_chamber) | ((z >= 0)));   %atoms sticking to the chamber walls
        if_hit_wall(py_outer_wall_index & (~py_inside_index)) = 1;
        
        if_hit_aperture = (z <= -L_py) & (z >= -L_py - 0.3e-3) & (abs(x) <= D_py/2 + d_hole/2) & (abs(y) <= D_py/2 + d_hole/2) &(~if_ever_in_py);
        
        in_time(py_inside_index & (~if_ever_in_py)) = current_time;     %the time when atoms move into the pyramid
        
        trap_time = current_time - in_time(output_index);      %the time atoms spend inside the pyramid
        
        out_num = out_num + length(output_index(output_index == 1));    
        judge_hit_wall = if_hit_wall(output_index);
        out_num_from_wall = out_num_from_wall + length(judge_hit_wall(judge_hit_wall == 1));
        loss_through_wall_index = (z >= - L_py)&(~py_outer_wall_index);
        loss_num_through_surface = loss_num_through_surface + length(loss_through_wall_index(loss_through_wall_index == 1));
        
        %update pre-set statistics
        loss = loss + sum(exp(-Gamma_collision*trap_time));
        v = sqrt(vx.^2 + vy.^2 + vz.^2);
        angle = atan(sqrt(vx.^2 + vy.^2)./vz);
        angle_x = atan(vx./vz);
        output_velocity = output_velocity+ sum(v(output_index));
        output_angle = output_angle + sum(angle(output_index));
        
        v_out = v(output_index);
        v_initial = initial_v(output_index);
        angle_x_out = angle_x(output_index);
        
        if(record_index <= N_record*0.8) && (length(v_out) >= 1);
        for out_index  = 1:length(length(v_out))
            Record_out_v(record_index + out_index) = v_out(out_index);
            Record_initial_v(record_index + out_index) = v_initial(out_index);
            Record_angle_x(record_index + out_index) = angle_x_out(out_index);
        end
        record_index = record_index + length(v_out);
        end
        
        if(current_time >= 0.01)
        aperture_atom_x = [aperture_atom_x, x(if_hit_aperture)];
        aperture_atom_y = [aperture_atom_y, y(if_hit_aperture)];
        aperture_atom_vx = [aperture_atom_vx, vx(if_hit_aperture)];
        aperture_atom_vy = [aperture_atom_vy, vy(if_hit_aperture)];
        aperture_atom_vz = [aperture_atom_vz, vz(if_hit_aperture)];
        end
        
            
        if_ever_in_py(py_inside_index) = 1;
        
        %renew the atoms sticking to the chamber wall
        stick_chamber_index = stick_chamber_index | stick_py_index;
        stick_chamber_real_index = find(stick_chamber_index == 1);
        N_stick_chamber = length(stick_chamber_real_index);
        
        if_ever_in_py(stick_chamber_index) = 0;
        in_time(stick_chamber_index) = 0;

x_add_chamber = zeros(1,N_stick_chamber);
y_add_chamber = zeros(1,N_stick_chamber);
z_add_chamber = zeros(1,N_stick_chamber);
for atom_index = 1:N_stick_chamber
    x_temp = - D_chamber/2 + rand()*D_chamber;
    y_temp = - D_chamber/2 + rand()*D_chamber;
    z_temp = - L_chamber + rand()*L_chamber;
     while(((z_temp >= - L_py) && (x_temp > z_temp - d_hole/2) ...
                        && (x_temp < - z_temp + d_hole/2) && ...
                        (y_temp > z_temp - d_hole/2) && ...
                        (y_temp < - z_temp + d_hole/2)))
              x_temp = - D_chamber/2 + rand()*D_chamber;
              y_temp  = - D_chamber/2 + rand()*D_chamber;
              z_temp = - L_chamber + rand()*L_chamber;  
     end
     x_add_chamber(1,atom_index) = x_temp;
     y_add_chamber(1,atom_index) = y_temp;
     z_add_chamber(1,atom_index) = z_temp;
end

        x(stick_chamber_index) = x_add_chamber;
        y(stick_chamber_index) = y_add_chamber;
        z(stick_chamber_index) = z_add_chamber;
        %vx(stick_chamber_index) = vx_add_chamber;
        %vy(stick_chamber_index) = vy_add_chamber;
        %vz(stick_chamber_index) = vz_add_chamber;
        
        %initial_v(stick_chamber_index) = sqrt(vx_add_chamber.^2 + vy_add_chamber.^2 + vz_add_chamber.^2);
        if_hit_wall(stick_chamber_index) = 0;    
        
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        
        %calculate the force
        costhetax = -(x - x_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetay = -(y - y_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetaz = 2*(z - z_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
 
        Br = beta*sqrt((x - x_c).^2+ (y - y_c).^2 + 4*(z - z_c).^2);

        Sz1 = Gaussion_intensity(x,y,z,0,0,P,Dx0,Dy0,0)/I0;
        Sz2 = Reflectivity^2*Gaussion_intensity(x,y,z,pi,0,P,Dx0,Dy0,0)/I0;

        Sx1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0, d_hole/2)/I0;
        Sx2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,3*pi/2,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi,P,Dx0,Dy0, d_hole/2)/I0;
        
        Sz2((abs(x) <= d_hole/2) & (abs(y) <= d_hole/2)) = 0;
        Sz2((abs(x) >= D_py/2) | (abs(y) >= D_py/2)) = 0;
        Sz1((z >= - L_py) & (~py_inside_index) ) = 0;
        Sz2((z >= - L_py) & (~py_inside_index) ) = 0;
        %Sz1( (~py_inside_index) ) = 0;
        %Sz2( (~py_inside_index) ) = 0;
        Sx1(~py_inside_index) = 0;
        Sx2(~py_inside_index) = 0;
        Sy1(~py_inside_index) = 0;
        Sy2(~py_inside_index) = 0;
        
        St = 1*(Sx1 + Sx2 + Sy1 + Sy2 + Sz1 + Sz2);

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx1+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx1;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx2+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx2;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy1+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy1;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy2+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy2;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz1+4*(delta - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz1;
    
        Rzm = (1/4*(- costhetaz - 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(- costhetaz + 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz2+4*(delta + k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz2;
           
        R_t = (Rxp + Rxm + Ryp + Rym + Rzp + Rzm);
        
        
        theta_recoil = rand(1,length(x))*pi;
        phi_recoil = rand(1,length(x))*2*pi;
        
        direcx = randi([-1,1],1,length(x));
        direcy = randi([-1,1],1,length(x));
        direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm);  %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym); %+ hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp - Rzm);  %+ hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
       
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
        
       
end
    
    TotalOutNum = TotalOutNum + out_num;
    %record pre-set statistics
    flux_record(j) = out_num/(time_step*dt);
    loss_record(j) = loss/out_num;
    velocity_record(j) = output_velocity/out_num;
    angle_record(j) = output_angle/out_num;
    hit_ratio_record(j) = out_num_from_wall/out_num;
    loss_ratio_record(j) = loss_num_through_surface/out_num;
    
    
    plot3(plot_x1,plot_y1,plot_z, 'k', plot_x2,plot_y1,plot_z, 'k', plot_x1,plot_y2,plot_z, 'k', plot_x2,plot_y2,plot_z, 'k');
    hold on;
    plot3(x,y,z,'o');
    xlabel('x');
    ylabel('y');
    zlabel('z');
    xlim([- D_chamber/2, D_chamber/2]);
    ylim([- D_chamber/2, D_chamber/2]);
    zlim([- L_chamber, 0.01]);
    %view([30,15]);
    view([0,0]);
    hold off;
    
    drawnow
    frame = getframe(fig);
    im{j} = frame2im(frame);
    %pause(0.0001);
   

end

figure;
plot(time_record, flux_record,'.');
xlabel('t');
ylabel('flux')

figure;
plot(time_record, loss_record,'.');
xlabel('t');
ylabel('loss')

figure;
plot(time_record, velocity_record,'.');
xlabel('t');
ylabel('v');

figure;
plot(time_record, angle_record,'.');
xlabel('t');
ylabel('angle');

figure;
plot(time_record, hit_ratio_record,'.');
xlabel('t');
ylabel('hit ratio');

figure;
plot(time_record, loss_ratio_record,'.');
xlabel('t');
ylabel('loss ratio');

Recorded_out_v = Record_out_v(Record_out_v ~= 0);
Recorded_initial_v = Record_initial_v(Record_initial_v ~= 0);
Recorded_angle_x = Record_angle_x(Record_angle_x ~= 0);

figure;
histogram(Recorded_out_v,20);
xlabel('v_{out} (m/s)','fontsize',20);
%xlim([8,40]);
title('v_{out} @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 6 mm, L_{chamber} = 120 mm' );
set(gca,'FontSize',16);

figure;
histogram(Recorded_angle_x,200);
xlabel('\theta_x','fontsize',20);
title('\theta_{x} @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 6 mm, L_{chamber} = 120 mm' );
%xlim([-0.2, 0.2])
set(gca,'FontSize',16);

figure;
histogram(Recorded_initial_v,100);
xlabel('v_{initial}','fontsize',20);
title('v_{initial} (m/s) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 9 mm' );
set(gca,'FontSize',16);

hot_v = Recorded_out_v(Recorded_out_v >= 20);
hot_theta = Recorded_angle_x(Recorded_out_v >= 20);
figure;
histogram(hot_v, 100);
title('v_{out} of hot atoms @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 9 mm, L_{chamber} = 120 mm' );
set(gca,'FontSize',16);
figure;
histogram(hot_theta, 100);
title('\theta_{x} of hot atoms @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 9 mm, L_{chamber} = 120 mm' );
set(gca,'FontSize',16);

figure;
histogram(sqrt(aperture_atom_vx.^2 + aperture_atom_vy.^2 + aperture_atom_vz.^2),100);
xlabel('v_{through}','fontsize',20);
title('v_{through} (m/s) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm, L_{chamber} = 150 mm, with slower' );
set(gca,'FontSize',16);

figure;
plot(aperture_atom_x, aperture_atom_y, 'o', 'MarkerSize',2);
xlabel('x_{through}','fontsize',20);
ylabel('y_{through}','fontsize',20);
title('Postion @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm, L_{chamber} = 150 mm, with slower' );
set(gca,'FontSize',16);

mean_flux = mean(flux_record(end - 5: end));

mean_loss = mean(loss_record(end - 5:end));

mean_velocity = mean(velocity_record(end-5:end));

mean_angle = mean(angle_record(end-5:end));

mean_hit_ratio = mean(hit_ratio_record(end-5:end));

mean_loss_ratio = mean(loss_ratio_record(end-5:end));


P_chamber = 1e-6;
T = 20 + 273;
n_real = P_chamber/(kB*T);
n_sim = N_atom/(pi*D_chamber^2/4*(L_chamber - L_py));
real_load_flux = mean_flux*n_real/n_sim*Fvc
N_trapped = 100*n_real/n_sim*Fvc;
real_flux = real_load_flux*mean_loss


%%
filename = '/Users/XQ/desktop/pyramid_picture2.gif';
for index = 1:round(step/time_step)
    [A,map] = rgb2ind(im{index},256);
    if index == 1
        imwrite(A,map, filename, 'gif','LoopCount', Inf, 'DelayTime',0.01);
    else
        imwrite(A,map, filename, 'gif','WriteMode', 'append', 'DelayTime',0.01);
    end
end

%%
%1d scan chamber length
velo = 10;
dt = D_chamber/abs(velo)/3000;
t_total = 0.02;
step = round(t_total/dt);
time_step = 500;

d_hole = 2e-3;


delta_variable = - 3.5;    %(per Gamma)
beta_variable = 8;    %(per G/cm)
delta = delta_variable*Gamma;
beta = beta_variable*0.01;

P = 380e-3;
Dx0 = 35e-3;
Dy0 = 35e-3;

scan_L_chamber = linspace(40,200,10);
scan_flux = zeros(1,length(scan_L_chamber));
scan_angle_div = zeros(1,length(scan_L_chamber));
scan_hit_ratio = zeros(1,length(scan_L_chamber));
scan_loss_ratio = zeros(1,length(scan_L_chamber));
scan_velocity = zeros(1,length(scan_L_chamber));
scan_velocity_div = zeros(1,length(scan_L_chamber));

%push beam



for dhole_index = 1:length(scan_L_chamber)

L_chamber = scan_L_chamber(dhole_index)*1e-3;

in_time = zeros(1,N_atom);

x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

for j = 1:round(step/time_step)
out_num_from_wall = 0;
out_num = 0;
loss = 0;
output_velocity = 0;
output_angle = 0;
loss_num_through_surface = 0;
for i = 1:time_step
    
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        py_outer_wall_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2 - d_t) & (x < -z + d_hole/2 + d_t) & (y > z - d_hole/2 - d_t) & (y < -z + d_hole/2 + d_t));
        output_index = (z >= 0) & ((abs(x) <= d_hole/2)& (abs(y) <= d_hole/2)) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= pi/2);
 
        stick_py_index = py_outer_wall_index & (~py_inside_index) & (if_ever_in_py);
        stick_chamber_index = ((sqrt(x.^2 + y.^2) >= D_chamber/2) | (z <= -L_chamber) | ((z >= 0)));
        if_hit_wall(py_outer_wall_index & (~py_inside_index)) = 1;
        
        in_time(py_inside_index & (~if_ever_in_py)) = current_time;
        
        trap_time = current_time - in_time(output_index);
        
        out_num = out_num + length(output_index(output_index == 1));
        judge_hit_wall = if_hit_wall(output_index);
        out_num_from_wall = out_num_from_wall + length(judge_hit_wall(judge_hit_wall == 1));
        loss_through_wall_index = (z >= - L_py)&(~py_outer_wall_index);
        loss_num_through_surface = loss_num_through_surface + length(loss_through_wall_index(loss_through_wall_index == 1));
        
        loss = loss + sum(exp(-Gamma_collision*trap_time));
        v = sqrt(vx.^2 + vy.^2 + vz.^2);
        angle = atan(sqrt(vx.^2 + vy.^2)./vz);
        angle_x = atan(vx./vz);
        output_velocity = output_velocity+ sum(v(output_index));
        output_angle = output_angle + sum(angle(output_index));
        
        v_out = v(output_index);
        v_initial = initial_v(output_index);
        angle_x_out = angle_x(output_index);
        %record the output atoms
        if(record_index <= N_record*0.8) && (length(v_out) >= 1);
        for out_index  = 1:length(length(v_out))
            Record_out_v(record_index + out_index) = v_out(out_index);
            Record_initial_v(record_index + out_index) = v_initial(out_index);
            Record_angle_x(record_index + out_index) = angle_x_out(out_index);
        end
        record_index = record_index + length(v_out);
        end
            
        if_ever_in_py(py_inside_index) = 1;
        
         %discard the atoms sticking to the chamber wall
        % stick_chamber_index = stick_chamber_index;
        stick_chamber_index = stick_chamber_index | stick_py_index;
        stick_chamber_real_index = find(stick_chamber_index == 1);
        N_stick_chamber = length(stick_chamber_real_index);
        
        if_ever_in_py(stick_chamber_index) = 0;
        in_time(stick_chamber_index) = 0;

    z_add_chamber = (rand(1,N_stick_chamber)-1)* L_chamber;      
    x_add_chamber = (rand(1,N_stick_chamber)-0.5)*D_chamber/sqrt(2);
    y_add_chamber = (rand(1,N_stick_chamber)-0.5)*D_chamber/sqrt(2);
    vx_add_chamber = zeros(1,N_stick_chamber);
    vy_add_chamber = zeros(1,N_stick_chamber);
    vz_add_chamber = zeros(1,N_stick_chamber);

for atom_index = 1:N_stick_chamber
    v_random = rand()*vmax;
    p = VDF(v_random)/fmax;
    s = rand();
    while(s > p)
        v_random = rand()*vmax;
        p = VDF(v_random)/fmax;
        s = rand();
    end
    theta_random = rand()*pi;
    p = j_theta(theta_random)/1;
    s = rand();
    while(s > p)
        theta_random = rand()*pi;
        p = j_theta(theta_random)/1;
        s = rand();
    end
    phi_random = rand()*2*pi;
    
    vz_add_chamber(1,atom_index) = v_random*cos(theta_random);
    vx_add_chamber(1,atom_index) = v_random*sin(theta_random)*cos(phi_random);
    vy_add_chamber(1,atom_index) = v_random*sin(theta_random)*sin(phi_random);
    
end
        x(stick_chamber_index) = x_add_chamber;
        y(stick_chamber_index) = y_add_chamber;
        z(stick_chamber_index) = z_add_chamber;
        vx(stick_chamber_index) = vx_add_chamber;
        vy(stick_chamber_index) = vy_add_chamber;
        vz(stick_chamber_index) = vz_add_chamber;
        
        initial_v(stick_chamber_index) = sqrt(vx_add_chamber.^2 + vy_add_chamber.^2 + vz_add_chamber.^2);
        if_hit_wall(stick_chamber_index) = 0;    
        
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        
        %calculate the force
        costhetax = -(x - x_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetay = -(y - y_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetaz = 2*(z - z_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
 
        Br = beta*sqrt((x - x_c).^2+ (y - y_c).^2 + 4*(z - z_c).^2);

        Sz1 = Gaussion_intensity(x,y,z,0,0,P,Dx0,Dy0,0)/I0;
        Sz2 = Reflectivity^2*Gaussion_intensity(x,y,z,pi,0,P,Dx0,Dy0,0)/I0;

        Sx1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0, d_hole/2)/I0;
        Sx2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,3*pi/2,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi,P,Dx0,Dy0, d_hole/2)/I0;
        
        Sz2((abs(x) <= d_hole/2) & (abs(y) <= d_hole/2)) = 0;
        Sz1((z >= - L_py) & (~py_inside_index) ) = 0;
        Sz2((z >= - L_py) & (~py_inside_index) ) = 0;
        Sx1(~py_inside_index) = 0;
        Sx2(~py_inside_index) = 0;
        Sy1(~py_inside_index) = 0;
        Sy2(~py_inside_index) = 0;
        
        St = 1*(Sx1 + Sx2 + Sy1 + Sy2 + Sz1 + Sz2);

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx1+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx1;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx2+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx2;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy1+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy1;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy2+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy2;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz1+4*(delta - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz1;
    
        Rzm = (1/4*(- costhetaz - 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(- costhetaz + 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz2+4*(delta + k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz2;
           
        R_t = (Rxp + Rxm + Ryp + Rym + Rzp + Rzm);
        
        
        theta_recoil = rand(1,length(x))*pi;
        phi_recoil = rand(1,length(x))*2*pi;
        
        direcx = randi([-1,1],1,length(x));
        direcy = randi([-1,1],1,length(x));
        direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm)  + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym) + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp - Rzm)  + hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
       
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
        
       
end

    
    flux_record(j) = out_num/(time_step*dt);
    inside_num_record(j) = length(py_inside_index(py_inside_index == 1));
    loss_record(j) = loss/out_num;
    velocity_record(j) = output_velocity/out_num;
    angle_record(j) = output_angle/out_num;
    hit_ratio_record(j) = out_num_from_wall/out_num;
    loss_ratio_record(j) = loss_num_through_surface/out_num;
    
     

end


Recorded_out_v = Record_out_v(Record_out_v ~= 0);
Recorded_angle_x = Record_angle_x(Record_angle_x ~= 0);

mean_flux = mean(flux_record(end - 5: end));

mean_loss = mean(loss_record(end - 5:end));

mean_velocity = mean(Recorded_out_v);
mean_velocity_div = std(Recorded_out_v);
mean_angle_div = std(Recorded_angle_x);

mean_hit_ratio = mean(hit_ratio_record(end-5:end));

mean_loss_ratio = mean(loss_ratio_record(end-5:end));


n_real = P_chamber/(kB*T);
n_sim = N_atom/(pi*D_chamber^2/4*(L_chamber - L_py));
real_load_flux = mean_flux*n_real/n_sim*Fvc;
real_flux = real_load_flux*mean_loss;

scan_flux(dhole_index) = real_flux;
scan_hit_ratio(dhole_index) = mean_hit_ratio;
scan_loss_ratio (dhole_index) = mean_loss_ratio;
scan_velocity(dhole_index) = mean_velocity;
scan_velocity_div(dhole_index) = mean_velocity_div;
scan_angle_div(dhole_index) = mean_angle_div;

end

figure;
plot(scan_L_chamber, scan_flux, '.');
xlabel('d_{hole}', 'fontsize',20);
ylabel('flux (s^{-1})', 'fontsize',20);
title('flux @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_L_chamber, scan_velocity, '.');
xlabel('L_{chamber} (mm)', 'fontsize',20);
ylabel( 'mean(v) (m/s)', 'fontsize',20);
title('mean(v) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_L_chamber, scan_velocity_div, '.');
xlabel('L_{chamber} (mm)', 'fontsize',20);
ylabel( 'std(v) (m/s)', 'fontsize',20);
title('std(v) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_L_chamber, scan_angle_div, '.');
xlabel('L_{chamber} (mm)', 'fontsize',20);
ylabel( 'std(\theta_{x}) (m/s)', 'fontsize',20);
title('std(\theta_{x}) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_L_chamber, scan_loss_ratio, '.');
xlabel('L_{chamber} (mm)', 'fontsize',20);
ylabel( 'Loss ratio', 'fontsize',20);
title('Loss ratio @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);


%%
%1d scan hole size
velo = 10;
dt = D_chamber/abs(velo)/3000;
t_total = 0.02;
step = round(t_total/dt);
time_step = 500;



delta_variable = - 3.5;    %(per Gamma)
beta_variable = 8;    %(per G/cm)
delta = delta_variable*Gamma;
beta = beta_variable*0.01;

P = 380e-3;
Dx0 = 35e-3;
Dy0 = 35e-3;

L_chamber = 120e-3;
%push beam



for dhole_index = 1:length(scan_d_hole)


d_hole = scan_d_hole(dhole_index)*1e-3;

in_time = zeros(1,N_atom);

x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

%magnetic center
x_c = 0;
y_c = 0;
z_c = -L_py/2;

flux_record = zeros(1,round(step/time_step));
hit_ratio_record = zeros(1,round(step/time_step));
loss_ratio_record = zeros(1,round(step/time_step));
time_record = linspace(0,t_total,round(step/time_step));
inside_num_record = zeros(1,round(step/time_step));
loss_record = zeros(1,round(step/time_step));
velocity_record = zeros(1,round(step/time_step));
angle_record = zeros(1,round(step/time_step));
if_ever_in_py = zeros(1,N_atom);
in_time = zeros(1,N_atom);

if_hit_wall = zeros(1,N_atom);


%record part of the output atom
N_record = 10000;
Record_out_v = zeros(1,N_record);
Record_initial_v = zeros(1,N_record);
Record_angle_x = zeros(1,N_record);

initial_v = sqrt(vx0.^2 + vy0.^2 + vz0.^2);

current_time = 0;
record_index = 0;

for j = 1:round(step/time_step)
out_num_from_wall = 0;
out_num = 0;
loss = 0;
output_velocity = 0;
output_angle = 0;
loss_num_through_surface = 0;
for i = 1:time_step
    
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        py_outer_wall_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2 - d_t) & (x < -z + d_hole/2 + d_t) & (y > z - d_hole/2 - d_t) & (y < -z + d_hole/2 + d_t));
        output_index = (z >= 0) & ((abs(x) <= d_hole/2)& (abs(y) <= d_hole/2)) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= pi/2);
 
        stick_py_index = py_outer_wall_index & (~py_inside_index) & (if_ever_in_py);
        stick_chamber_index = ((sqrt(x.^2 + y.^2) >= D_chamber/2) | (z <= -L_chamber) | ((z >= -L_py) & (~py_outer_wall_index)));
        if_hit_wall(py_outer_wall_index & (~py_inside_index)) = 1;
        
        in_time(py_inside_index & (~if_ever_in_py)) = current_time;
        
        trap_time = current_time - in_time(output_index);
        
        out_num = out_num + length(output_index(output_index == 1));
        judge_hit_wall = if_hit_wall(output_index);
        out_num_from_wall = out_num_from_wall + length(judge_hit_wall(judge_hit_wall == 1));
        loss_through_wall_index = (z >= - L_py)&(~py_outer_wall_index);
        loss_num_through_surface = loss_num_through_surface + length(loss_through_wall_index(loss_through_wall_index == 1));
        
        loss = loss + sum(exp(-Gamma_collision*trap_time));
        v = sqrt(vx.^2 + vy.^2 + vz.^2);
        angle = atan(sqrt(vx.^2 + vy.^2)./vz);
        angle_x = atan(vx./vz);
        output_velocity = output_velocity+ sum(v(output_index));
        output_angle = output_angle + sum(angle(output_index));
        
        v_out = v(output_index);
        v_initial = initial_v(output_index);
        angle_x_out = angle_x(output_index);
        %record the output atoms
        if(record_index <= N_record*0.8) && (length(v_out) >= 1);
        for out_index  = 1:length(length(v_out))
            Record_out_v(record_index + out_index) = v_out(out_index);
            Record_initial_v(record_index + out_index) = v_initial(out_index);
            Record_angle_x(record_index + out_index) = angle_x_out(out_index);
        end
        record_index = record_index + length(v_out);
        end
            
        if_ever_in_py(py_inside_index) = 1;
        
         %discard the atoms sticking to the chamber wall
        % stick_chamber_index = stick_chamber_index;
        stick_chamber_index = stick_chamber_index | stick_py_index;
        stick_chamber_real_index = find(stick_chamber_index == 1);
        N_stick_chamber = length(stick_chamber_real_index);
        
        if_ever_in_py(stick_chamber_index) = 0;
        in_time(stick_chamber_index) = 0;

    z_add_chamber = (rand(1,N_stick_chamber)-1)* L_chamber;      
    x_add_chamber = (rand(1,N_stick_chamber)-0.5)*D_chamber/sqrt(2);
    y_add_chamber = (rand(1,N_stick_chamber)-0.5)*D_chamber/sqrt(2);
    vx_add_chamber = zeros(1,N_stick_chamber);
    vy_add_chamber = zeros(1,N_stick_chamber);
    vz_add_chamber = zeros(1,N_stick_chamber);

for atom_index = 1:N_stick_chamber
    v_random = rand()*vmax;
    p = VDF(v_random)/fmax;
    s = rand();
    while(s > p)
        v_random = rand()*vmax;
        p = VDF(v_random)/fmax;
        s = rand();
    end
    theta_random = rand()*pi;
    p = j_theta(theta_random)/1;
    s = rand();
    while(s > p)
        theta_random = rand()*pi;
        p = j_theta(theta_random)/1;
        s = rand();
    end
    phi_random = rand()*2*pi;
    
    vz_add_chamber(1,atom_index) = v_random*cos(theta_random);
    vx_add_chamber(1,atom_index) = v_random*sin(theta_random)*cos(phi_random);
    vy_add_chamber(1,atom_index) = v_random*sin(theta_random)*sin(phi_random);
    
end
        x(stick_chamber_index) = x_add_chamber;
        y(stick_chamber_index) = y_add_chamber;
        z(stick_chamber_index) = z_add_chamber;
        vx(stick_chamber_index) = vx_add_chamber;
        vy(stick_chamber_index) = vy_add_chamber;
        vz(stick_chamber_index) = vz_add_chamber;
        
        initial_v(stick_chamber_index) = sqrt(vx_add_chamber.^2 + vy_add_chamber.^2 + vz_add_chamber.^2);
        if_hit_wall(stick_chamber_index) = 0;    
        
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        
        %calculate the force
        costhetax = -(x - x_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetay = -(y - y_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetaz = 2*(z - z_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
 
        Br = beta*sqrt((x - x_c).^2+ (y - y_c).^2 + 4*(z - z_c).^2);

        Sz1 = Gaussion_intensity(x,y,z,0,0,P,Dx0,Dy0,0)/I0;
        Sz2 = Reflectivity^2*Gaussion_intensity(x,y,z,pi,0,P,Dx0,Dy0,0)/I0;

        Sx1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0, d_hole/2)/I0;
        Sx2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,3*pi/2,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi,P,Dx0,Dy0, d_hole/2)/I0;
        
        Sz2((abs(x) <= d_hole/2) & (abs(y) <= d_hole/2)) = 0;
        Sz1((z >= - L_py) & (~py_inside_index) ) = 0;
        Sz2((z >= - L_py) & (~py_inside_index) ) = 0;
        Sx1(~py_inside_index) = 0;
        Sx2(~py_inside_index) = 0;
        Sy1(~py_inside_index) = 0;
        Sy2(~py_inside_index) = 0;
        
        St = 1*(Sx1 + Sx2 + Sy1 + Sy2 + Sz1 + Sz2);

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx1+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx1;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx2+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx2;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy1+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy1;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy2+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy2;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz1+4*(delta - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz1;
    
        Rzm = (1/4*(- costhetaz - 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(- costhetaz + 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz2+4*(delta + k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz2;
           
        R_t = (Rxp + Rxm + Ryp + Rym + Rzp + Rzm);
        
        
        theta_recoil = rand(1,length(x))*pi;
        phi_recoil = rand(1,length(x))*2*pi;
        
        direcx = randi([-1,1],1,length(x));
        direcy = randi([-1,1],1,length(x));
        direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm)  + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym) + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp - Rzm)  + hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
       
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
        
       
end

    
    flux_record(j) = out_num/(time_step*dt);
    inside_num_record(j) = length(py_inside_index(py_inside_index == 1));
    loss_record(j) = loss/out_num;
    velocity_record(j) = output_velocity/out_num;
    angle_record(j) = output_angle/out_num;
    hit_ratio_record(j) = out_num_from_wall/out_num;
    loss_ratio_record(j) = loss_num_through_surface/out_num;
    
     

end


Recorded_out_v = Record_out_v(Record_out_v ~= 0);
Recorded_angle_x = Record_angle_x(Record_angle_x ~= 0);

mean_flux = mean(flux_record(end - 5: end));

mean_loss = mean(loss_record(end - 5:end));

mean_velocity = mean(Recorded_out_v);
mean_velocity_div = std(Recorded_out_v);
mean_angle_div = std(Recorded_angle_x);

mean_hit_ratio = mean(hit_ratio_record(end-5:end));

mean_loss_ratio = mean(loss_ratio_record(end-5:end));


n_real = P_chamber/(kB*T);
n_sim = N_atom/(pi*D_chamber^2/4*(L_chamber));
real_load_flux = mean_flux*n_real/n_sim*Fvc;
real_flux = real_load_flux*mean_loss;

scan_flux(dhole_index) = real_flux;
scan_hit_ratio(dhole_index) = mean_hit_ratio;
scan_loss_ratio (dhole_index) = mean_loss_ratio;
scan_velocity(dhole_index) = mean_velocity;
scan_velocity_div(dhole_index) = mean_velocity_div;
scan_angle_div(dhole_index) = mean_angle_div;

end

figure;
plot(scan_d_hole, scan_flux, '.');
xlabel('d_{hole}', 'fontsize',20);
ylabel('flux (s^{-1})', 'fontsize',20);
title('flux @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_d_hole, scan_velocity, '.');
xlabel('d_{hole} (mm) (mm)', 'fontsize',20);
ylabel( 'mean(v) (m/s)', 'fontsize',20);
title('mean(v) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_d_hole, scan_velocity_div, '.');
xlabel('d_{hole} (mm) (mm)', 'fontsize',20);
ylabel( 'std(v) (m/s)', 'fontsize',20);
title('std(v) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_L_chamber, scan_angle_div, '.');
xlabel('d_{hole} (mm) (mm)', 'fontsize',20);
ylabel( 'std(\theta_{x}) (m/s)', 'fontsize',20);
title('std(\theta_{x}) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_d_hole, scan_loss_ratio, '.');
xlabel('d_{hole} (mm)', 'fontsize',20);
ylabel( 'Loss ratio', 'fontsize',20);
title('Loss ratio @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

%%
%1d scan zero B point

velo = 10;
dt = D_chamber/abs(velo)/3000;
t_total = 0.02;
step = round(t_total/dt);
time_step = 500;

d_hole = 2e-3;
L_chamber = 120e-3;


delta_variable = - 3.5;    %(per Gamma)
beta_variable = 8;    %(per G/cm)
delta = delta_variable*Gamma;
beta = beta_variable*0.01;
 
Dx0 = 35e-3;
Dy0 = 35e-3;

scan_zc = linspace(10,28,10);
scan_flux = zeros(1,length(scan_zc));
scan_angle_div = zeros(1,length(scan_zc));
scan_hit_ratio = zeros(1,length(scan_zc));
scan_loss_ratio = zeros(1,length(scan_zc));
scan_velocity = zeros(1,length(scan_zc));
scan_velocity_div = zeros(1,length(scan_zc));



%push beam



for zc_index = 1:length(scan_zc)

z_c = - scan_zc(zc_index)*1e-3;

in_time = zeros(1,N_atom);

x = x0;
y = y0;
z = z0;
vx = vx0;
vy = vy0;
vz = vz0;

flux_record = zeros(1,round(step/time_step));
hit_ratio_record = zeros(1,round(step/time_step));
loss_ratio_record = zeros(1,round(step/time_step));
time_record = linspace(0,t_total,round(step/time_step));
inside_num_record = zeros(1,round(step/time_step));
loss_record = zeros(1,round(step/time_step));
velocity_record = zeros(1,round(step/time_step));
angle_record = zeros(1,round(step/time_step));
if_ever_in_py = zeros(1,N_atom);
in_time = zeros(1,N_atom);

if_hit_wall = zeros(1,N_atom);


%record part of the output atom
N_record = 10000;
Record_out_v = zeros(1,N_record);
Record_initial_v = zeros(1,N_record);
Record_angle_x = zeros(1,N_record);

initial_v = sqrt(vx0.^2 + vy0.^2 + vz0.^2);

current_time = 0;
record_index = 0;

for j = 1:round(step/time_step)
out_num_from_wall = 0;
out_num = 0;
loss = 0;
output_velocity = 0;
output_angle = 0;
loss_num_through_surface = 0;
for i = 1:time_step
    
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        py_outer_wall_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2 - d_t) & (x < -z + d_hole/2 + d_t) & (y > z - d_hole/2 - d_t) & (y < -z + d_hole/2 + d_t));
        output_index = (z >= 0) & ((abs(x) <= d_hole/2)& (abs(y) <= d_hole/2)) & (atan(sqrt(vx.^2 + vy.^2)./vz) <= pi/2);
 
        stick_py_index = py_outer_wall_index & (~py_inside_index) & (if_ever_in_py);
        stick_chamber_index = ((sqrt(x.^2 + y.^2) >= D_chamber/2) | (z <= -L_chamber) | ((z >= -L_py)&(~py_outer_wall_index)));
        if_hit_wall(py_outer_wall_index & (~py_inside_index)) = 1;
        
        in_time(py_inside_index & (~if_ever_in_py)) = current_time;
        
        trap_time = current_time - in_time(output_index);
        
        out_num = out_num + length(output_index(output_index == 1));
        judge_hit_wall = if_hit_wall(output_index);
        out_num_from_wall = out_num_from_wall + length(judge_hit_wall(judge_hit_wall == 1));
        loss_through_wall_index = (z >= - L_py)&(~py_outer_wall_index);
        loss_num_through_surface = loss_num_through_surface + length(loss_through_wall_index(loss_through_wall_index == 1));
        
        loss = loss + sum(exp(-Gamma_collision*trap_time));
        v = sqrt(vx.^2 + vy.^2 + vz.^2);
        angle = atan(sqrt(vx.^2 + vy.^2)./vz);
        angle_x = atan(vx./vz);
        output_velocity = output_velocity+ sum(v(output_index));
        output_angle = output_angle + sum(angle(output_index));
        
        v_out = v(output_index);
        v_initial = initial_v(output_index);
        angle_x_out = angle_x(output_index);
        %record the output atoms
        if(record_index <= N_record*0.8) && (length(v_out) >= 1);
        for out_index  = 1:length(length(v_out))
            Record_out_v(record_index + out_index) = v_out(out_index);
            Record_initial_v(record_index + out_index) = v_initial(out_index);
            Record_angle_x(record_index + out_index) = angle_x_out(out_index);
        end
        record_index = record_index + length(v_out);
        end
            
        if_ever_in_py(py_inside_index) = 1;
        
         %discard the atoms sticking to the chamber wall
        % stick_chamber_index = stick_chamber_index;
        stick_chamber_index = stick_chamber_index | stick_py_index;
        stick_chamber_real_index = find(stick_chamber_index == 1);
        N_stick_chamber = length(stick_chamber_real_index);
        
        if_ever_in_py(stick_chamber_index) = 0;
        in_time(stick_chamber_index) = 0;

    z_add_chamber = (rand(1,N_stick_chamber)-1)* L_chamber;      
    x_add_chamber = (rand(1,N_stick_chamber)-0.5)*D_chamber/sqrt(2);
    y_add_chamber = (rand(1,N_stick_chamber)-0.5)*D_chamber/sqrt(2);
    vx_add_chamber = zeros(1,N_stick_chamber);
    vy_add_chamber = zeros(1,N_stick_chamber);
    vz_add_chamber = zeros(1,N_stick_chamber);

for atom_index = 1:N_stick_chamber
    v_random = rand()*vmax;
    p = VDF(v_random)/fmax;
    s = rand();
    while(s > p)
        v_random = rand()*vmax;
        p = VDF(v_random)/fmax;
        s = rand();
    end
    theta_random = rand()*pi;
    p = j_theta(theta_random)/1;
    s = rand();
    while(s > p)
        theta_random = rand()*pi;
        p = j_theta(theta_random)/1;
        s = rand();
    end
    phi_random = rand()*2*pi;
    
    vz_add_chamber(1,atom_index) = v_random*cos(theta_random);
    vx_add_chamber(1,atom_index) = v_random*sin(theta_random)*cos(phi_random);
    vy_add_chamber(1,atom_index) = v_random*sin(theta_random)*sin(phi_random);
    
end
        x(stick_chamber_index) = x_add_chamber;
        y(stick_chamber_index) = y_add_chamber;
        z(stick_chamber_index) = z_add_chamber;
        vx(stick_chamber_index) = vx_add_chamber;
        vy(stick_chamber_index) = vy_add_chamber;
        vz(stick_chamber_index) = vz_add_chamber;
        
        initial_v(stick_chamber_index) = sqrt(vx_add_chamber.^2 + vy_add_chamber.^2 + vz_add_chamber.^2);
        if_hit_wall(stick_chamber_index) = 0;    
        
        py_inside_index = ((z >= - L_py) & (z <= 0) & (x > z - d_hole/2) & (x < -z + d_hole/2) & (y > z - d_hole/2) & (y < -z + d_hole/2));
        
        %calculate the force
        costhetax = -(x - x_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetay = -(y - y_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
        costhetaz = 2*(z - z_c)./sqrt((x - x_c).^2 + (y - y_c).^2 + 4*(z - z_c).^2);
 
        Br = beta*sqrt((x - x_c).^2+ (y - y_c).^2 + 4*(z - z_c).^2);

        Sz1 = Gaussion_intensity(x,y,z,0,0,P,Dx0,Dy0,0)/I0;
        Sz2 = Reflectivity^2*Gaussion_intensity(x,y,z,pi,0,P,Dx0,Dy0,0)/I0;

        Sx1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi/2,P,Dx0,Dy0, d_hole/2)/I0;
        Sx2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,3*pi/2,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy1 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,0,P,Dx0,Dy0,  d_hole/2)/I0;
        Sy2 = Reflectivity*Gaussion_intensity(x,y,z,pi/2,pi,P,Dx0,Dy0, d_hole/2)/I0;
        
        Sz2((abs(x) <= d_hole/2) & (abs(y) <= d_hole/2)) = 0;
        Sz1((z >= - L_py) & (~py_inside_index) ) = 0;
        Sz2((z >= - L_py) & (~py_inside_index) ) = 0;
        Sx1(~py_inside_index) = 0;
        Sx2(~py_inside_index) = 0;
        Sy1(~py_inside_index) = 0;
        Sy2(~py_inside_index) = 0;
        
        St = 1*(Sx1 + Sx2 + Sy1 + Sy2 + Sz1 + Sz2);

        Rxp = (1/4*(costhetax + 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetax - 1).^2*Gamma/2./(1+Sx1+4*(delta - k*vx - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx1+4*(delta - k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx1;
        
        Rxm = (1/4*(- costhetax + 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetax - 1).^2*Gamma/2./(1+Sx2+4*(delta + k*vx - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetax).^2)*Gamma/2./(1+Sx2+4*(delta + k*vx - muz/hbar*Br).^2/Gamma^2)).*Sx2;
        
        Ryp = (1/4*(costhetay + 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetay - 1).^2*Gamma/2./(1+Sy1+4*(delta - k*vy - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy1+4*(delta - k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy1;
    
        Rym = ( 1/4*(- costhetay + 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mup/hbar*Br).^2/Gamma^2)...
        + 1/4*(- costhetay - 1).^2*Gamma/2./(1+Sy2+4*(delta + k*vy - mum/hbar*Br).^2/Gamma^2)...
        + 1/2*(1-(costhetay).^2)*Gamma/2./(1+Sy2+4*(delta + k*vy - muz/hbar*Br).^2/Gamma^2)).*Sy2;
    
        Rzp = (1/4*(costhetaz - 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(costhetaz + 1).^2*Gamma/2./(1+Sz1+4*(delta - k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz1+4*(delta - k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz1;
    
        Rzm = (1/4*(- costhetaz - 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mup/hbar*Br).^2/Gamma^2) ...
        + 1/4*(- costhetaz + 1).^2*Gamma/2./(1+Sz2+4*(delta + k*vz - mum/hbar*Br).^2/Gamma^2) ...
        +1/2*(1-(costhetaz).^2)*Gamma/2./(1+Sz2+4*(delta + k*vz - muz/hbar*Br).^2/Gamma^2)).*Sz2;
           
        R_t = (Rxp + Rxm + Ryp + Rym + Rzp + Rzm);
        
        
        theta_recoil = rand(1,length(x))*pi;
        phi_recoil = rand(1,length(x))*2*pi;
        
        direcx = randi([-1,1],1,length(x));
        direcy = randi([-1,1],1,length(x));
        direcz = randi([0,1],1,length(x));
        
        
        Fx = hbar*k*(Rxp - Rxm)  + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*cos(phi_recoil) + hbar*k*sqrt((Rxp + Rxm)*dt)/dt.*direcx;
        Fy = hbar*k*(Ryp - Rym) + hbar*k*sqrt(R_t*dt)/dt.*sin(theta_recoil).*sin(phi_recoil) + hbar*k*sqrt((Ryp + Rym)*dt)/dt.*direcy;
        Fz = hbar*k*(Rzp - Rzm)  + hbar*k*sqrt(R_t*dt)/dt.*cos(theta_recoil) + hbar*k*sqrt(Rzp*dt)/dt.*direcz;
       
       
        
       

        vx = vx + Fx/m*dt;
        vy = vy + Fy/m*dt;
        vz = vz + Fz/m*dt;
    
        x = x + vx*dt;
        y = y + vy*dt;
        z = z + vz*dt;
        
        current_time = current_time + dt;
        
       
end

    
    flux_record(j) = out_num/(time_step*dt);
    inside_num_record(j) = length(py_inside_index(py_inside_index == 1));
    loss_record(j) = loss/out_num;
    velocity_record(j) = output_velocity/out_num;
    angle_record(j) = output_angle/out_num;
    hit_ratio_record(j) = out_num_from_wall/out_num;
    loss_ratio_record(j) = loss_num_through_surface/out_num;
    
     

end


Recorded_out_v = Record_out_v(Record_out_v ~= 0);
Recorded_angle_x = Record_angle_x(Record_angle_x ~= 0);

mean_flux = mean(flux_record(end - 5: end));

mean_loss = mean(loss_record(end - 5:end));

mean_velocity = mean(Recorded_out_v);
mean_velocity_div = std(Recorded_out_v);
mean_angle_div = std(Recorded_angle_x);

mean_hit_ratio = mean(hit_ratio_record(end-5:end));

mean_loss_ratio = mean(loss_ratio_record(end-5:end));


n_real = P_chamber/(kB*T);
n_sim = N_atom/(pi*D_chamber^2/4*(L_chamber - L_py));
real_load_flux = mean_flux*n_real/n_sim*Fvc;
real_flux = real_load_flux*mean_loss;

scan_flux(zc_index) = real_flux;
scan_hit_ratio(zc_index) = mean_hit_ratio;
scan_loss_ratio (zc_index) = mean_loss_ratio;
scan_velocity(zc_index) = mean_velocity;
scan_velocity_div(zc_index) = mean_velocity_div;
scan_angle_div(zc_index) = mean_angle_div;

end

figure;
plot(scan_zc, scan_flux, 'o');
hold on;
plot(scan_zc, scan_flux);
xlabel('d_{hole}', 'fontsize',20);
ylabel('flux (s^{-1})', 'fontsize',20);
title('flux @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_zc, scan_velocity, 'o');
hold on;
plot(scan_zc, scan_velocity);
xlabel('L_{chamber} (mm)', 'fontsize',20);
ylabel( 'mean(v) (m/s)', 'fontsize',20);
title('mean(v) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_zc, scan_velocity_div, 'o');
hold on;
plot(scan_zc, scan_velocity_div);
xlabel('L_{chamber} (mm)', 'fontsize',20);
ylabel( 'std(v) (m/s)', 'fontsize',20);
title('std(v) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_zc, scan_angle_div, 'o');
hold on;
plot(scan_zc, scan_angle_div);
xlabel('L_{chamber} (mm)', 'fontsize',20);
ylabel( 'std(\theta_{x}) (m/s)', 'fontsize',20);
title('std(\theta_{x}) @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);

figure;
plot(scan_zc, scan_loss_ratio, 'o');
hold on;
plot(scan_zc, scan_loss_ratio);
xlabel('L_{chamber} (mm)', 'fontsize',20);
ylabel( 'Loss ratio', 'fontsize',20);
title('Loss ratio @ P = 380 mW, D = 35 mm, R = 0.8, d_{hole} = 2 mm');
set(gca,'FontSize',16);