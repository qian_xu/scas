classdef MagneticField <  matlab.mixin.SetGet
    %MAGNETICFIELD Represents the magnetic field (Abstract clasee)
    %  An MagneticField has methods: CalculateMagField
    
    
    properties
        
    end
    
    methods (Abstract)
        CalculateMagField( x,y,z);
    end
    
end

