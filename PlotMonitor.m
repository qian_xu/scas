classdef PlotMonitor
    %PLOTMONITOR Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Simulation
        FigHandle;
        DataPlot;
    end
    
    methods
        function obj = PlotMonitor(Sim)
            %PLOTMONITOR Construct an instance of this class
            %   set the Simulation property
            obj.Simulation = Sim;
        end
        
        function outputArg = PlotAtoms(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
    end
end

