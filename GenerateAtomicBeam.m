function [AtomicBeam] = GenerateAtomicBeam( AtomNum, Vtruncation)
%GENERATEATOMICBEAM generate a list of atoms which form a atomic beam
%   Detailed explanation goes here
m = 1.44e-25;
Gamma = 2*pi*32e6;
I0 = 430;

% Distance from the nozzle to the chamber
ds = 83e-3;
% Initial radius of the atomic beam
rbeam = 0.2e-3;
% Radius of the chamber
rchamber = 30e-3;

f1 = arrayfun(@(x) Atom.Create(m,Gamma,I0, [-ds; (rand()-0.5)*rbeam; (rand()-0.5)*rbeam] ,[0,0,0]), 1:AtomNum, 'UniformOutput', 0);
AtomicBeam = cat(1,f1{:});
muB = 9.27400e-24;
gp = - 1;
gm = 1;
gz = 0;
[AtomicBeam.Mup] = deal(gp*muB);
[AtomicBeam.Mum] = deal(gm*muB);
[AtomicBeam.Muz] = deal(gz*muB);
% set(ThermalAtoms, {'Mup', 'Mum', 'Muz'}, {gp*muB,  gm*muB,  gz*muB});


%Set the velocity of atoms 
BeamVelocities = ones(3,AtomNum);
vmax = Vtruncation;
thetamax = asin(rchamber/0.09);

fmax = max(VDF(linspace(0,Vtruncation,100)));   %Only works when VTruncation <= Vp
jmax = max(JTheta(linspace(0,thetamax,100)));

for AtomIndex = 1:AtomNum
    VRandom = rand()*vmax;
    p = VDF(VRandom)/fmax;
    s = rand();
    while(s > p)
        VRandom = rand()*vmax;
        p = VDF(VRandom)/fmax;
        s = rand();
    end
    ThetaRandom = rand()*pi;
    p = JTheta(ThetaRandom)/1;
    s = rand();
    while(s > p)
        ThetaRandom = rand()*thetamax;
        p = JTheta(ThetaRandom)/jmax;
        s = rand();
    end
    PhiRandom = rand()*2*pi;
    
    BeamVelocities(1,AtomIndex) = VRandom*cos(ThetaRandom);
    BeamVelocities(3,AtomIndex) = VRandom*sin(ThetaRandom)*cos(PhiRandom);
    BeamVelocities(2,AtomIndex) = VRandom*sin(ThetaRandom)*sin(PhiRandom);
    
end

% set(ThermalAtoms, {'Velocity'}, num2cell(ThermalVelocities,1)');
% set(ThermalAtoms, 'State', AtomState.InsideChamberNeverInPyramid);
v = num2cell(BeamVelocities,1)';
[AtomicBeam.Velocity] = deal(v{:});
[AtomicBeam.CurrentState] = deal(AtomState.NeverInChamber);
[AtomicBeam.PreviousState] = deal(AtomState.NeverInChamber);

end

