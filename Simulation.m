classdef Simulation < matlab.mixin.SetGet
    %SIMULATION Tracks and moves atoms, applies forces, handles output
    %   A simulation performs the time integration of atomic motion,
    %   including the effects of forces.
    
    properties (GetAccess=public,SetAccess=private)
       
        %INITIALIZEEVENT Simulation starts to initialize
        InitializeEvent;
        
        %TIMESTEPEVENT Simulation advances by a time step
        TimeStepEvent;
        
        %UPDATEATOMSSTATE Update states of atoms before recording
        UpdateAtomsStateBeforeRecordEvent; 
        
        %RECORDATOMSEVENT Record atoms
        RecordAtomsEvent;
        
        %UPDATEATOMSSTATEAFTER Update states of atoms after recording
        UpdateAtomsStateAfterRecordEvent; 
        
    end
    
    properties        
        
        %ATOMS An array of atoms
        Atoms;
        
        %FORCES A cell array of forces
        Forces;
        
        %BOUNDARYCONDITION Boundary condition: 1 - Dsicard atoms hitting
        %the chamber wall; 2 - Atoms hitting the chamber wall flash into
        %the chamber with velocity unchanged (constant atom number)
        BoundaryCondition;

        
    end
   
    methods
        
        function instance = Simulation()
           instance.InitializeEvent = Event();
           instance.TimeStepEvent = Event();
           instance.UpdateAtomsStateBeforeRecordEvent = Event();
           instance.RecordAtomsEvent = Event();
           instance.UpdateAtomsStateAfterRecordEvent = Event();
           instance.Atoms = AtomList();
        end
        
        function AddAtoms(instance, atoms)
            %ADDATOMS Add atoms to the simulation
            if ~isa(atoms, 'Atom')
                error('atoms must be of type Atom');
            end
            
            instance.Atoms.append(atoms);
        end
        
        function SpecifyForces(instance, Forces)
            %SET_LASER_BEAMS Set laser beams in the simulation
            instance.Forces = Forces;
        end
            
        function [] = Initialize(instance)
            %INITIALIZE Initialization
            instance.InitializeEvent.invoke(instance, {});            
        end
        
        function Evolution(instance, TotalTime, TimeStep)
            %EVOLUTION Simulate the evolution of atoms given specific total
            %time and time step
            
            % Evolution loop here
            for i=1:round(TotalTime/TimeStep)
                %If there are no atoms left, end the evolution
                if(length(instance.Atoms) == 0)
                    break;
                end               
                
                % Fire events
                instance.TimeStepEvent.invoke(instance, struct('Step', i));
                instance.UpdateAtomsStateBeforeRecordEvent.invoke(instance, {});
                instance.RecordAtomsEvent.invoke(instance, {});
                instance.UpdateAtomsStateAfterRecordEvent.invoke(instance, {});

                %Exert boundary conditions
                if(instance.BoundaryCondition == 1)
                    instance.DiscardAtomsBC();
                end
                
                AtomNum = length(instance.Atoms);
                TotalForces = zeros(3,AtomNum);
                for ForceIndex = 1:length(instance.Forces)
                    TotalForces = TotalForces + instance.Forces{ForceIndex}.CalculateForce(instance.Atoms);
                end
                
                % Update velocities and positions of atoms
                instance.Atoms.Velocities = instance.Atoms.Velocities + TotalForces/instance.Atoms.getAtom(1).Mass*TimeStep;
                instance.Atoms.Positions = instance.Atoms.Positions + instance.Atoms.Velocities*TimeStep;
                    
            end
            
        end
        
        function  DiscardAtomsBC(instance)
            %DISCARDATOMS Discard atoms which hit the chamber, hit the
            %pyramid wall or get out from the hole
            
            % TODO: Instead atoms have only two states - dead or alive.
            % Atom state can be changed by other modifiers.
            % This just harvests up and deletes the dead ones.
            
            %Get atoms hitting the chamber
            hitChamberMask = (instance.Atoms.CurrentStates == AtomState.HittingChamber);
      
            % TODO: Sleep atoms
            instance.Atoms.delete(hitChamberMask);
            %InitalAtoms(hitChamberMask) = [];
        end    
        
    end
    
end

