function [ Flux ] = CalculateFlux( PyramidSim, InitialAtoms, LaserPower, LaserDetuning, BeamWaist1, BeamWaist2, MagneticFieldGradient, PyLength, ChamberLength, HoleDiameter )
%CALCULATEFLUX Calculate the flux given the input parameters
%   Detailed explanation goes here

global ChamberDiameter Vtruncation T ChamberPressure kB AtomNum dt


%set the laser beams
for BeamIndex = 1:length(PyramidSim.Beams)
set(PyramidSim.Beams{BeamIndex}, {'Power','Detuning','BeamWaist1','BeamWaist2','HoleDiameter'}, {LaserPower, LaserDetuning, BeamWaist1, BeamWaist2, HoleDiameter});
end

%Set the linear magnetic field
set(PyramidSim.MagField, 'Gradient', MagneticFieldGradient);

%Set the monitor
plot_z = linspace(-PyLength,0,1000);
plot_x1 = -plot_z + HoleDiameter/2;
plot_y1 = -plot_z + HoleDiameter/2;
plot_x2 = plot_z - HoleDiameter/2;
plot_y2 = plot_z - HoleDiameter/2;
PyramidPlotPoints = [[plot_x1;plot_y1;plot_z], [plot_x1;plot_y2;plot_z], [plot_x2;plot_y1;plot_z],[plot_x2;plot_y2;plot_z]];
set(PyramidSim.Monitor, 'PyramidPlotPoints',PyramidPlotPoints);
set(PyramidSim.Monitor, 'ChamberLength', ChamberLength);

%Set the initial atoms in the simulation
PyramidSim.Initialize(InitialAtoms);

%Run the simulation to obtain the relative flux

%Evolve the system to equilibrium 
TransitTime = 0.005;
PyramidSim.Evolution(TransitTime, dt);
PyramidSim.OutAtoms = [];
PyramidSim.ThroughBottomSurfaceAtoms = [];

%Calculate the relative flux
SimTime = 0.015;
PyramidSim.Evolution(SimTime, dt);
RelativeFlux = length(PyramidSim.OutAtoms)/SimTime;

%Calculate the absolute flux
nReal = ChamberPressure/(kB*T);
nSim = AtomNum/(pi*ChamberDiameter^2/4*(ChamberLength - PyLength));
f1 =@(v) VDF(v);
Fvc = integral(f1,0,150);
Flux = RelativeFlux*nReal/nSim*Fvc;

end

