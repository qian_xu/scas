
%Unit1: Test the intensity field of the Gaussian beam
%Generate a plotting grid in the x-z plane
rm = 0.05;
x = linspace(- 0.05, 0.05, 801);
z = linspace(- 0.12, 0.01, 801);
y = 0;
[x,z] = meshgrid(x,z);

Gamma = 2*pi*6.06e6;
delta = - 4*Gamma;

%set the parameters of the Gaussian beam


%Set the geometry of the pyramid and the chamber
PyWallLength = sqrt(2)*28e-3;
PyReflectivity = 1;
ChamberLength = 120e-3;
ChamberDiameter = 100e-3;
HoleDiameter = 2e-3;
WallThickness= 0.8e-3;
PyAngle = 0.02;
PyLength = PyWallLength*cos(pi/4 - PyAngle);
PyDiameter = PyWallLength*sin(pi/4 - PyAngle)*2 + HoleDiameter;

%Create six trapping beams
P = 380e-3;
Dx0 = 35e-3;
Dy0 = 35e-3;
lambda = 780e-9;


%Create six beams
BeamAngle = pi/6;
BeamXp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2, 0, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamXp, {'Direction', 'Polarization'}, {[cos(2*PyAngle); 0; sin(2*PyAngle)],1});
set(BeamXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamXm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2, pi, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamXm, {'Direction', 'Polarization'}, {[- cos(2*PyAngle); 0; sin(2*PyAngle)],1});
set(BeamXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamYp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2,- pi/2, HoleDiameter/2*tan(pi/4 + PyAngle));
set(BeamYp, {'Direction', 'Polarization'}, {[0; cos(2*PyAngle); sin(2*PyAngle)],1});
set(BeamYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamYm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2,  pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamYm, {'Direction', 'Polarization'}, {[0; - cos(2*PyAngle); sin(2*PyAngle)],1});
set(BeamYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamZp = PyIncidentBeam.Create(P, lambda, delta, Dx0, Dy0, BeamAngle, 0, 0 );
set(BeamZp, {'Direction', 'Polarization'}, {[0; 0; 1],- 1});
set(BeamZp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
%BeamZm = PyRetroBeam.Create(P, lambda, delta, Dx0, Dy0, 0, pi, 0 );
%set(BeamZm, {'Direction', 'Polarization'}, {[0; 0; - 1], - 1});
%set(BeamZm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroXp = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle, pi, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroXp, {'Direction', 'Polarization'}, {[sin(4*PyAngle); 0; - cos(4*PyAngle)], - 1});
set(BeamRetroXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroXm = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle,  0, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroXm, {'Direction', 'Polarization'}, {[ - sin(4*PyAngle); 0; - cos(4*PyAngle)], - 1});
set(BeamRetroXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroYp = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle,  pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroYp, {'Direction', 'Polarization'}, {[0; sin(4*PyAngle); - cos(4*PyAngle)], - 1});
set(BeamRetroYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroYm = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle, - pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroYm, {'Direction', 'Polarization'}, {[0; - sin(4*PyAngle); - cos(4*PyAngle)], - 1});
set(BeamRetroYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
Beams = {BeamXp,BeamXm,BeamYp,BeamYm,BeamZp, BeamRetroXp, BeamRetroXm, BeamRetroYp, BeamRetroYm};

%set(Beams, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity});

TestIntensity = BeamXp.CalculateIntensity(x,y,z) + BeamXm.CalculateIntensity(x,y,z)  ...
    + BeamYp.CalculateIntensity(x,y,z) + BeamYm.CalculateIntensity(x,y,z) ...
    + BeamZp.CalculateIntensity(x,y,z) ...
    + BeamRetroXp.CalculateIntensity(x,y,z) + BeamRetroXm.CalculateIntensity(x,y,z) + BeamRetroYp.CalculateIntensity(x,y,z) + BeamRetroYm.CalculateIntensity(x,y,z);

%TestIntensity = BeamRetroXp.CalculateIntensity(x,y,z) + BeamRetroXm.CalculateIntensity(x,y,z)  + BeamRetroYp.CalculateIntensity(x,y,z) + BeamRetroYm.CalculateIntensity(x,y,z); 
%TestIntensity = BeamXm.CalculateIntensity(x,y,z);

%Plot
figure;
surf(x,z,TestIntensity);
xlabel('x (m)', 'fontsize',20);
ylabel('z (m)','fontsize',20);
ylim([-0.1,0.01]);
title('S_{total} @ P = 380 mW, D_1 = D_2 = 35 mm, I_0 = 1.67 mw/cm^2, d_{hole} = 2 mm, \Delta\theta = 2^?', 'fontsize', 25);
shading interp
colormap(jet)
colorbar;
pbaspect([1 1 1])
view(0,90);
set(gca,'FontSize',22);
set(gca,'position',[0.001 0.12 0.99 0.8]);
set(gcf,'Position',[90 90 980 620]);

%%
%Unit1: Test the intensity field of the Gaussian beam
%Generate a plotting grid in the x-y plane
rm = 0.05;
x = linspace(- 0.05, 0.05, 201);
y = linspace(- 0.05, 0.05, 201);
z =  - 0.025;
[x,y] = meshgrid(x,y);

Gamma = 2*pi*6.06e6;
delta = - 4*Gamma;

%set the parameters of the Gaussian beam


%Set the geometry of the pyramid and the chamber
PyWallLength = sqrt(2)*28e-3;
PyReflectivity = 0.9;
ChamberLength = 120e-3;
ChamberDiameter = 100e-3;
HoleDiameter = 5e-3;
WallThickness= 0.8e-3;
PyAngle = pi/60;
PyLength = PyWallLength*cos(pi/4 - PyAngle);
PyDiameter = PyWallLength*sin(pi/4 - PyAngle)*2 + HoleDiameter;

%Create six trapping beams
P = 380e-3;
Dx0 = 35e-3;
Dy0 = 35e-3;
lambda = 780e-9;

%Create six beams
BeamXp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2, 0, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamXp, {'Direction', 'Polarization'}, {[cos(2*PyAngle); 0; sin(2*PyAngle)],1});
set(BeamXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamXm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2, pi, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamXm, {'Direction', 'Polarization'}, {[- cos(2*PyAngle); 0; sin(2*PyAngle)],1});
set(BeamXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamYp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2,- pi/2, HoleDiameter/2*tan(pi/4 + PyAngle));
set(BeamYp, {'Direction', 'Polarization'}, {[0; cos(2*PyAngle); sin(2*PyAngle)],1});
set(BeamYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamYm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2,  pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamYm, {'Direction', 'Polarization'}, {[0; - cos(2*PyAngle); sin(2*PyAngle)],1});
set(BeamYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamZp = PyIncidentBeam.Create(P, lambda, delta, Dx0, Dy0, 0, 0, 0 );
set(BeamZp, {'Direction', 'Polarization'}, {[0; 0; 1],- 1});
set(BeamZp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
%BeamZm = PyRetroBeam.Create(P, lambda, delta, Dx0, Dy0, 0, pi, 0 );
%set(BeamZm, {'Direction', 'Polarization'}, {[0; 0; - 1], - 1});
%set(BeamZm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroXp = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle, pi, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroXp, {'Direction', 'Polarization'}, {[sin(4*PyAngle); 0; - cos(4*PyAngle)], - 1});
set(BeamRetroXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroXm = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle,  0, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroXm, {'Direction', 'Polarization'}, {[ - sin(4*PyAngle); 0; - cos(4*PyAngle)], - 1});
set(BeamRetroXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroYp = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle,  pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroYp, {'Direction', 'Polarization'}, {[0; sin(4*PyAngle); - cos(4*PyAngle)], - 1});
set(BeamRetroYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroYm = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle, - pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroYm, {'Direction', 'Polarization'}, {[0; - sin(4*PyAngle); - cos(4*PyAngle)], - 1});
set(BeamRetroYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
Beams = {BeamXp,BeamXm,BeamYp,BeamYm,BeamZm};

%set(Beams, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity});

%TestIntensity = BeamXp.CalculateIntensity(x,y,z) + BeamXm.CalculateIntensity(x,y,z)  ...
    %+ BeamYp.CalculateIntensity(x,y,z) + BeamYm.CalculateIntensity(x,y,z) ...
    %+ BeamZp.CalculateIntensity(x,y,z) ...
    %+ BeamRetroXp.CalculateIntensity(x,y,z) + BeamRetroXm.CalculateIntensity(x,y,z) + BeamRetroYp.CalculateIntensity(x,y,z) + BeamRetroYm.CalculateIntensity(x,y,z);

TestIntensity = BeamRetroXp.CalculateIntensity(x,y,z) + BeamRetroXm.CalculateIntensity(x,y,z) + BeamRetroYp.CalculateIntensity(x,y,z) + BeamRetroYm.CalculateIntensity(x,y,z); 
%TestIntensity = BeamZp.CalculateIntensity(x,y,z);

%Plot
figure;
surf(x,y,TestIntensity);
xlabel('x (m)');
ylabel('z (m)');
%ylim([-0.1,0.01]);
title('S_{total} @ P = 200 mW, D_1 = D_2 = 16.5 mm, I_0 = 42.5 mw/cm^2');
shading interp
colormap(jet)
colorbar;
pbaspect([1 1 1])
view(0,90);


%%
%Test the operation on a group of atoms
%set the parameters for atoms
m = 1.44e-25;
Gamma = 2*pi*6.06e6;
I0 = 16.7;

%create a group of atoms
AtomNum = 5;
f1 = arrayfun(@(x) Atom.Create(m,Gamma,I0,rand(3,1),rand(3,1)), 1:AtomNum, 'UniformOutput', 0);
Atoms = cat(1,f1{:});

Positions = [Atoms.Position];
Velocitys = [Atoms.Velocity];

G = Gravity();
G.CalculateForce(Atoms(1))

%%
%Test the GenerateThermalAtoms function
AtomNum = 1000;
NewThermalAtoms = GenerateThermalAtoms(AtomNum, -ChamberDiameter/2, ChamberDiameter/2, -ChamberDiameter/2, ChamberDiameter/2, -ChamberLength, 0, 80);
NewThermalVelocities = [NewThermalAtoms.Velocity];
NewThermalPositions = [NewThermalAtoms.Position];
figure;
plot3(NewThermalPositions(1,:), NewThermalPositions(2,:), NewThermalPositions(3,:), 'o');

figure;
plot3(NewThermalVelocities(1,:),NewThermalVelocities(2,:),NewThermalVelocities(3,:),'o');

%%
%Test the linear magnetic field
MagField = LinearMagneticField();
set(MagField, 'Gradient', 20*0.01);
set(MagField, 'FieldCenter', [0; 0; - 0.01]);
[PlotX,PlotY,PlotZ] = meshgrid(linspace(-0.03,0.03,10),linspace(-0.03,0.03,10),linspace(-0.03,0.03,10));
PlotX = reshape(PlotX,[1,10*10*10]);
PlotY = reshape(PlotY,[1,10*10*10]);
PlotZ = reshape(PlotZ,[1,10*10*10]);
%[PlotX,PlotY] = meshgrid(linspace(-0.03,0.03,50),linspace(-0.03,0.03,50));
B = MagField.CalculateMagField(PlotX,PlotY,PlotZ);

figure;
quiver3(PlotX,PlotY,PlotZ, B(1,:),B(2,:),B(3,:))

%%
%Test the scattering force

m = 1.44e-25;
Gamma = 2*pi*6.06e6;
I0 = 16.7;

%set the parameters of pyramid
PyLength = 28e-3;
PyDiameter = 56e-3;
ChamberLength = 120e-3;
ChamberDiameter = 100e-3;
HoleDiameter = 8e-3;
PyWallThickness = 0.8e-3;
PyReflectivity = 1;
PyAngle = pi/100;

%x-vx-F graph
AtomNum = 6400;
TestZ = linspace(-ChamberLength, 0,sqrt(AtomNum));
TestVz = linspace(-200,200,sqrt(AtomNum));
[TestZ,TestVz] = meshgrid(TestZ,TestVz);
TestZ = reshape(TestZ, [1,AtomNum]);
TestVz = reshape(TestVz, [1,AtomNum]);
TestX = zeros(1,AtomNum);
TestY = zeros(1,AtomNum);
TestVx = zeros(1,AtomNum);
TestVy = zeros(1,AtomNum);

%Set the linear magnetic field
Xc = 0;
Yc = 0;
Zc = - PyLength/2;
Beta = 8*0.01;   %Linear gradient
MagField = LinearMagneticField();
set(MagField, 'Gradient', Beta);
set(MagField, 'FieldCenter', [Xc; Yc; Zc]);

%Create six trapping beams
P = 380e-3;
Dx0 = 35e-3;
Dy0 = 35e-3;
lambda = 780e-9;
Gamma = 2*pi*6.06e6;
delta = - 4*Gamma;

%Create six beams
BeamXp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2, 0, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamXp, {'Direction', 'Polarization'}, {[cos(2*PyAngle); 0; sin(2*PyAngle)],1});
set(BeamXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamXm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2, pi, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamXm, {'Direction', 'Polarization'}, {[- cos(2*PyAngle); 0; sin(2*PyAngle)],1});
set(BeamXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamYp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2,- pi/2, HoleDiameter/2*tan(pi/4 + PyAngle));
set(BeamYp, {'Direction', 'Polarization'}, {[0; cos(2*PyAngle); sin(2*PyAngle)],1});
set(BeamYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamYm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2,  pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamYm, {'Direction', 'Polarization'}, {[0; - cos(2*PyAngle); sin(2*PyAngle)],1});
set(BeamYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamZp = PyIncidentBeam.Create(P, lambda, delta, Dx0, Dy0, 0, 0, 0 );
set(BeamZp, {'Direction', 'Polarization'}, {[0; 0; 1],- 1});
set(BeamZp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
%BeamZm = PyRetroBeam.Create(P, lambda, delta, Dx0, Dy0, 0, pi, 0 );
%set(BeamZm, {'Direction', 'Polarization'}, {[0; 0; - 1], - 1});
%set(BeamZm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroXp = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle, pi, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroXp, {'Direction', 'Polarization'}, {[sin(4*PyAngle); 0; - cos(4*PyAngle)], - 1});
set(BeamRetroXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroXm = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle,  0, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroXm, {'Direction', 'Polarization'}, {[ - sin(4*PyAngle); 0; - cos(4*PyAngle)], - 1});
set(BeamRetroXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroYp = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle,  pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroYp, {'Direction', 'Polarization'}, {[0; sin(4*PyAngle); - cos(4*PyAngle)], - 1});
set(BeamRetroYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroYm = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle, - pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroYm, {'Direction', 'Polarization'}, {[0; - sin(4*PyAngle); - cos(4*PyAngle)], - 1});
set(BeamRetroYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
%Separate beams into two groups because retrobeams are resonant
%simutaneously
PushAndTransverseBeams = {BeamXp,BeamXm,BeamYp,BeamYm,BeamZp};
RetroBeams = {BeamRetroXp, BeamRetroXm,BeamRetroYp,BeamRetroYm}; 
Beams = {PushAndTransverseBeams, RetroBeams};

%create a group of atoms

f1 = arrayfun(@(x) Atom.Create(m,Gamma,I0,rand(3,1),rand(3,1)), 1:AtomNum, 'UniformOutput', 0);
Atoms = cat(1,f1{:});
muB = 9.27400e-24;
gp = - 2/3;
gm = 2/3;
gz = 0;
set(Atoms, 'Mup', gp*muB);
set(Atoms, 'Mum', gm*muB);
set(Atoms, 'Muz', gz*muB);
InitialPositions = [TestX; TestY; TestZ];
InitialVelocities = [TestVx; TestVy; TestVz];
set(Atoms, {'Position'}, num2cell(InitialPositions, 1)');
set(Atoms, {'Velocity'}, num2cell(InitialVelocities, 1)');

%Set the scattering force
dt = ChamberDiameter/10/3000;
ScatPushAndTransverseForce = ScatteringForce();
set(ScatPushAndTransverseForce, 'Beams', PushAndTransverseBeams);
set(ScatPushAndTransverseForce, 'MagField', MagField);
%set(ScatForce, 'MagField', ZeemanMagField);
set(ScatPushAndTransverseForce, 'TimeStep', dt);
set(ScatPushAndTransverseForce, {'IfAddRecoil', 'IfAddScatteringFluctuation'}, {true,true});
set(ScatPushAndTransverseForce,'IfResonant',false);

dt = ChamberDiameter/10/3000;
ScatRetroForce = ScatteringForce();
set(ScatRetroForce, 'Beams', RetroBeams);
set(ScatRetroForce, 'MagField', MagField);
%set(ScatForce, 'MagField', ZeemanMagField);
set(ScatRetroForce, 'TimeStep', dt);
set(ScatRetroForce, {'IfAddRecoil', 'IfAddScatteringFluctuation'}, {true,true});
set(ScatRetroForce, 'IfResonant',true);

f1 = ScatPushAndTransverseForce.CalculateForce(Atoms);
f2 = ScatRetroForce.CalculateForce(Atoms);
f = f1 + f2;

AtomsPositions = [Atoms.Position];
AtomsX = AtomsPositions(1,:);
AtomsY = AtomsPositions(2,:);
AtomsZ = AtomsPositions(3,:);

AtomsVelocities = [Atoms.Velocity];
AtomsVx = AtomsVelocities(1,:);
AtomsVy = AtomsVelocities(2,:);
AtomsVz = AtomsVelocities(3,:);
figure;
quiver3(AtomsX,AtomsY,AtomsZ, f(1,:),f(2,:),f(3,:));
xlabel('x');
ylabel('y');
zlabel('z');


AtomsZ = reshape(AtomsZ,[sqrt(AtomNum),sqrt(AtomNum)]);
AtomsVz = reshape(AtomsVz, [sqrt(AtomNum),sqrt(AtomNum)]);
Fz = reshape(f(3,:), [sqrt(AtomNum),sqrt(AtomNum)]);
figure;
surf(AtomsZ, AtomsVz, Fz/m)
xlabel('x (m)');
ylabel('vx (m/s)');
title('a_x @ \Gamma = 2\pi*32MHZ, \delta = - 1\Gamma, B = 34.79G/cm, \zeta_d = 1')
shading interp
colormap(jet)
colorbar;
view([0,90]);
hold on;

%%
AtomNum = 6400;
TestX = linspace(-PyDiameter/2, PyDiameter/2,sqrt(AtomNum));
TestVx = linspace(-200,200,sqrt(AtomNum));
[TestX,TestVx] = meshgrid(TestX,TestVx);
TestX = reshape(TestX, [1,AtomNum]);
TestVx = reshape(TestVx, [1,AtomNum]);
TestZ = zeros(1,AtomNum) - 0.014;
TestY = zeros(1,AtomNum);
TestVz = zeros(1,AtomNum);
TestVy = zeros(1,AtomNum);

InitialPositions = [TestX; TestY; TestZ];
InitialVelocities = [TestVx; TestVy; TestVz];
set(Atoms, {'Position'}, num2cell(InitialPositions, 1)');
set(Atoms, {'Velocity'}, num2cell(InitialVelocities, 1)');

f = ScatForce.CalculateForce(Atoms);

AtomsPositions = [Atoms.Position];
AtomsX = AtomsPositions(1,:);
AtomsY = AtomsPositions(2,:);
AtomsZ = AtomsPositions(3,:);

AtomsVelocities = [Atoms.Velocity];
AtomsVx = AtomsVelocities(1,:);
AtomsVy = AtomsVelocities(2,:);
AtomsVz = AtomsVelocities(3,:);
figure;
quiver3(AtomsX,AtomsY,AtomsZ, f(1,:),f(2,:),f(3,:));
xlabel('x');
ylabel('y');
zlabel('z');


AtomsX = reshape(AtomsX,[sqrt(AtomNum),sqrt(AtomNum)]);
AtomsVx = reshape(AtomsVx, [sqrt(AtomNum),sqrt(AtomNum)]);
Fx = reshape(f(1,:), [sqrt(AtomNum),sqrt(AtomNum)]);
figure;
surf(AtomsX, AtomsVx, Fx/m)
xlabel('x (m)');
ylabel('vx (m/s)');
title('a_x @ \Gamma = 2\pi*32MHZ, \delta = - 1\Gamma, B = 34.79G/cm, \zeta_d = 1')
shading interp
colormap(jet)
colorbar;
view([0,90]);
hold on;

%%
%Test the ZeemanSlowerLikeMagneticField



m = 1.44e-25;
Gamma = 2*pi*6.06e6;
I0 = 16.7;

%set the parameters of pyramid
PyLength = 28e-3;
PyDiameter = 56e-3;
ChamberLength = 120e-3;
ChamberDiameter = 100e-3;
HoleDiameter = 2e-3;
PyWallThickness = 0.8e-3;
PyReflectivity = 1;
PyAngle = pi/60000;

%x-vx-F graph
AtomNum = 6400;
TestZ = linspace(-ChamberLength, 0,sqrt(AtomNum));
TestVz = linspace(-300,300,sqrt(AtomNum));
[TestZ,TestVz] = meshgrid(TestZ,TestVz);
TestZ = reshape(TestZ, [1,AtomNum]);
TestVz = reshape(TestVz, [1,AtomNum]);
TestX = zeros(1,AtomNum) - 0.002;
TestY = zeros(1,AtomNum) - 0.002;
TestVx = zeros(1,AtomNum);
TestVy = zeros(1,AtomNum);


%Create six trapping beams
P = 380e-3;
Dx0 = 35e-3;
Dy0 = 35e-3;
lambda = 780e-9;
Gamma = 2*pi*6.06e6;
delta = - 4*Gamma;

%Create six beams
BeamXp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2, 0, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamXp, {'Direction', 'Polarization'}, {[cos(2*PyAngle); 0; sin(2*PyAngle)],1});
set(BeamXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamXm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2, pi, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamXm, {'Direction', 'Polarization'}, {[- cos(2*PyAngle); 0; sin(2*PyAngle)],1});
set(BeamXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamYp = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2,- pi/2, HoleDiameter/2*tan(pi/4 + PyAngle));
set(BeamYp, {'Direction', 'Polarization'}, {[0; cos(2*PyAngle); sin(2*PyAngle)],1});
set(BeamYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamYm = PyTransverseBeam.Create(P, lambda, delta, Dx0, Dy0, pi/2 + PyAngle*2,  pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamYm, {'Direction', 'Polarization'}, {[0; - cos(2*PyAngle); sin(2*PyAngle)],1});
set(BeamYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamZp = PyIncidentBeam.Create(P, lambda, delta, Dx0, Dy0, 0, 0, 0 );
set(BeamZp, {'Direction', 'Polarization'}, {[0; 0; 1],- 1});
set(BeamZp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
%BeamZm = PyRetroBeam.Create(P, lambda, delta, Dx0, Dy0, 0, pi, 0 );
%set(BeamZm, {'Direction', 'Polarization'}, {[0; 0; - 1], - 1});
%set(BeamZm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroXp = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle, pi, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroXp, {'Direction', 'Polarization'}, {[sin(4*PyAngle); 0; - cos(4*PyAngle)], - 1});
set(BeamRetroXp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroXm = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle,  0, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroXm, {'Direction', 'Polarization'}, {[ - sin(4*PyAngle); 0; - cos(4*PyAngle)], - 1});
set(BeamRetroXm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroYp = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle,  pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroYp, {'Direction', 'Polarization'}, {[0; sin(4*PyAngle); - cos(4*PyAngle)], - 1});
set(BeamRetroYp, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
BeamRetroYm = PyRetroInclinedBeam.Create(P, lambda, delta, Dx0, Dy0, pi - 4*PyAngle, - pi/2, HoleDiameter/2*tan(pi/4 + PyAngle) );
set(BeamRetroYm, {'Direction', 'Polarization'}, {[0; - sin(4*PyAngle); - cos(4*PyAngle)], - 1});
set(BeamRetroYm, {'PyLength', 'PyDiameter', 'HoleDiameter', 'PyReflectivity','PyAngle'}, {PyLength, PyDiameter, HoleDiameter,PyReflectivity,PyAngle});
%Separate beams into two groups because retrobeams are resonant
%simutaneously
PushAndTransverseBeams = {BeamXp,BeamXm,BeamYp,BeamYm,BeamZp};
RetroBeams = {BeamRetroXp, BeamRetroXm,BeamRetroYp,BeamRetroYm}; 
Beams = {PushAndTransverseBeams, RetroBeams};

%Set the zeeman slower like magnetic field
ZeemanMagField = ZeemanSlowerLikeMagneticField();
Vout = 30;
Beta = 8*0.01;
set(ZeemanMagField, {'L','l','am','Vout','Gradient', 'FieldCenter','WaveVector','Mu','Detuning'},...
    {ChamberLength, PyLength, 0.9e5, Vout, Beta, [0;0;-PyLength/2],2*pi/lambda, gm*muB, delta});
PlotZ = linspace(-ChamberLength,0.02,1000);
PlotX = zeros(1,length(PlotZ));
PlotY = zeros(1,length(PlotZ));
PlotB = ZeemanMagField.CalculateMagField(PlotX,PlotY,PlotZ);
figure;
plot(PlotZ,PlotB)


%create a group of atoms

f1 = arrayfun(@(x) Atom.Create(m,Gamma,I0,rand(3,1),rand(3,1)), 1:AtomNum, 'UniformOutput', 0);
Atoms = cat(1,f1{:});
muB = 9.27400e-24;
gp = - 2/3;
gm = 2/3;
gz = 0;
set(Atoms, 'Mup', gp*muB);
set(Atoms, 'Mum', gm*muB);
set(Atoms, 'Muz', gz*muB);
InitialPositions = [TestX; TestY; TestZ];
InitialVelocities = [TestVx; TestVy; TestVz];
set(Atoms, {'Position'}, num2cell(InitialPositions, 1)');
set(Atoms, {'Velocity'}, num2cell(InitialVelocities, 1)');


%Set the scattering force
dt = ChamberDiameter/10/3000;
ScatPushAndTransverseForce = ScatteringForce();
set(ScatPushAndTransverseForce, 'Beams', PushAndTransverseBeams);
set(ScatPushAndTransverseForce, 'MagField', MagField);
%set(ScatForce, 'MagField', ZeemanMagField);
set(ScatPushAndTransverseForce, 'TimeStep', dt);
set(ScatPushAndTransverseForce, {'IfAddRecoil', 'IfAddScatteringFluctuation'}, {true,true});
set(ScatPushAndTransverseForce,'IfResonant',false);

dt = ChamberDiameter/10/3000;
ScatRetroForce = ScatteringForce();
set(ScatRetroForce, 'Beams', RetroBeams);
set(ScatRetroForce, 'MagField', MagField);
%set(ScatForce, 'MagField', ZeemanMagField);
set(ScatRetroForce, 'TimeStep', dt);
set(ScatRetroForce, {'IfAddRecoil', 'IfAddScatteringFluctuation'}, {true,true});
set(ScatRetroForce, 'IfResonant',true);


f = ScatForce.CalculateForce(Atoms);

AtomsPositions = [Atoms.Position];
AtomsX = AtomsPositions(1,:);
AtomsY = AtomsPositions(2,:);
AtomsZ = AtomsPositions(3,:);

AtomsVelocities = [Atoms.Velocity];
AtomsVx = AtomsVelocities(1,:);
AtomsVy = AtomsVelocities(2,:);
AtomsVz = AtomsVelocities(3,:);
figure;
quiver3(AtomsX,AtomsY,AtomsZ, f(1,:),f(2,:),f(3,:));
xlabel('x');
ylabel('y');
zlabel('z');


AtomsZ = reshape(AtomsZ,[sqrt(AtomNum),sqrt(AtomNum)]);
AtomsVz = reshape(AtomsVz, [sqrt(AtomNum),sqrt(AtomNum)]);
Fz = reshape(f(3,:), [sqrt(AtomNum),sqrt(AtomNum)]);
figure;
surf(AtomsZ, AtomsVz, Fz/m)
xlabel('x (m)');
ylabel('vx (m/s)');
title('a_x @ \Gamma = 2\pi*32MHZ, \delta = - 1\Gamma, B = 34.79G/cm, \zeta_d = 1')
shading interp
colormap(jet)
colorbar;
view([0,90]);
hold on;




