classdef Gravity <  ForceField
    %GRAVITY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function f = CalculateForce(~, Atoms)
            g = 10;
            AtomPositions = [Atoms.Position];
            AtomVelocities = [Atoms.Velocity];
            x = AtomPositions(1,:);
            y = AtomPositions(2,:);
            z = AtomPositions(3,:);
            vx = AtomVelocities(1,:);
            vy = AtomVelocities(2,:);
            vz = AtomVelocities(3,:);
            AtomNum = length(Atoms);
            Fx = zeros(1,AtomNum);
            Fy = zeros(1,AtomNum);
            Fz = - [Atoms.Mass]*g.*ones(1,AtomNum);
            
            f = [Fx; Fy; Fz];
        end
    end
    
end

