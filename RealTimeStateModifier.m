classdef RealTimeStateModifier < matlab.mixin.SetGet
 
    properties
        Simulation;
        
        %Set the chamber diameter and the tube diameter
        ChamberDiameter;
        TubeDiameter;
        
        IfKeepOutAtoms;
        
    end
    
    methods
        function instance = RealTimeStateModifier(sim)
            %REALTIMEMONITOR Construct an instance of this class
            instance.Simulation = sim;
            sim.UpdateAtomsStateBeforeRecordEvent.add(EventListener(@(sender, callback) instance.UpdateAtomsStateBeforeRecord(sender, callback)));
            sim.UpdateAtomsStateAfterRecordEvent.add(EventListener(@(sender, callback) instance.UpdateAtomsStateAfterRecord(sender, callback)));
        end
        
        
        function UpdateAtomsStateBeforeRecord(instance, sender, callback)
            
            AtomsPositions = instance.Simulation.Atoms.Positions;
            AtomsVelocities = instance.Simulation.Atoms.Velocities;
            AtomsCurrentStates = instance.Simulation.Atoms.CurrentStates;
            AtomsPreviousStates = instance.Simulation.Atoms.PreviousStates;
            x = AtomsPositions(1,:);
            y = AtomsPositions(2,:);
            z = AtomsPositions(3,:);
            vx = AtomsVelocities(1,:);
            vy = AtomsVelocities(2,:);
            vz = AtomsVelocities(3,:);
            
            ThetaCut = 0.2;
            IfInsideChamber = sqrt(x.^2 + y.^2 + z.^2) < instance.ChamberDiameter/2;
            IfComingOut  = (z >= instance.ChamberDiameter/2 - 0.005) & (sqrt(x.^2 + y.^2) <= instance.TubeDiameter/2) & (abs(atan(sqrt(vx.^2 + vy.^2)./vz)) <= ThetaCut)...
                & (AtomsPreviousStates == AtomState.InsideChamber);
            IfHittingChamber = (sqrt(x.^2 + y.^2 + z.^2) >= instance.ChamberDiameter/2)&(AtomsPreviousStates == AtomState.InsideChamber)&(~IfComingOut);
            
            instance.Simulation.Atoms.CurrentStates(IfInsideChamber) = AtomState.InsideChamber;
            instance.Simulation.Atoms.CurrentStates(IfComingOut) = AtomState.ComingOutFromTube;
            instance.Simulation.Atoms.CurrentStates(IfHittingChamber) = AtomState.HittingChamber;
            
        end
        
        function UpdateAtomsStateAfterRecord(instance, sender, callback)
            
          %UPDATEATOMSSTATESAFTERRECORD Update the states of atoms after
            %recording           
            instance.Simulation.Atoms.CurrentStates(instance.Simulation.Atoms.CurrentStates == AtomState.ComingOutFromTube) = AtomState.HasComeOutOfTube;
            instance.Simulation.Atoms.PreviousStates = instance.Simulation.Atoms.CurrentStates;
            
            if(~instance.IfKeepOutAtoms)
                instance.Simulation.Atoms.CurrentStates(instance.Simulation.Atoms.CurrentStates == AtomState.HasComeOutOfTube) = AtomState.HittingChamber;
            end
            
        end
    end


end

