function [ I ] = GaussionIntensity( x,y,z,theta,phi, P, Dx0,Dy0, d_shift)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%Dx0 = 16.5e-3;
%Dy0 = 16.5e-3;   %change this to change the z direction

%P = 200e-3;

zR = 20;
%rotate the frame
%x_0 = cos(phi)*x - sin(phi)*cos(theta)*y + sin(phi)*sin(theta)*z ;
%y_0 = sin(phi)*x + cos(phi)*cos(theta)*y  - sin(theta)*cos(phi)*z;
%z_0 = sin(theta)*y + cos(theta)*z;

x_0 = cos(phi)*x - sin(phi)*y;
y_0 = cos(theta)*sin(phi)*x + cos(phi)*cos(theta)*y  - sin(theta)*z + d_shift;
z_0 = sin(theta)*sin(phi)*x + sin(theta)*cos(phi)*y + cos(theta)*z;

Dx = Dx0*sqrt(1+(z_0.^2)/zR^2);
Dy = Dy0*sqrt(1+(z_0.^2)/zR^2);

I = P./(pi/2*Dx.*Dy).*exp(-2*x_0.^2./Dx.^2 -2*y_0.^2./Dy.^2);


end

