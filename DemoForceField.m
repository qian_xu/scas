classdef DemoForceField < ForceField
    %DEMOFORCEFIELD Simple example of calculating a force on atoms.
    
    properties
        
        %INTENSITY Intensity of cooling beam, units I/Isat
        Intensity;
        
        %DETUNING Detuning of the cooling beam, units delta/Gamma
        Detuning;
        
    end
    
    methods
        
        function f = CalculateForces(instance, atom)
            %CALCULATEFORCES Calculltes forces applied to atom
            
            
        end
    end
    
end

