classdef Atom < handle
    %ATOM Represents an atom
    %  An atom has properties such as position and velocity.
    
    properties
        
        %POSITION Position of atom, 3x1 vector
        Position;
        
        %VELOCITY Velocity of atom, 3x1 vector
        Velocity;
        
        %MASS Mass of atom
        Mass;
        
        %TRANLINEWIDTH Transition linewidth
        TranLineWidth;
        
        %SATINTENSITY Saturation intensitys
        SatIntensity;
        
        %MUP Magnetic moment corresponding to the sigma plus transition
        Mup; 
        
        %MUM Magnetic moment corresponding to the sigma minus transition
        Mum;
        
        %MUZ Magnetic moment corresponding to the pi transition
        Muz;
        
        %STATE Represents the state of an atom: -1-hitting the chamber; 1-inside the chamber and
        %never in the pyramid; 2-entering the pyramid from the bottom
        %surface; 3-entering the pyramid from the pyramid walls; 4-inside
        %the pyramid; 5-coming out the pyramid from the hole; 6-has come
        %out from the hole
        CurrentState;
        PreviousState;
    end
    
    methods (Static)
        %CREATE Create new atom 
        function obj = Create(mass, tranlinewidth, satintensity, position, velocity)
            obj = Atom();
            obj.Mass = mass;
            obj.TranLineWidth = tranlinewidth;
            obj.SatIntensity = satintensity;
            obj.Position = position;
            obj.Velocity = velocity;
        end
    end
    
    methods 

        function UpdatePosition(instance, NewPosition)
            %UPDATEPOSITION Update the postion of atom
            instance.Position = NewPosition;        
        end
        
        function UpdateVelocity(instance, NewVelocity)
            %UPDATEVELOCITY Update the velocity of atom
            instance.Velocity = NewVelocity;        
        end

%         function set.State(obj, value)
%             %SET.STATE Set state of atom
%             if(~isa(value, 'AtomState'))
%                 error('Atom.State must be of type AtomState');
%             end
%             obj.State = value;
%         end

        function obj = CopyAtom(instance)
            %COPY Copy a new object which has same properties with instance
            obj = Atom();
            obj.Position = instance.Position;
            obj.Velocity = instance.Velocity;
            obj.Mass = instance.Mass;
            obj.TranLineWidth = instance.TranLineWidth;
            obj.SatIntensity = instance.SatIntensity;
            obj.Mup = instance.Mup;
            obj.Mum = instance.Mum;
            obj.Muz = instance.Muz;
            obj.CurrentState = instance.CurrentState;
            obj.PreviousState = instance.PreviousState;
        end
    end
    
end

