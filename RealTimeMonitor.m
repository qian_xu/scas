classdef RealTimeMonitor < matlab.mixin.SetGet
    %REALTIMEMONITOR Watches simulation in real time
    
    properties
        Simulation
        FigHandle;
        DataPlot;
 
        ChamberDiameter;
        ds;
    end
    
    methods
        function instance = RealTimeMonitor(sim)
            %REALTIMEMONITOR Construct an instance of this class
            instance.Simulation = sim;
            
            % Listen to events.
            sim.InitializeEvent.add(EventListener(@(sender, callback) instance.start(sender, callback)));
            sim.TimeStepEvent.add(EventListener(@(sender, callback) instance.onTimeStep(sender, callback)));
        end
        
        function start(instance, sender, callback)
            
            instance.FigHandle = figure('Color', 'w');
            a = axes();
            instance.DataPlot = plot3(a, 0,0,0, '.', 'MarkerSize', 10, 'Color', 'k');
            title(a, '$\mathrm{SimViewer}^\mathrm{(tm)}$', 'Interpreter', 'Latex');
            xlim(a, [-instance.ds, instance.ChamberDiameter/2]);
            ylim(a, [-instance.ChamberDiameter/(2), instance.ChamberDiameter/(2)]);
            zlim(a, [-instance.ChamberDiameter/(2), instance.ChamberDiameter/(2) + 0.07]);
            xlabel(a, '$X$', 'Interpreter', 'Latex', 'FontSize', 10);
            ylabel(a, '$y$', 'Interpreter', 'Latex', 'FontSize', 10);
            zlabel(a, '$z$', 'Interpreter', 'Latex', 'FontSize', 10);
            view([30,15]);
            
            
        end
        
        function onTimeStep(instance, sender, callback)
            
            if ishandle(instance.DataPlot)
                pos = instance.Simulation.Atoms.Positions;
                set(instance.DataPlot, 'XData', pos(1,:), 'YData', pos(2,:), 'ZData', pos(3,:));
                drawnow;
            end
            
        end
      
    end
end

